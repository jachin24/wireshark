<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ko">
<context>
    <name>Abbreviation</name>
    <message>
        <source/>
        <comment>for &quot;not applicable&quot;</comment>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="about_dialog.ui" line="14"/>
        <source>About Wireshark</source>
        <translation>Wireshark에 대하여</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="36"/>
        <source>Wireshark</source>
        <translation>Wireshark</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="64"/>
        <source>&lt;span size=\&quot;x-large\&quot; weight=\&quot;bold\&quot;&gt;Network Protocol Analyzer&lt;/span&gt;</source>
        <translation>&lt;span size=\&quot;x-large\&quot; weight=\&quot;bold\&quot;&gt;네트워크 프로토콜 분석기&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="95"/>
        <source>Copy the version information to the clipboard</source>
        <translation>버전정보를 클립보드에 복사</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="98"/>
        <source>Copy to Clipboard</source>
        <translation>클립보드로 복사</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="121"/>
        <source>Authors</source>
        <translation>제작자</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="127"/>
        <source>Search Authors</source>
        <translation>제작자 검색</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="154"/>
        <source>Folders</source>
        <translation>폴더</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="160"/>
        <source>Filter by path</source>
        <translation>경로로 필터링</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="181"/>
        <source>Plugins</source>
        <translation>플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="187"/>
        <source>No plugins found.</source>
        <translation>플러그인이 발견되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="196"/>
        <source>Search Plugins</source>
        <translation>플러그인 검색</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="203"/>
        <source>Filter by type:</source>
        <translation>유형별 필터링:</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="229"/>
        <source>Keyboard Shortcuts</source>
        <translation>키보드 단축키</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="235"/>
        <source>Search Shortcuts</source>
        <translation>단축키 검색</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="262"/>
        <source>Acknowledgments</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="about_dialog.ui" line="268"/>
        <source>License</source>
        <translation>라이센스</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="497"/>
        <source>The directory does not exist</source>
        <translation>디렉터리가 존재하지 않음</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="498"/>
        <source>Should the directory %1 be created?</source>
        <translation>%1 디렉터리를 생성하시겠습니까?</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="502"/>
        <source>The directory could not be created</source>
        <translation>디렉터리를 생성할 수 없습니다</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="503"/>
        <source>The directory %1 could not be created.</source>
        <translation>%1 디렉터리를 만들 수 없었습니다.</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="532"/>
        <source>Show in Finder</source>
        <translation>탐색기로 표시</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="534"/>
        <source>Show in Folder</source>
        <translation>폴더에서 표시</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="541"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message numerus="yes">
        <location filename="about_dialog.cpp" line="546"/>
        <source>Copy Row(s)</source>
        <translation><numerusform>행 복사</numerusform></translation>
    </message>
</context>
<context>
    <name>AddressEditorFrame</name>
    <message>
        <location filename="address_editor_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="address_editor_frame.ui" line="32"/>
        <source>Name Resolution Preferences…</source>
        <oldsource>Name Resolution Preferences...</oldsource>
        <translation>도메인 이름 설정…</translation>
    </message>
    <message>
        <location filename="address_editor_frame.ui" line="52"/>
        <source>Address:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <location filename="address_editor_frame.ui" line="75"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <location filename="address_editor_frame.cpp" line="173"/>
        <source>Can&apos;t assign %1 to %2.</source>
        <translation>%1 에서 %2 로 할당할 수 없습니다.</translation>
    </message>
</context>
<context>
    <name>AdvancedPrefsModel</name>
    <message>
        <location filename="models/pref_models.cpp" line="348"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="350"/>
        <source>Status</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="352"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="354"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
</context>
<context>
    <name>ApplyLineEdit</name>
    <message>
        <location filename="widgets/apply_line_edit.cpp" line="30"/>
        <source>Apply changes</source>
        <translation>변경내용 적용</translation>
    </message>
</context>
<context>
    <name>AuthorListModel</name>
    <message>
        <location filename="about_dialog.cpp" line="100"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="100"/>
        <source>Email</source>
        <translation>이메일</translation>
    </message>
</context>
<context>
    <name>BluetoothAttServerAttributesDialog</name>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="20"/>
        <source>Bluetooth ATT Server Attributes</source>
        <translation>블루투스 ATT 서버 속성</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="54"/>
        <source>Handle</source>
        <translation>처리</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="59"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="64"/>
        <source>UUID Name</source>
        <translation>UUID 이름</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="96"/>
        <source>All Interfaces</source>
        <translation>모든 인터페이스</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="150"/>
        <source>All Devices</source>
        <translation>모든 장치</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="158"/>
        <source>Remove duplicates</source>
        <translation>중복된 내용 삭제</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="180"/>
        <source>Copy Cell</source>
        <translation>셀 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="185"/>
        <source>Copy Rows</source>
        <translation>행 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="190"/>
        <source>Copy All</source>
        <translation>모두 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="195"/>
        <source>Save as image</source>
        <translation>그림으로 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="200"/>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="203"/>
        <source>Mark/Unmark Row</source>
        <translation>행 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="206"/>
        <source>Ctrl-M</source>
        <translation>Ctrl-M</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.ui" line="211"/>
        <source>Mark/Unmark Cell</source>
        <translation>셀 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.cpp" line="372"/>
        <source>Save Table Image</source>
        <translation>표 그림으로 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_att_server_attributes_dialog.cpp" line="374"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG 그림 (*.png)</translation>
    </message>
</context>
<context>
    <name>BluetoothDeviceDialog</name>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="23"/>
        <source>Bluetooth Device</source>
        <translation>블루투스 장치</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="75"/>
        <source>BD_ADDR</source>
        <translation>BD_ADDR</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="80"/>
        <source>OUI</source>
        <translation>OUI</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="85"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="90"/>
        <source>Class of Device</source>
        <translation>장비 클래스</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="95"/>
        <source>LMP Version</source>
        <translation>LMP 버전</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="100"/>
        <source>LMP Subversion</source>
        <translation>LMP 하위버전</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="105"/>
        <source>Manufacturer</source>
        <translation>제조업체</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="110"/>
        <source>HCI Version</source>
        <translation>HCI 버전</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="115"/>
        <source>HCI Revision</source>
        <translation>HCI 리비전</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="120"/>
        <source>Scan</source>
        <translation>스캔</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="125"/>
        <source>Authentication</source>
        <translation>인증</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="130"/>
        <source>Encryption</source>
        <translation>암호화</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="135"/>
        <source>ACL MTU</source>
        <translation>ACL MTU</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="140"/>
        <source>ACL Total Packets</source>
        <translation>ACL 전체 패킷</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="145"/>
        <source>SCO MTU</source>
        <translation>SCO MTU</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="150"/>
        <source>SCO Total Packets</source>
        <translation>SCO 전체 패킷</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="155"/>
        <source>LE ACL MTU</source>
        <translation>LE ACL MTU</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="160"/>
        <source>LE ACL Total Packets</source>
        <translation>LE ACL 전체 패킷</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="165"/>
        <source>LE ISO MTU</source>
        <translation>LE ISO MTU</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="170"/>
        <source>LE ISO Total Packets</source>
        <translation>LE ISO 전체 패킷</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="175"/>
        <source>Inquiry Mode</source>
        <translation>질의 모드</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="180"/>
        <source>Page Timeout</source>
        <translation>페이지 시간 초과</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="185"/>
        <source>Simple Pairing Mode</source>
        <translation>심플 페어링 모드</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="190"/>
        <source>Voice Setting</source>
        <translation>음성 설정</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="195"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="200"/>
        <source>Changes</source>
        <translation>변경</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="213"/>
        <location filename="bluetooth_device_dialog.cpp" line="640"/>
        <source>%1 changes</source>
        <translation>%1 변경</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="230"/>
        <source>Copy Cell</source>
        <translation>셀 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="235"/>
        <source>Copy Rows</source>
        <translation>행 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="240"/>
        <source>Copy All</source>
        <translation>모두 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="245"/>
        <source>Save as image</source>
        <translation>그림으로 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="250"/>
        <location filename="bluetooth_device_dialog.ui" line="253"/>
        <source>Mark/Unmark Row</source>
        <translation>행 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="256"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.ui" line="261"/>
        <source>Mark/Unmark Cell</source>
        <translation>셀 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="164"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="173"/>
        <source>Bluetooth Device - %1%2</source>
        <translation>블루투스 장치 - %1%2</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="563"/>
        <source>enabled</source>
        <translation>사용됨</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="563"/>
        <source>disabled</source>
        <translation>해제됨</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="571"/>
        <source>%1 ms (%2 slots)</source>
        <translation>%1 밀리초 (%2 슬롯)</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="675"/>
        <source>Save Table Image</source>
        <translation>표 그림 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_device_dialog.cpp" line="677"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG 그림 (*.png)</translation>
    </message>
</context>
<context>
    <name>BluetoothDevicesDialog</name>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="20"/>
        <source>Bluetooth Devices</source>
        <translation>블루투스 장치</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="54"/>
        <source>BD_ADDR</source>
        <translation>BD_ADDR</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="59"/>
        <source>OUI</source>
        <translation>OUI</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="64"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="69"/>
        <source>LMP Version</source>
        <translation>LMP 버전</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="74"/>
        <source>LMP Subversion</source>
        <translation>LMP 서브버전</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="79"/>
        <source>Manufacturer</source>
        <translation>제조업체</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="84"/>
        <source>HCI Version</source>
        <translation>HCI 버전</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="89"/>
        <source>HCI Revision</source>
        <translation>HCI 리비전</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="94"/>
        <source>Is Local Adapter</source>
        <translation>로컬 아답터 여부</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="126"/>
        <source>All Interfaces</source>
        <translation>모든 인터페이스</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="134"/>
        <source>Show information steps</source>
        <translation>정보 단계 표시</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="146"/>
        <location filename="bluetooth_devices_dialog.cpp" line="373"/>
        <source>%1 items; Right click for more option; Double click for device details</source>
        <translation>%1 항목; 더 많은 옵션을 위해 마우스 오른쪽 버튼 클릭; 장치 세부정보를 위해 더블 클릭</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="163"/>
        <source>Copy Cell</source>
        <translation>셀 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="168"/>
        <source>Copy Rows</source>
        <translation>행 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="173"/>
        <source>Copy All</source>
        <translation>모두 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="178"/>
        <source>Save as image</source>
        <translation>그림으로 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="183"/>
        <location filename="bluetooth_devices_dialog.ui" line="186"/>
        <source>Mark/Unmark Row</source>
        <translation>행 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="189"/>
        <source>Ctrl-M</source>
        <translation>Ctrl-M</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.ui" line="194"/>
        <source>Mark/Unmark Cell</source>
        <translation>셀 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.cpp" line="334"/>
        <location filename="bluetooth_devices_dialog.cpp" line="354"/>
        <source>true</source>
        <translation>true</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.cpp" line="442"/>
        <source>Save Table Image</source>
        <translation>표 그림 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_devices_dialog.cpp" line="444"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG 그림 (*.png)</translation>
    </message>
</context>
<context>
    <name>BluetoothHciSummaryDialog</name>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="20"/>
        <source>Bluetooth HCI Summary</source>
        <translation>블루투스 HCI 요약</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="84"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="89"/>
        <source>OGF</source>
        <translation>OGF</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="94"/>
        <source>OCF</source>
        <translation>OCF</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="99"/>
        <source>Opcode</source>
        <translation>Opcode</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="104"/>
        <source>Event</source>
        <translation>이벤트</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="109"/>
        <source>Subevent</source>
        <translation>하위이벤트</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="114"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="486"/>
        <source>Status</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="119"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="518"/>
        <source>Reason</source>
        <translation>원인</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="124"/>
        <source>Hardware Error</source>
        <translation>하드웨어 오류</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="129"/>
        <source>Occurrence</source>
        <translation>출현지점</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="134"/>
        <source>Link Control Commands</source>
        <translation>연결 제어 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="137"/>
        <source>0x01</source>
        <translation>0x01</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="161"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="193"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="225"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="257"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="289"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="321"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="353"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="385"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="417"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="449"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="481"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="513"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="545"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="577"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="166"/>
        <source>Link Policy Commands</source>
        <translation>연결 정책 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="169"/>
        <source>0x02</source>
        <translation>0x02</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="198"/>
        <source>Controller &amp; Baseband Commands</source>
        <translation>제어기 및 기저대 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="201"/>
        <source>0x03</source>
        <translation>0x03</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="230"/>
        <source>Informational Parameters</source>
        <translation>정보 인자</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="233"/>
        <source>0x04</source>
        <translation>0x04</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="262"/>
        <source>Status Parameters</source>
        <translation>상태 인자</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="265"/>
        <source>0x05</source>
        <translation>0x05</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="294"/>
        <source>Testing Commands</source>
        <translation>테스트 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="297"/>
        <source>0x06</source>
        <translation>0x06</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="326"/>
        <source>LE Controller Commands</source>
        <translation>LE 제어기 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="329"/>
        <source>0x08</source>
        <translation>0x08</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="358"/>
        <source>Bluetooth Logo Testing Commands</source>
        <translation>블루투스 로고 테스트 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="361"/>
        <source>0x3E</source>
        <translation>0x3E</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="390"/>
        <source>Vendor-Specific Commands</source>
        <translation>제조사 지정 명령</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="393"/>
        <source>0x3F</source>
        <translation>0x3F</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="422"/>
        <source>Unknown OGF</source>
        <translation>알 수 없는 OGF</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="454"/>
        <source>Events</source>
        <translation>행사</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="550"/>
        <source>Hardware Errors</source>
        <translation>하드웨어 오류</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="590"/>
        <source>Results filter:</source>
        <translation>결과 필터:</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="613"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="636"/>
        <source>All Interfaces</source>
        <translation>모든 인터페이스</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="651"/>
        <source>All Adapters</source>
        <translation>모든 아답터</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="678"/>
        <source>Copy Cell</source>
        <translation>셀 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="683"/>
        <source>Copy Rows</source>
        <translation>행 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="688"/>
        <source>Copy All</source>
        <translation>모두 복사</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="693"/>
        <source>Save as image</source>
        <translation>그림으로 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="698"/>
        <location filename="bluetooth_hci_summary_dialog.ui" line="701"/>
        <source>Mark/Unmark Row</source>
        <translation>행 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="704"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.ui" line="709"/>
        <source>Mark/Unmark Cell</source>
        <translation>셀 표식/표식해제</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="360"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="521"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="527"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="612"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="378"/>
        <source>Adapter %1</source>
        <translation>아답터 %1</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="469"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="542"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="597"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="617"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="661"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="696"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="735"/>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="772"/>
        <source>Frame %1</source>
        <translation>프레임 %1</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="684"/>
        <source>Pending</source>
        <translation>보류</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="886"/>
        <source>Save Table Image</source>
        <translation>표 그림 저장</translation>
    </message>
    <message>
        <location filename="bluetooth_hci_summary_dialog.cpp" line="888"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG 그림 (*.png)</translation>
    </message>
</context>
<context>
    <name>ByteViewTab</name>
    <message>
        <location filename="byte_view_tab.cpp" line="39"/>
        <source>Packet bytes</source>
        <translation>패킷 바이트</translation>
    </message>
</context>
<context>
    <name>ByteViewText</name>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="90"/>
        <source>Allow hover highlighting</source>
        <translation>호버 강조 표시를 허용</translation>
    </message>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="101"/>
        <source>Show bytes as hexadecimal</source>
        <translation>16진수로 바이트 표시</translation>
    </message>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="105"/>
        <source>…as bits</source>
        <translation>비트 단위로 표시</stranslation>
    </message>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="115"/>
        <source>Show text based on packet</source>
        <translation>패킷상의 텍스트 표시</translation>
    </message>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="119"/>
        <source>…as ASCII</source>
        <translation>ASCII로 보기</translation>
    </message>
    <message>
        <location filename="widgets/byte_view_text.cpp" line="123"/>
        <source>…as EBCDIC</source>
        <translation>EBCDIC로 보기</translation>
    </message>
</context>
<context>
    <name>CaptureFile</name>
    <message>
        <location filename="capture_file.cpp" line="279"/>
        <source> [closing]</source>
        <translation> [닫는중]</translation>
    </message>
    <message>
        <location filename="capture_file.cpp" line="283"/>
        <source> [closed]</source>
        <translation> [닫힘]</translation>
    </message>
</context>
<context>
    <name>CaptureFileDialog</name>
    <message>
        <location filename="capture_file_dialog.cpp" line="148"/>
        <source>This capture file contains comments.</source>
        <translation>이 캡쳐 파일은 주석이 포함되어있습니다.</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="158"/>
        <source>The file format you chose doesn&apos;t support comments. Do you want to save the capture in a format that supports comments or discard the comments and save in the format you chose?</source>
        <translation>선택하신 파일 형식은 주석을 지원하지 않습니다.주석을 지원하는 형식으로 캡쳐한 내용을 저장하시겠습니까? 아니면 주석을 제외하고, 선택하신 형식으로 저장하시겠습니까?</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="162"/>
        <location filename="capture_file_dialog.cpp" line="171"/>
        <source>Discard comments and save</source>
        <translation>주석 제외 후 저장</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="163"/>
        <source>Save in another format</source>
        <translation>다른 형식으로 저장</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="168"/>
        <source>No file format in which it can be saved supports comments. Do you want to discard the comments and save in the format you chose?</source>
        <translation>저장할 수 있는 파일 형식이 주석을 지원하지 않습니다. 주석을 제외하고 선택한 형식으로 저장하시겠습니까?？</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="471"/>
        <source>All Files (</source>
        <translation>모든 파일 (</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="477"/>
        <source>All Capture Files</source>
        <translation>모든 캡쳐 파일</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="595"/>
        <source>Format:</source>
        <translation>형식:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="600"/>
        <source>Size:</source>
        <translation>크기:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="605"/>
        <source>Start / elapsed:</source>
        <translation>시작 / 경과:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="654"/>
        <source>Automatically detect file type</source>
        <translation>파일 유형을 자동으로 감지</translation>
    </message>
    <message numerus="yes">
        <source>%1, error after %Ln packet(s)</source>
        <oldsource>%1, error after %2 packets</oldsource>
        <translation type="vanished"><numerusform>%Ln 패킷 다음에서 %1, 오류</numerusform></translation>
    </message>
    <message numerus="yes">
        <source>%1, timed out at %Ln packet(s)</source>
        <oldsource>%1, timed out at %2 packets</oldsource>
        <translation type="vanished"><numerusform>%Ln 패킷에서 %1, 시간 초과</numerusform></translation>
    </message>
    <message numerus="yes">
        <source>%1, %Ln packet(s)</source>
        <translation type="vanished"><numerusform>%1, %Ln 패킷</numerusform></translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="617"/>
        <source>Prepend packets</source>
        <translation>앞쪽에 패킷 넣기</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="618"/>
        <source>Insert packets from the selected file before the current file. Packet timestamps will be ignored.</source>
        <translation>선택한 파일의 패킷을 현재 파일 앞에 추가합니다. 패킷의 타임스탬프는 무시될 것입니다.</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="621"/>
        <source>Merge chronologically</source>
        <translation>시계열 병합</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="622"/>
        <source>Insert packets in chronological order.</source>
        <translation>시계열 순으로 패킷을 추가합니다.</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="626"/>
        <source>Append packets</source>
        <translation>뒤쪽에 패킷 넣기</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="627"/>
        <source>Insert packets from the selected file after the current file. Packet timestamps will be ignored.</source>
        <translation>현재 파일 뒤쪽에 선택한 파일의 패킷을 추가합니다. 타임스탬프는 무시될 것입니다.</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="642"/>
        <source>Read filter:</source>
        <translation>읽기 필터:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="672"/>
        <source>Compress with g&amp;zip</source>
        <translation>gzip 형식으로 압축(&amp;z)</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="280"/>
        <location filename="capture_file_dialog.cpp" line="705"/>
        <source>Open Capture File</source>
        <oldsource>Wireshark: Open Capture File</oldsource>
        <translation>캡쳐 파일 열기</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="297"/>
        <location filename="capture_file_dialog.cpp" line="737"/>
        <source>Save Capture File As</source>
        <oldsource>Wireshark: Save Capture File As</oldsource>
        <translation>다른 이름으로 캡쳐 파일 저장</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="743"/>
        <source>Save as:</source>
        <translation>다른 이름으로 저장:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="314"/>
        <location filename="capture_file_dialog.cpp" line="780"/>
        <source>Export Specified Packets</source>
        <oldsource>Wireshark: Export Specified Packets</oldsource>
        <translation>지정된 패킷 내보내기</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="784"/>
        <source>Export as:</source>
        <translation>다른 이름으로 내보내기:</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="336"/>
        <location filename="capture_file_dialog.cpp" line="828"/>
        <source>Merge Capture File</source>
        <oldsource>Wireshark: Merge Capture File</oldsource>
        <translation>캡쳐 파일 병합</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="767"/>
        <location filename="capture_file_dialog.cpp" line="817"/>
        <source>Unknown file type returned by save as dialog.</source>
        <translation>다른 이름으로 저장 대화 상자에서 알 수 없는 파일 형식이 반환되었습니다.</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="768"/>
        <location filename="capture_file_dialog.cpp" line="818"/>
        <source>Please report this as a Wireshark issue at https://gitlab.com/wireshark/wireshark/-/issues.</source>
        <translation>이 문제를 Wireshark 이슈로서 보고해주십시오. https://gitlab.com/wireshark/wireshark/-/issues</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="931"/>
        <source>directory</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="938"/>
        <source>unknown file format</source>
        <translation>알 수 없는 파일 형식</translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="940"/>
        <source>error opening file</source>
        <translation>파일 열기 오류</translation>
    </message>
    <message numerus="yes">
        <location filename="capture_file_dialog.cpp" line="963"/>
        <source>%1, error after %Ln data record(s)</source>
        <oldsource>%1, error after %Ln record(s)</oldsource>
        <translation><numerusform>%1, %Ln 데이터 기록 다음에 오류</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="capture_file_dialog.cpp" line="970"/>
        <source>%1, timed out at %Ln data record(s)</source>
        <translation><numerusform>%1, %Ln 데이터 기록에서 시간초과</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="capture_file_dialog.cpp" line="973"/>
        <source>%1, %Ln data record(s)</source>
        <translation><numerusform>%1, %Ln 데이터 기록</numerusform></translation>
    </message>
    <message>
        <location filename="capture_file_dialog.cpp" line="999"/>
        <location filename="capture_file_dialog.cpp" line="1023"/>
        <source>unknown</source>
        <translation>알 수 없음</translation>
    </message>
</context>
<context>
    <name>CaptureFilePropertiesDialog</name>
    <message>
        <location filename="capture_file_properties_dialog.ui" line="39"/>
        <source>Details</source>
        <translation>상세 정보</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.ui" line="57"/>
        <source>Capture file comments</source>
        <translation>캡쳐 파일 의견</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="48"/>
        <source>Refresh</source>
        <translation>새로 고침</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="53"/>
        <source>Copy To Clipboard</source>
        <translation>클립 보드로 복사</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="58"/>
        <source>Save Comments</source>
        <translation>주석 저장</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="66"/>
        <source>Capture File Properties</source>
        <translation>캡쳐 파일 속성</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="157"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="160"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="164"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="169"/>
        <source>Length</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="174"/>
        <source>Hash (SHA256)</source>
        <translation>해시 (SHA256)</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="179"/>
        <source>Hash (RIPEMD160)</source>
        <translation>해시 (RIPEMD160)</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="184"/>
        <source>Hash (SHA1)</source>
        <translation>해시 (SHA1)</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="194"/>
        <source>Format</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="208"/>
        <source>Encapsulation</source>
        <translation>캡슐화</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="214"/>
        <source>Snapshot length</source>
        <translation>스냅샷 길이</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="225"/>
        <source>Time</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="230"/>
        <source>First packet</source>
        <translation>처음 패킷</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="236"/>
        <source>Last packet</source>
        <translation>마지막 패킷</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="256"/>
        <source>Elapsed</source>
        <translation>경과 시간</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="271"/>
        <source>Section %1</source>
        <translation>%1 부분</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="275"/>
        <source>Capture</source>
        <translation>캡쳐</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="290"/>
        <source>Hardware</source>
        <translation>하드웨어</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="301"/>
        <source>OS</source>
        <translation>운영체제</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="312"/>
        <source>Application</source>
        <translation>응용프로그램</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="321"/>
        <source>Interfaces</source>
        <translation>인터페이스</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="325"/>
        <source>Interface</source>
        <translation>인터페이스</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="326"/>
        <source>Dropped packets</source>
        <translation>누락된 패킷</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="327"/>
        <source>Capture filter</source>
        <translation>캡쳐 필터</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="328"/>
        <source>Link type</source>
        <translation>연결 유형</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="329"/>
        <source>Packet size limit (snaplen)</source>
        <translation>패킷 크기 제한 (snaplen)</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="358"/>
        <source>none</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="361"/>
        <source>%1 bytes</source>
        <translation>%1 바이트</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="377"/>
        <source>Statistics</source>
        <translation>통계</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="381"/>
        <source>Measurement</source>
        <translation>측정</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="382"/>
        <source>Captured</source>
        <translation>캡쳐됨</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="383"/>
        <source>Displayed</source>
        <translation>보여짐</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="384"/>
        <source>Marked</source>
        <translation>표식됨</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="404"/>
        <source>Packets</source>
        <translation>패킷 수</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="422"/>
        <source>Time span, s</source>
        <translation>시간 간격, 초</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="440"/>
        <source>Average pps</source>
        <translation>초당 평균 패킷</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="458"/>
        <source>Average packet size, B</source>
        <translation>평균 패킷 크기, 바이트</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="477"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="498"/>
        <source>Average bytes/s</source>
        <translation>초당 평균 바이트</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="519"/>
        <source>Average bits/s</source>
        <translation>초당 평균 비트 수</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="552"/>
        <source>Section Comment</source>
        <translation>부분 의견</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="562"/>
        <source>Packet Comments</source>
        <translation>패킷 주석</translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="573"/>
        <source>&lt;p&gt;Frame %1: </source>
        <translation>&lt;p&gt;프레임 %1: </translation>
    </message>
    <message>
        <location filename="capture_file_properties_dialog.cpp" line="644"/>
        <source>Created by Wireshark %1

</source>
        <translation>Wireshark %1 에 의해 만들어졌습니다

</translation>
    </message>
</context>
<context>
    <name>CaptureFilterCombo</name>
    <message>
        <location filename="widgets/capture_filter_combo.cpp" line="38"/>
        <source>Capture filter selector</source>
        <translation>캡쳐 필터 선택자</translation>
    </message>
</context>
<context>
    <name>CaptureFilterEdit</name>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="127"/>
        <source>Capture filter entry</source>
        <translation>캡쳐 필터 항목</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="140"/>
        <source>Manage saved bookmarks.</source>
        <translation>저장한 북마크를 관리합니다.</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="176"/>
        <source>Apply this filter string to the display.</source>
        <translation>디스플레이에 이 문자열을 적용합니다.</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="292"/>
        <source>Multiple filters selected. Override them here or leave this blank to preserve them.</source>
        <extracomment>This is a very long concept that needs to fit into a short space.</extracomment>
        <translation>여러 필터가 선택되었습니다. 여기에 덮어쓰거나 보존하기 위해 빈 열으로 남겨두십시오.</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="293"/>
        <source>&lt;p&gt;The interfaces you have selected have different capture filters. Typing a filter here will override them. Doing nothing will preserve them.&lt;/p&gt;</source>
        <translation>&lt;p&gt;선택한 인터페이스에 다른 캡쳐 필터가 있습니다. 여기에 필터를 입력하면 덮어씁니다. 아무것도 하지 않으면 그것들을 보존할 것입니다.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="297"/>
        <source>Enter a capture filter %1</source>
        <translation>%1 캡쳐 필터 입력</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="393"/>
        <source>Save this filter</source>
        <translation>이 필터 저장</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="395"/>
        <source>Remove this filter</source>
        <translation>이 필터 삭제</translation>
    </message>
    <message>
        <location filename="widgets/capture_filter_edit.cpp" line="397"/>
        <source>Manage Capture Filters</source>
        <translation>캡쳐 필터 관리</translation>
    </message>
</context>
<context>
    <name>CaptureInfoDialog</name>
    <message>
        <location filename="capture_info_dialog.cpp" line="95"/>
        <source>Capture Information</source>
        <translation>캡쳐 정보</translation>
    </message>
    <message>
        <location filename="capture_info_dialog.cpp" line="98"/>
        <source>Stop Capture</source>
        <translation>캡쳐 정지</translation>
    </message>
    <message>
        <location filename="capture_info_dialog.cpp" line="118"/>
        <source>%1 packets, %2:%3:%4</source>
        <translation>%1 패킷, %2:%3:%4</translation>
    </message>
</context>
<context>
    <name>CaptureInfoModel</name>
    <message>
        <location filename="capture_info_dialog.cpp" line="194"/>
        <source>Other</source>
        <translation>기타</translation>
    </message>
</context>
<context>
    <name>CaptureOptionsDialog</name>
    <message>
        <location filename="capture_options_dialog.ui" line="21"/>
        <source>Input</source>
        <translation>입력</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="48"/>
        <source>Interface</source>
        <translation>인터페이스</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="53"/>
        <source>Traffic</source>
        <translation>트래픽</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="58"/>
        <source>Link-layer Header</source>
        <translation>링크 레이어 헤더</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="63"/>
        <source>Promiscuous</source>
        <translation>무차별</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="68"/>
        <source>Snaplen (B)</source>
        <translation>Snaplen (B)</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="73"/>
        <source>Buffer (MB)</source>
        <translation>버퍼 (MB)</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="78"/>
        <source>Monitor Mode</source>
        <translation>감시 모드</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="83"/>
        <source>Capture Filter</source>
        <translation>캡쳐 필터</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You probably want to enable this. Usually a network card will only capture the traffic sent to its own network address. If you want to capture all traffic that the network card can &amp;quot;see&amp;quot;, mark this option. See the FAQ for some more details of capturing packets from a switched network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;이 기능을 사용 가능으로 설정해야 할 수 있습니다. 일반적으로 네트워크 카드는 자신의 네트워크 주소로 전송된 트래픽만 캡쳐합니다. 네트워크 카드가 &amp;quot;볼 수 있는&amp;quot; 모든 트래픽을 캡쳐하려면 이 선택사항을 지정하십시오. 전환된 네트워크로부터의 패킷 캡쳐링에 대한 자세한 내용은 FAQ를 참조하십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="96"/>
        <source>Enable promiscuous mode on all interfaces</source>
        <translation>모든 인터페이스에서 무차별 모드 사용</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="119"/>
        <source>Show and hide interfaces, add comments, and manage pipes and remote interfaces.</source>
        <translation>인터페이스를 표시하거나 숨기고, 설명을 추가하거나, 파이프 및 원격 인터페이스를 관리합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="122"/>
        <source>Manage Interfaces…</source>
        <translation>인터페이스 관리…</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="133"/>
        <source>Capture filter for selected interfaces:</source>
        <translation>선택한 인터페이스에 대한 캡쳐 필터:</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="163"/>
        <source>Compile BPFs</source>
        <translation>BPF 컴파일</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="173"/>
        <source>Output</source>
        <translation>출력</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="182"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter the file name to which captured data will be written. By default, a temporary file will be used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;기본적으로 임시파일을 사용하기 때문에, 별도로 캡쳐한 데이터에 대한 파일 이름을 입력하십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="188"/>
        <source>Capture to a permanent file</source>
        <translation>영구 파일로 캡쳐</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="200"/>
        <source>File:</source>
        <translation>파일:</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="210"/>
        <location filename="capture_options_dialog.ui" line="931"/>
        <source>Browse…</source>
        <translation>찾아보기…</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="222"/>
        <source>Output format:</source>
        <translation>출력 형식:</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="229"/>
        <source>pcapng</source>
        <translation>pcapng</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="236"/>
        <source>pcap</source>
        <translation>pcap</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="258"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Instead of using a single capture file, multiple files will be created.&lt;/p&gt;&lt;p&gt;The generated file names will contain an incrementing number and the start time of the capture.&lt;/p&gt;&lt;p&gt;NOTE: If enabled, at least one of the new-file criteria MUST be selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;1개의 캡쳐 파일을 사용하는 대신 여러 파일이 생성됩니다.&lt;/p&gt;&lt;p&gt;생성된 파일 이름은 일련 번호와 캡쳐 시작 시간을 포함합니다.&lt;/p&gt;&lt;p&gt;참고: 활성화된 경우, 새로운 파일 기준 중 하나 이상을 선택해야합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="261"/>
        <source>Create a new file automatically…</source>
        <translation>새 파일 자동으로 생성하기…</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="333"/>
        <location filename="capture_options_dialog.ui" line="388"/>
        <location filename="capture_options_dialog.ui" line="446"/>
        <source>after</source>
        <translation>패킷 이후</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="330"/>
        <location filename="capture_options_dialog.ui" line="369"/>
        <source>Switch to the next file after the specified number of packets have been captured.</source>
        <translation>지정한 양의 패킷을 캡쳐 한 후, 다음 파일로 전환합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="340"/>
        <location filename="capture_options_dialog.ui" line="762"/>
        <source>packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="273"/>
        <location filename="capture_options_dialog.ui" line="385"/>
        <location filename="capture_options_dialog.ui" line="421"/>
        <source>Switch to the next file after the file size exceeds the specified file size.</source>
        <translation>지정한 파일 크기를 초과한 후에는 다음 파일로 전환합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="277"/>
        <location filename="capture_options_dialog.ui" line="838"/>
        <source>kilobytes</source>
        <translation>킬로 바이트</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="282"/>
        <location filename="capture_options_dialog.ui" line="843"/>
        <source>megabytes</source>
        <translation>메가 바이트</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="287"/>
        <location filename="capture_options_dialog.ui" line="848"/>
        <source>gigabytes</source>
        <translation>기가 바이트</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="295"/>
        <location filename="capture_options_dialog.ui" line="347"/>
        <location filename="capture_options_dialog.ui" line="443"/>
        <source>Switch to the next file when the time capturing to the current file exceeds the specified time.</source>
        <translation>현재 파일을 캡쳐하는 시간이 지정된 시간을 초과했을 때 다음 파일로 바꿉니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="299"/>
        <location filename="capture_options_dialog.ui" line="403"/>
        <location filename="capture_options_dialog.ui" line="886"/>
        <source>seconds</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="304"/>
        <location filename="capture_options_dialog.ui" line="408"/>
        <location filename="capture_options_dialog.ui" line="891"/>
        <source>minutes</source>
        <translation>분</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="309"/>
        <location filename="capture_options_dialog.ui" line="413"/>
        <location filename="capture_options_dialog.ui" line="896"/>
        <source>hours</source>
        <translation>시</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="457"/>
        <source>when time is a multiple of</source>
        <translation>시간이 배수인 경우</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="395"/>
        <location filename="capture_options_dialog.ui" line="453"/>
        <location filename="capture_options_dialog.ui" line="464"/>
        <source>Switch to the next file when the (wall clock) time is an even multiple of the specified interval.
For example, use 1 hour to have a new file created every hour on the hour.</source>
        <translation>지정한 주기마다 다음 파일로 전환합니다.
예를 들어, 1시간을 설정하면 매시간마다 새로운 파일이 만들어집니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="487"/>
        <source>compression</source>
        <translation>압축</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="493"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="503"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="521"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;After capturing has switched to the next file and the given number of files has exceeded, the oldest file will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;다음 파일로 캡쳐링이 전환된 후, 생성하기로 지정한 파일 수를 초과하면 가장 오래된 파일을 지울 것입니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="524"/>
        <source>Use a ring buffer with </source>
        <translation>다음과 함께 링 버퍼 사용</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="547"/>
        <location filename="capture_options_dialog.ui" line="801"/>
        <source>files</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="585"/>
        <source>Options</source>
        <translation>선택사항</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="593"/>
        <source>Display Options</source>
        <translation>선택사항 표시</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="602"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Using this option will show the captured packets immediately on the main screen. Please note: this will slow down capturing, so increased packet drops might appear.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;이 선택 사항을 사용하면 기본화면에서 바로 캡쳐한 패킷들을 표시합니다. 주의: 이 선택사항은 캡쳐 동작을 느리게 할 수 있으므로 표시된 패킷 외에 버려지는 패킷이 발생할 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="605"/>
        <source>Update list of packets in real-time</source>
        <translation>실시간으로 패킷 목록을 새로 고침</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="612"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will scroll the &amp;quot;Packet List&amp;quot; automatically to the latest captured packet, when the &amp;quot;Update list of packets in real-time&amp;quot; option is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&amp;quot;실시간으로 패킷 목록을 새로 고침&amp;quot; 선택 사항을 사용하면 &amp;quot;패킷 내역&amp;quot;이 가장 최신 캡쳐된 패킷으로 자동 갱신됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="615"/>
        <source>Automatically scroll during live capture</source>
        <translation>캡쳐 중에 자동 스크롤</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="622"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show the capture info dialog while capturing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;패킷 캡쳐 중에 캡쳐링 정보 대화 상자를 표시합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="625"/>
        <source>Show capture information during live capture</source>
        <translation>캡쳐하면서 캡쳐 정보 화면을 표시</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="648"/>
        <source>Name Resolution</source>
        <translation>이름 해석</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="657"/>
        <source>Perform MAC layer name resolution while capturing.</source>
        <translation>캡쳐링 중 MAC 레이어 주소를 호스트 이름으로 해석하는 동작을 수행합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="660"/>
        <source>Resolve MAC addresses</source>
        <translation>MAC 주소 해석</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform network layer name resolution while capturing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;캡쳐중에 네트워크 계층의 이름 분석을 수행합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="670"/>
        <source>Resolve network names</source>
        <translation>네트워크 이름 해석</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="677"/>
        <source>Perform transport layer name resolution while capturing.</source>
        <translation>캡쳐중에 전송계층 이름 해석을 실시합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="680"/>
        <source>Resolve transport names</source>
        <translation>전송계층 이름 해석</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="708"/>
        <source>Stop capture automatically after…</source>
        <translation>다음 이후 자동으로 캡쳐를 정지</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="730"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stop capturing after the specified number of packets have been captured.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;지정된 개수의 패킷을 캡쳐한 후 캡쳐를 정지합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="746"/>
        <location filename="capture_options_dialog.ui" line="785"/>
        <source>Stop capturing after the specified number of packets have been captured.</source>
        <translation>지정된 갯수의 패킷을 캡쳐한 후 캡쳐를 정지합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="769"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stop capturing after the specified number of files have been created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;지정된 갯수의 파일을 생성하면, 캡쳐를 정지합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="808"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stop capturing after the specified amount of data has been captured.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;지정된 수량의 데이터를 캡쳐하면, 캡쳐를 정지합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="818"/>
        <location filename="capture_options_dialog.ui" line="834"/>
        <source>Stop capturing after the specified amount of data has been captured.</source>
        <translation>지정된 수량의 데이터를 캡쳐하면 캡쳐를 정지합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="856"/>
        <location filename="capture_options_dialog.ui" line="866"/>
        <location filename="capture_options_dialog.ui" line="882"/>
        <source>Stop capturing after the specified amount of time has passed.</source>
        <translation>지정 시간이 지난 뒤 캡쳐를 정지합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="910"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optionally specify a temporary directory for unnamed capture files.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;선택적으로 이름이 지정되지 않은 캡쳐 파일의 임시 디렉터리를 지정합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.ui" line="916"/>
        <source>Directory for temporary files</source>
        <translation>임시 파일을 위한 디렉터리</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="184"/>
        <source>Capture Options</source>
        <translation>캡쳐 선택 사항</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="189"/>
        <source>Start</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="212"/>
        <source>Leave blank to use a temporary file</source>
        <translation>임시 파일 사용을 위해 공백으로 남겨둠</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="381"/>
        <source>Specify a Capture File</source>
        <translation>캡쳐 파일 지정</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="387"/>
        <source>Specify temporary directory</source>
        <translation>임시 디렉터리를 지정</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="809"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="809"/>
        <source>Addresses</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="809"/>
        <source>Address</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="819"/>
        <source>no addresses</source>
        <translation>주소 없음</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="1027"/>
        <location filename="capture_options_dialog.cpp" line="1035"/>
        <location filename="capture_options_dialog.cpp" line="1046"/>
        <location filename="capture_options_dialog.cpp" line="1053"/>
        <location filename="capture_options_dialog.cpp" line="1066"/>
        <location filename="capture_options_dialog.cpp" line="1074"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="1028"/>
        <location filename="capture_options_dialog.cpp" line="1036"/>
        <location filename="capture_options_dialog.cpp" line="1067"/>
        <location filename="capture_options_dialog.cpp" line="1075"/>
        <source>Multiple files: Requested filesize too large. The filesize cannot be greater than 2 GiB.</source>
        <translation>여러 파일: 요청하신 파일 크기가 너무 큽니다. 파일 크기는 2GB보다 클 수 없습니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="1047"/>
        <source>Multiple files: No capture file name given. You must specify a filename if you want to use multiple files.</source>
        <translation>여러 파일: 캡쳐 파일 이름이 없습니다. 여러 파일을 사용하려면 파일 이름을 지정해야 합니다.</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="1054"/>
        <source>Multiple files: No file limit given. You must specify a file size, interval, or number of packets for each file.</source>
        <translation>여러 파일: 주어진 파일 제한 용량이 없습니다. 각 파일별 파일 제한 크기나, 생성시간주기, 패킷의 갯수를 지정해야 합니다.</translation>
    </message>
</context>
<context>
    <name>CapturePreferencesFrame</name>
    <message>
        <location filename="capture_preferences_frame.ui" line="20"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="29"/>
        <source>Default interface</source>
        <translation>기본 인터페이스</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="63"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You probably want to enable this. Usually a network card will only capture the traffic sent to its own network address. If you want to capture all traffic that the network card can &amp;quot;see&amp;quot;, mark this option. See the FAQ for some more details of capturing packets from a switched network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;이 옵션을 사용하길 원하실 겁니다. 일반적으로 네트워크 카드는 자체 네트워크 주소로 송신된 트래픽만 캡쳐합니다. 네트워크 카드가 &amp;quot;참조&amp;quot; 할 수 있는 모든 트래픽을 캡쳐하려면 이 선택 사항을 표시하십시오. 전환된 네트워크에서 패킷을 캡쳐하는 방법에 대한 자세한 내용은 FAQ를 참조하십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="66"/>
        <source>Capture packets in promiscuous mode</source>
        <translation>무차별 모드로 패킷을 캡쳐</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Capture packets in the next-generation capture file format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;다음 세대(Next Generation)의 캡쳐 파일 형식으로 패킷을 캡쳐합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="76"/>
        <source>Capture packets in pcapng format</source>
        <translation>pcapng 형식으로 패킷을 캡쳐합니다</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="83"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Update the list of packets while capture is in progress. This can result in dropped packets on high-speed networks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;캡쳐가 진행되는 동안 패킷목록을 갱신합니다. 이로인해 고속 네트워크에서는 패킷이 손실 될 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="86"/>
        <source>Update list of packets in real time</source>
        <translation>실시간으로 패킷 목록을 갱신</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Keep the packet list scrolled to the bottom while capturing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;캡쳐하는 동안 패킷 목록을 맨 아래로 스크롤한 상태로 유지합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="96"/>
        <source>Automatic scrolling in live capture</source>
        <translation>캡쳐 중에 자동 스크롤</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="103"/>
        <source>Don&apos;t load interfaces on startup</source>
        <translation>시작할 때 인터페이스를 불러오지 않기</translation>
    </message>
    <message>
        <location filename="capture_preferences_frame.ui" line="110"/>
        <source>Disable external capture interfaces</source>
        <translation>외부 캡쳐 인터페이스 사용 해제</translation>
    </message>
</context>
<context>
    <name>ColoringRulesDelegate</name>
    <message>
        <location filename="models/coloring_rules_delegate.cpp" line="74"/>
        <source>the &quot;@&quot; symbol will be ignored.</source>
        <translation>&quot;@&quot; 기호는 무시됩니다.</translation>
    </message>
</context>
<context>
    <name>ColoringRulesDialog</name>
    <message>
        <location filename="coloring_rules_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="51"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="63"/>
        <source>Add a new coloring rule.</source>
        <translation>새로운 색상 규칙을 추가합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="76"/>
        <source>Delete this coloring rule.</source>
        <translation>이 색상 규칙을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="89"/>
        <source>Duplicate this coloring rule.</source>
        <translation>이 색상 규칙을 복사합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="99"/>
        <source>Clear all coloring rules.</source>
        <translation>모든 색상 규칙을 지웁니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="109"/>
        <source>Set the foreground color for this rule.</source>
        <translation>이 규칙에 전경색을 설정합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="115"/>
        <source>Foreground</source>
        <translation>전경색</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="131"/>
        <source>Set the background color for this rule.</source>
        <translation>이 규칙에 배경색을 설정합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="137"/>
        <source>Background</source>
        <translation>배경색</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="153"/>
        <source>Set the display filter using this rule.</source>
        <translation>이 규칙을 사용할 표시 필터를 설정합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.ui" line="156"/>
        <source>Apply as filter</source>
        <translation>필터 적용</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="91"/>
        <source>Select a file and add its filters to the end of the list.</source>
        <translation>파일을 선택하고 목록의 말미에 필터를 추가합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="93"/>
        <source>Save filters in a file.</source>
        <translation>파일에 필터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="54"/>
        <source>Coloring Rules %1</source>
        <translation>색상 규칙 %1</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="90"/>
        <source>Import…</source>
        <translation>가져오기...</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="92"/>
        <source>Export…</source>
        <translation>내보내기...</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="95"/>
        <source>Copy coloring rules from another profile.</source>
        <translation>다른 프로파일에서 색상 규칙을 복사합니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="103"/>
        <source>Open </source>
        <translation>열기 </translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="264"/>
        <source>Double click to edit. Drag to move. Rules are processed in order until a match is found.</source>
        <translation>더블 클릭하여 편집합니다. 끌어서 움직입니다. 규칙은 일치된 내용을 찾을때까지 순서대로 처리됩니다.</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="437"/>
        <source>Import Coloring Rules</source>
        <translation>색상 규칙 가져오기</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="456"/>
        <source>Export %1 Coloring Rules</source>
        <translation>%1 색상 규칙 내보내기</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="133"/>
        <source>Your coloring rules file contains unknown rules</source>
        <translation>색상 규칙 파일에 알 수 없는 규칙을 포함하고 있음</translation>
    </message>
    <message>
        <location filename="coloring_rules_dialog.cpp" line="134"/>
        <source>Wireshark doesn&apos;t recognize one or more of your coloring rules. They have been disabled.</source>
        <translation>Wireshark는 1개 이상의 색상 규칙을 인식하지 못합니다. 인식하지 못한 규칙들은 사용하지 않습니다.</translation>
    </message>
</context>
<context>
    <name>ColoringRulesModel</name>
    <message>
        <location filename="models/coloring_rules_model.cpp" line="142"/>
        <location filename="models/coloring_rules_model.cpp" line="203"/>
        <source>New coloring rule</source>
        <translation>새로운 색상 규칙</translation>
    </message>
    <message>
        <location filename="models/coloring_rules_model.cpp" line="184"/>
        <source>Unable to save coloring rules: %1</source>
        <translation>색상 규칙을 저장할 수 없습니다: %1</translation>
    </message>
    <message>
        <location filename="models/coloring_rules_model.cpp" line="397"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="models/coloring_rules_model.cpp" line="399"/>
        <source>Filter</source>
        <translation>필터</translation>
    </message>
</context>
<context>
    <name>ColumnEditorFrame</name>
    <message>
        <location filename="column_editor_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="32"/>
        <source>Title:</source>
        <oldsource>Title</oldsource>
        <translation>제목:</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="55"/>
        <source>Type:</source>
        <oldsource>Type</oldsource>
        <translation>유형:</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="78"/>
        <source>Fields:</source>
        <oldsource>Fields</oldsource>
        <translation>필드:</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="101"/>
        <source>Occurrence:</source>
        <oldsource>Occurrence</oldsource>
        <translation>출현지점:</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="111"/>
        <source>Resolve Names:</source>
        <translation>해석된 이름:</translation>
    </message>
    <message>
        <location filename="column_editor_frame.ui" line="114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;p&gt;Show human-readable strings instead of raw values for fields. Only applicable to custom columns with fields that have value strings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;p&gt;필드의 원시값 대신 사람이 읽을 수 있는 문자열로 표시합니다. 문자열 값을 갖는 필드가 있는 사용자 지정 칼럼에만 적용할 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="column_editor_frame.cpp" line="178"/>
        <source>Missing fields.</source>
        <translation>찾을 수 없는 필드입니다.</translation>
    </message>
    <message>
        <location filename="column_editor_frame.cpp" line="180"/>
        <source>Invalid fields.</source>
        <translation>유효하지 않은 필드입니다.</translation>
    </message>
    <message>
        <location filename="column_editor_frame.cpp" line="182"/>
        <source>Invalid occurrence value.</source>
        <translation>유효하지 않은 출현지점 값입니다.</translation>
    </message>
</context>
<context>
    <name>ColumnListModel</name>
    <message>
        <location filename="models/column_list_model.cpp" line="241"/>
        <source>Displayed</source>
        <translation>표시됨</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="243"/>
        <source>Title</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="245"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="247"/>
        <source>Fields</source>
        <translation>필드</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="249"/>
        <source>Field Occurrence</source>
        <translation>필드 출현지점</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="251"/>
        <source>Resolved</source>
        <translation>해석됨</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="321"/>
        <source>&lt;html&gt;Show human-readable strings instead of raw values for fields. Only applicable to custom columns with fields that have value strings.&lt;/html&gt;</source>
        <translation>&lt;html&gt;필드에 대한 원시값 대신 사람이 읽을 수 있는 문자열로 보여줍니다. 문자열 값을 갖는 필드를 가진 사용자 지정 칼럼에 대해서만 적용가능합니다.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="models/column_list_model.cpp" line="496"/>
        <source>New Column</source>
        <translation>새로운 열</translation>
    </message>
</context>
<context>
    <name>ColumnPreferencesFrame</name>
    <message>
        <location filename="column_preferences_frame.ui" line="20"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="column_preferences_frame.ui" line="50"/>
        <source>Add a new column</source>
        <translation>새로운 열 추가</translation>
    </message>
    <message>
        <location filename="column_preferences_frame.ui" line="60"/>
        <source>Delete selected column</source>
        <translation>선택한 열 제거</translation>
    </message>
    <message>
        <location filename="column_preferences_frame.ui" line="70"/>
        <source>Show displayed columns only</source>
        <translation>표시된 열만 보기</translation>
    </message>
    <message>
        <location filename="column_preferences_frame.cpp" line="120"/>
        <source>Reset all changes</source>
        <translation>모든 변경 사항 다시 설정</translation>
    </message>
</context>
<context>
    <name>CompiledFilterOutput</name>
    <message>
        <location filename="compiled_filter_output.ui" line="14"/>
        <source>Compiled Filter Output</source>
        <translation>컴파일된 필터 출력</translation>
    </message>
    <message>
        <location filename="compiled_filter_output.cpp" line="39"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="compiled_filter_output.cpp" line="40"/>
        <source>Copy filter text to the clipboard.</source>
        <translation>필터 텍스트를 클립 보드로 복사합니다.</translation>
    </message>
</context>
<context>
    <name>ConversationDataModel</name>
    <message>
        <location filename="models/atap_data_model.cpp" line="590"/>
        <source>Address A</source>
        <translation>주소 A</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="592"/>
        <source>Port A</source>
        <translation>포트 A</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="594"/>
        <source>Address B</source>
        <translation>주소 B</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="596"/>
        <source>Port B</source>
        <translation>포트 B</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="598"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="600"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="602"/>
        <source>Stream ID</source>
        <translation>스트림 ID</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="608"/>
        <source>Packets A </source>
        <translation>패킷 A</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="610"/>
        <source>Bytes A </source>
        <translation>바이트 A</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="612"/>
        <source>Packets B </source>
        <translation>패킷 B</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="614"/>
        <source>Bytes B </source>
        <translation>바이트 B</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="616"/>
        <source>Abs Start</source>
        <translation>Abs 시작</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="616"/>
        <source>Rel Start</source>
        <translation>Rel 시작</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="618"/>
        <source>Duration</source>
        <translation>지속시간</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="620"/>
        <source>Bits/s A </source>
        <translation>비트/초 A</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="622"/>
        <source>Bits/s B </source>
        <translation>비트/초 B</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="604"/>
        <source>Total Packets</source>
        <translation>전체 패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="606"/>
        <source>Percent Filtered</source>
        <translation>필터된 비율</translation>
    </message>
</context>
<context>
    <name>ConversationDialog</name>
    <message>
        <location filename="conversation_dialog.cpp" line="102"/>
        <source>Follow Stream…</source>
        <translation>스트림 따라가기…</translation>
    </message>
    <message>
        <location filename="conversation_dialog.cpp" line="103"/>
        <source>Follow a TCP or UDP stream.</source>
        <translation>TCP 또는 UDP 스트림을 따라갑니다.</translation>
    </message>
    <message>
        <location filename="conversation_dialog.cpp" line="106"/>
        <source>Graph…</source>
        <translation>그래프...</translation>
    </message>
    <message>
        <location filename="conversation_dialog.cpp" line="107"/>
        <source>Graph a TCP conversation.</source>
        <translation>TCP 대화를 그래프화 합니다.</translation>
    </message>
</context>
<context>
    <name>ConversationHashTablesDialog</name>
    <message>
        <location filename="conversation_hash_tables_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="conversation_hash_tables_dialog.cpp" line="108"/>
        <source>Conversation Hash Tables</source>
        <translation>대화 해시표</translation>
    </message>
</context>
<context>
    <name>CopyFromProfileButton</name>
    <message>
        <location filename="widgets/copy_from_profile_button.cpp" line="23"/>
        <source>Copy from</source>
        <translation>다음으로부터 복사</translation>
    </message>
    <message>
        <location filename="widgets/copy_from_profile_button.cpp" line="25"/>
        <source>Copy entries from another profile.</source>
        <translation>다른 프로파일로부터 항목을 복사합니다.</translation>
    </message>
    <message>
        <location filename="widgets/copy_from_profile_button.cpp" line="117"/>
        <source>System default</source>
        <translation>시스템 기본값</translation>
    </message>
</context>
<context>
    <name>CredentialsDialog</name>
    <message>
        <location filename="credentials_dialog.ui" line="14"/>
        <source>Wireshark - Credentials</source>
        <translation>Wireshark - 인증서</translation>
    </message>
    <message>
        <location filename="credentials_dialog.cpp" line="62"/>
        <source>Credentials</source>
        <translation>인증서</translation>
    </message>
</context>
<context>
    <name>CredentialsModel</name>
    <message>
        <location filename="models/credentials_model.cpp" line="82"/>
        <source>Click to select the packet</source>
        <translation>클릭하여 패킷을 선택합니다</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="91"/>
        <source>Click to select the packet with username</source>
        <translation>클릭하여 사용자 이름으로 패킷을 선택합니다</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="95"/>
        <source>Username not available</source>
        <translation>사용자 이름을 사용할 수 없습니다</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="144"/>
        <source>Packet No.</source>
        <translation>패킷 번호.</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="146"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="148"/>
        <source>Username</source>
        <translation>사용자 이름</translation>
    </message>
    <message>
        <location filename="models/credentials_model.cpp" line="150"/>
        <source>Additional Info</source>
        <translation>추가 정보</translation>
    </message>
</context>
<context>
    <name>DataPrinter</name>
    <message>
        <location filename="utils/data_printer.cpp" line="204"/>
        <source>Copy Bytes as Hex + ASCII Dump</source>
        <translation>16진수 + 아스키 덤프 형식으로 바이트를 복사</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="205"/>
        <source>Copy packet bytes as a hex and ASCII dump.</source>
        <translation>16진수 및 아스키 덤프 형식으로 패킷 바이트를 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="209"/>
        <source>…as Hex Dump</source>
        <translation>…16진수 덤프 형식</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="210"/>
        <source>Copy packet bytes as a hex dump.</source>
        <translation>16진수 덤프로 패킷 바이트를 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="214"/>
        <source>…as Printable Text</source>
        <translation>…인쇄가능한 텍스트</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="215"/>
        <source>Copy only the printable text in the packet.</source>
        <translation>패킷 내의 출력 가능한 텍스트만 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="219"/>
        <source>…as a Hex Stream</source>
        <translation>…16진수 스트림</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="220"/>
        <source>Copy packet bytes as a stream of hex.</source>
        <translation>16진수 스트림으로 패킷 바이트를 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="224"/>
        <source>…as a Base64 String</source>
        <translation>…Base64 문자열</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="225"/>
        <source>Copy packet bytes as a base64 encoded string.</source>
        <translation>base64 인코딩 문자열로 패킷 바이트를 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="229"/>
        <source>…as Raw Binary</source>
        <translation>…원시 바이너리</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="230"/>
        <source>Copy packet bytes as application/octet-stream MIME data.</source>
        <translation>MIME 데이터(application/octet-stream)로 패킷 바이트를 복사합니다.</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="234"/>
        <source>…as Escaped String</source>
        <translation>…예외문자열</translation>
    </message>
    <message>
        <location filename="utils/data_printer.cpp" line="235"/>
        <source>Copy packet bytes as an escaped string.</source>
        <translation>특수문자 예외문자열로 패킷 바이트를 복사합니다.</translation>
    </message>
</context>
<context>
    <name>DecodeAsDialog</name>
    <message>
        <location filename="decode_as_dialog.ui" line="26"/>
        <source>Change the dissection behavior for a protocol.</source>
        <translation>프로토콜에 대한 분해 행동을 변경합니다.</translation>
    </message>
    <message>
        <location filename="decode_as_dialog.ui" line="39"/>
        <source>Remove this dissection behavior.</source>
        <translation>이 분해 행동을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="decode_as_dialog.ui" line="46"/>
        <source>Copy this dissection behavior.</source>
        <translation>이 분해 행동을 복사합니다.</translation>
    </message>
    <message>
        <location filename="decode_as_dialog.ui" line="59"/>
        <source>Clear all dissection behaviors.</source>
        <translation>모든 분해 행동을 지웁니다.</translation>
    </message>
    <message>
        <location filename="decode_as_dialog.cpp" line="66"/>
        <source>Decode As…</source>
        <translation>다른 이름으로 디코딩…</translation>
    </message>
    <message>
        <location filename="decode_as_dialog.cpp" line="72"/>
        <source>Open </source>
        <translation>열기 </translation>
    </message>
</context>
<context>
    <name>DecodeAsModel</name>
    <message>
        <location filename="models/decode_as_model.cpp" line="102"/>
        <source>Match using this field</source>
        <translation>이 필드를 사용하여 비교</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="104"/>
        <source>Change behavior when the field matches this value</source>
        <translation>필드가 이 값과 일치했을 때 동작 변경</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="106"/>
        <source>Field value type (and base, if Integer)</source>
        <translation>필드 값 유형(및 밑수, 정수인 경우)</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="110"/>
        <source>Current&quot;Decode As&quot; behavior</source>
        <translation>현재&quot;다른 이름으로 디코딩&quot;의 동작</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="108"/>
        <source>Default &quot;Decode As&quot; behavior</source>
        <translation>기본 &quot;다른 이름으로 디코딩&quot;의 동작</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="143"/>
        <source>String</source>
        <translation>문자열</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="145"/>
        <source>Integer, base </source>
        <translation>정수형, 밑수 </translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="157"/>
        <source>unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="161"/>
        <source>&lt;none&gt;</source>
        <translation>&lt;없음&gt;</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="166"/>
        <source>GUID</source>
        <translation>GUID</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="193"/>
        <source>Field</source>
        <translation>필드</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="195"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="197"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="199"/>
        <source>Default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="models/decode_as_model.cpp" line="201"/>
        <source>Current</source>
        <translation>현재</translation>
    </message>
</context>
<context>
    <name>DisplayFilterCombo</name>
    <message>
        <location filename="widgets/display_filter_combo.cpp" line="46"/>
        <source>Display filter selector</source>
        <translation>표시 필터 선택기</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_combo.cpp" line="49"/>
        <source>Select from previously used filters.</source>
        <translation>이전에 사용한 사용한 필터로부터 선택합니다.</translation>
    </message>
</context>
<context>
    <name>DisplayFilterEdit</name>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="78"/>
        <source>Display filter entry</source>
        <translation>필터 항목 표시</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="101"/>
        <source>Manage saved bookmarks.</source>
        <translation>저장한 책갈피를 관리합니다.</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="162"/>
        <source>Display Filter Expression…</source>
        <translation>필터 표현식을 표시…</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="256"/>
        <source>Apply a display filter %1 &lt;%2/&gt;</source>
        <translation>%1 &lt;%2/&gt; 표시 필터 적용</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="261"/>
        <source>Enter a display filter %1</source>
        <translation>%1 표시 필터 입력</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="107"/>
        <source>Clear display filter</source>
        <translation>표시 필터 지움</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="114"/>
        <source>Apply display filter</source>
        <translation>표시 필터 적용</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="155"/>
        <source>Left align buttons</source>
        <translation>왼쪽 정렬 버튼</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="265"/>
        <source>Apply a read filter %1</source>
        <translation>%1 읽기 필터 적용</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="340"/>
        <source>Current filter: %1</source>
        <translation>현재 필터: %1</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="364"/>
        <source>Invalid filter: </source>
        <translation>유효하지 않은 필터: </translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="426"/>
        <source>Save this filter</source>
        <translation>이 필터 저장</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="428"/>
        <source>Remove this filter</source>
        <translation>이 필터 삭제</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="430"/>
        <source>Manage Display Filters</source>
        <translation>표시 필터 관리</translation>
    </message>
    <message>
        <location filename="widgets/display_filter_edit.cpp" line="432"/>
        <source>Filter Button Preferences...</source>
        <translation>필터 버튼 속성...</translation>
    </message>
</context>
<context>
    <name>DisplayFilterExpressionDialog</name>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="24"/>
        <source>Select a field to start building a display filter.</source>
        <translation>표시 필터를 구축하도록 시작하기 위한 필드를 선택하십시오.</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="27"/>
        <source>Field Name</source>
        <translation>필드 이름</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search the list of field names.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;필드 이름의 목록을 검색합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="54"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="72"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Relations can be used to restrict fields to specific values. Each relation does the following:&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;&quot; cellspacing=&quot;2&quot; cellpadding=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;is present&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Match any packet that contains this field&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;==, !=, etc.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Compare the field to a specific value.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;contains, matches&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Check the field against a string (contains) or a regular expression (matches)&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;in&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Compare the field to a specific set of values&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;

</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;관계는 필드를 지정된 값으로 제한하는데 사용할 수 있습니다. 각각의 관계는 다음과 같이 움직입니다:&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;&quot; cellspacing=&quot;2&quot; cellpadding=&quot;0&quot;&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;is present&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;이 필드에 적합한 어떤 패킷과도 일치&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;==, !=, 등등.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;지정된 값과 필드를 비교&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;contains, matches&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;문자열 혹은 정규표현식에 대하여 필드를 확인하십시오.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;in&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;필드를 값의 집합과 비교&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;

</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="77"/>
        <source>Relation</source>
        <translation>관계</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="91"/>
        <source>By default order comparisons and contains/matches/in relations are true if any value matches. The quantifier &quot;all&quot; can be used to apply the test to all values in a frame.</source>
        <translation>기본 순서에 따라 비교와 contains/matches/in 관계 순으로 어떤 값과 일치하면 참입니다. 수량자 &quot;all&quot; 은 프레임 내의 모든 값을 시험하도록 적용할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="94"/>
        <source>Quantifier</source>
        <translation>수량자</source>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="103"/>
        <source>Any</source>
        <translation>Any</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="113"/>
        <source>All</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="155"/>
        <source>Match against this value.</source>
        <translation>이 값과 일치합니다.</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="158"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="172"/>
        <source>If the field you have selected has a known set of valid values they will be listed here.</source>
        <translation>선택한 필드에 알려진 유효한 값 집합이 있는 경우 여기에 나열됩니다.</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="175"/>
        <source>Predefined Values</source>
        <translation>사전정의된 값</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="202"/>
        <source>If the field you have selected covers a range of bytes (e.g. you have selected a protocol) you can restrict the match to a range of bytes here.</source>
        <translation>선택한 필드가 바이트 범위를 다루는 경우(예: 프로토콜을 선택한 경우), 바이트 범위에 일치하는 것만 제한할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="205"/>
        <source>Range (offset:length)</source>
        <translation>범위 (오프셋:길이)</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="224"/>
        <source>No display filter</source>
        <translation>표시 필터 없음</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.ui" line="231"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.cpp" line="138"/>
        <source>Display Filter Expression</source>
        <translation>필터 표현식 표시</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.cpp" line="300"/>
        <source>Select a field name to get started</source>
        <translation>시작한 필드 이름 선택</translation>
    </message>
    <message>
        <location filename="display_filter_expression_dialog.cpp" line="304"/>
        <source>Click OK to insert this filter</source>
        <translation>이 필터를 추가하기 위해 확인을 클릭합니다</translation>
    </message>
</context>
<context>
    <name>DissectorTablesDialog</name>
    <message>
        <location filename="dissector_tables_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="dissector_tables_dialog.ui" line="22"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="dissector_tables_dialog.cpp" line="27"/>
        <source>Dissector Tables</source>
        <translation>해부자 표</translation>
    </message>
</context>
<context>
    <name>DissectorTablesProxyModel</name>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="325"/>
        <location filename="models/dissector_tables_model.cpp" line="400"/>
        <source>Table Type</source>
        <translation>표 유형</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="409"/>
        <source>String</source>
        <translation>문자열</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="410"/>
        <location filename="models/dissector_tables_model.cpp" line="413"/>
        <source>Dissector Description</source>
        <translation>해부자 기술</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="412"/>
        <source>Integer</source>
        <translation>정수형</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="415"/>
        <location filename="models/dissector_tables_model.cpp" line="426"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="416"/>
        <location filename="models/dissector_tables_model.cpp" line="427"/>
        <source>Short Name</source>
        <translation>짧은 이름</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="423"/>
        <source>Table Name</source>
        <translation>표 이름</translation>
    </message>
    <message>
        <location filename="models/dissector_tables_model.cpp" line="424"/>
        <source>Selector Name</source>
        <translation>선택기 이름</translation>
    </message>
</context>
<context>
    <name>EnabledProtocolsDialog</name>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="54"/>
        <source>&lt;small&gt;&lt;i&gt;Disabling a protocol prevents higher layer protocols from being displayed&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;프로토콜 사용하지 않기는 표시중인 내용에서 상위 계층 프로토콜을 방지합니다.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="22"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="35"/>
        <source>in</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="66"/>
        <source>Enable All</source>
        <translation>전체 사용</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="73"/>
        <source>Disable All</source>
        <translation>전체 해제</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.ui" line="80"/>
        <source>Invert</source>
        <translation>반전</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="31"/>
        <source>Enabled Protocols</source>
        <translation>허용된 프로토콜</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="38"/>
        <source>Everywhere</source>
        <translation>모든 곳</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="39"/>
        <source>Only Protocols</source>
        <translation>프로토콜만</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="40"/>
        <source>Only Description</source>
        <translation>설명만</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="41"/>
        <source>Only enabled protocols</source>
        <translation>사용된 프로토콜만</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="42"/>
        <source>Only disabled protocols</source>
        <translation>사용되지 않는 프로토콜만</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="44"/>
        <source>any protocol</source>
        <translation>모든 프로토콜</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="45"/>
        <source>non-heuristic protocols</source>
        <translation>비-휴리스틱 프로토콜</translation>
    </message>
    <message>
        <location filename="enabled_protocols_dialog.cpp" line="46"/>
        <source>heuristic protocols</source>
        <translation>휴리스틱 프로토콜</translation>
    </message>
</context>
<context>
    <name>EnabledProtocolsModel</name>
    <message>
        <location filename="models/enabled_protocols_model.cpp" line="140"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="models/enabled_protocols_model.cpp" line="142"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
</context>
<context>
    <name>EndpointDataModel</name>
    <message>
        <location filename="models/atap_data_model.cpp" line="339"/>
        <source>Address</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="341"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="343"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="345"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="351"/>
        <source>Tx Packets</source>
        <translation>Tx 패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="353"/>
        <source>Tx Bytes</source>
        <translation>Tx 바이트</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="355"/>
        <source>Rx Packets</source>
        <translation>Rx 패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="357"/>
        <source>Rx Bytes</source>
        <translation>Rx 바이트</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="359"/>
        <source>Country</source>
        <translation>국가</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="361"/>
        <source>City</source>
        <translation>도시</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="363"/>
        <source>Latitude</source>
        <translation>위도</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="365"/>
        <source>Longitude</source>
        <translation>경도</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="367"/>
        <source>AS Number</source>
        <translation>AS? 번호</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="369"/>
        <source>AS Organization</source>
        <translation>AS 조직</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="347"/>
        <source>Total Packets</source>
        <translation>전체 패킷</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="349"/>
        <source>Percent Filtered</source>
        <translation>필터된 비율</translation>
    </message>
</context>
<context>
    <name>EndpointDialog</name>
    <message>
        <location filename="endpoint_dialog.cpp" line="80"/>
        <source>Map</source>
        <translation>지도</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="81"/>
        <source>Draw IPv4 or IPv6 endpoints on a map.</source>
        <translation>IPv4 또는 IPv6의 종단점을 지도에서 그립니다.</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="85"/>
        <source>Open in browser</source>
        <translation>브라우저에서 열기</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="87"/>
        <source>Save As…</source>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="140"/>
        <source>Map file error</source>
        <translation>지도 파일 오류</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="129"/>
        <source>Save Endpoints Map</source>
        <translation>종단점 지도 저장</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="141"/>
        <source>Failed to save map file %1.</source>
        <translation>%1 지도 파일 저장에 실패하였습니다.</translation>
    </message>
</context>
<context>
    <name>EthernetAddressModel</name>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="141"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="141"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="141"/>
        <source>Address</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="147"/>
        <source>All entries</source>
        <translation>모든 항목</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="148"/>
        <location filename="models/resolved_addresses_models.cpp" line="162"/>
        <source>Hosts</source>
        <translation>호스트</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="149"/>
        <location filename="models/resolved_addresses_models.cpp" line="170"/>
        <source>Ethernet Addresses</source>
        <translation>이더넷 주소</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="149"/>
        <location filename="models/resolved_addresses_models.cpp" line="177"/>
        <source>Ethernet Manufacturers</source>
        <translation>이더넷 제조업체</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="150"/>
        <location filename="models/resolved_addresses_models.cpp" line="184"/>
        <source>Ethernet Well-Known Addresses</source>
        <translation>잘 알려진 이더넷 멀티캐스트 주소</translation>
    </message>
</context>
<context>
    <name>ExpertInfoDialog</name>
    <message>
        <location filename="expert_info_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="30"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="39"/>
        <source>Limit to Display Filter</source>
        <translation>표시 필터로 제한</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="59"/>
        <source>Group by summary</source>
        <translation>요약으로 묶음</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="82"/>
        <location filename="expert_info_dialog.ui" line="92"/>
        <source>Search expert summaries.</source>
        <translation>전문가 요약을 검색합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="85"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="112"/>
        <source>Show…</source>
        <oldsource>Show...</oldsource>
        <translation>표시…</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="144"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="147"/>
        <source>Show error packets.</source>
        <translation>오류 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="158"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="161"/>
        <source>Show warning packets.</source>
        <translation>경고 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="172"/>
        <source>Note</source>
        <translation>주의</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="175"/>
        <source>Show note packets.</source>
        <translation>주의 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="186"/>
        <source>Chat</source>
        <translation>대화</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="189"/>
        <source>Show chat packets.</source>
        <translation>대화 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="200"/>
        <source>Comment</source>
        <translation>주석</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.ui" line="203"/>
        <source>Show comment packets.</source>
        <translation>주석 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="51"/>
        <source>Expert Information</source>
        <translation>전문가 정보</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="101"/>
        <source>Collapse All</source>
        <translation>모두 접음</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="105"/>
        <source>Expand All</source>
        <translation>모두 펼침</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="188"/>
        <location filename="expert_info_dialog.cpp" line="189"/>
        <source>Capture file closed.</source>
        <translation>캡쳐 파일이 닫혔습니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="191"/>
        <source>No display filter</source>
        <translation>표시 필터 없음</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="192"/>
        <source>No display filter set.</source>
        <translation>표시 필터가 설정되어 있지 않습니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="194"/>
        <source>Limit information to &quot;%1&quot;.</source>
        <translation>&quot;%1&quot;로 정보를 제한합니다.</translation>
    </message>
    <message>
        <location filename="expert_info_dialog.cpp" line="195"/>
        <source>Display filter: &quot;%1&quot;</source>
        <translation>표시 필터: &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ExpertInfoProxyModel</name>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="206"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="208"/>
        <source>Severity</source>
        <translation>심각성</translation>
    </message>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="210"/>
        <source>Summary</source>
        <translation>요약</translation>
    </message>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="212"/>
        <source>Group</source>
        <translation>묶음</translation>
    </message>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="214"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="models/expert_info_proxy_model.cpp" line="216"/>
        <source>Count</source>
        <translation>카운트</translation>
    </message>
</context>
<context>
    <name>ExportDissectionDialog</name>
    <message>
        <location filename="export_dissection_dialog.cpp" line="59"/>
        <source>Export Packet Dissections</source>
        <oldsource>Wireshark: Export Packet Dissections</oldsource>
        <translation>패킷 분해자 내보내기</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="94"/>
        <source>Export As:</source>
        <oldsource>Export as:</oldsource>
        <translation>다른 이름으로 내보내기:</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="98"/>
        <source>Plain text (*.txt)</source>
        <translation>일반 텍스트 (*.txt)</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="99"/>
        <source>Comma Separated Values - summary (*.csv)</source>
        <translation>쉼표로 구분된 값 - 요약 (*.csv)</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="100"/>
        <source>PSML - summary (*.psml, *.xml)</source>
        <translation>PSML - 요약 (*. psml, *.xml)</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="101"/>
        <source>PDML - details (*.pdml, *.xml)</source>
        <translation>PSML - 상세 정보 (*. psml, *.xml)</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="102"/>
        <source>JSON (*.json)</source>
        <translation>JSON (*.json)</translation>
    </message>
    <message>
        <location filename="export_dissection_dialog.cpp" line="103"/>
        <source>C Arrays - bytes (*.c, *.h)</source>
        <translation>C 언어 배열 - 바이트 (*.c, *.h)</translation>
    </message>
</context>
<context>
    <name>ExportObjectDialog</name>
    <message>
        <location filename="export_object_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="export_object_dialog.ui" line="39"/>
        <source>Content Type:</source>
        <translation>내용 유형:</translation>
    </message>
    <message>
        <location filename="export_object_dialog.ui" line="91"/>
        <source>Searching for objects</source>
        <translation>객체 검색 중</translation>
    </message>
    <message>
        <location filename="export_object_dialog.ui" line="25"/>
        <source>Text Filter:</source>
        <translation>텍스트 필터:</translation>
    </message>
    <message>
        <location filename="export_object_dialog.ui" line="32"/>
        <source>Only display entries containing this string</source>
        <translation>이 문자열을 포함하는 항목만 표시</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="69"/>
        <source>Preview</source>
        <translation>미리보기</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="73"/>
        <location filename="export_object_dialog.cpp" line="145"/>
        <location filename="export_object_dialog.cpp" line="156"/>
        <source>All Content-Types</source>
        <translation>모든 내용 유형</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="76"/>
        <source>Export</source>
        <translation>내보내기</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="76"/>
        <source>%1 object list</source>
        <translation>%1 객체 목록</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="265"/>
        <source>Save Object As…</source>
        <translation>다른 이름으로 객체를 저장...</translation>
    </message>
    <message>
        <location filename="export_object_dialog.cpp" line="295"/>
        <source>Save All Objects In…</source>
        <translation>모든 객체를 저장...</translation>
    </message>
</context>
<context>
    <name>ExportObjectModel</name>
    <message>
        <location filename="models/export_objects_model.cpp" line="104"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="models/export_objects_model.cpp" line="106"/>
        <source>Hostname</source>
        <translation>호스트 이름</translation>
    </message>
    <message>
        <location filename="models/export_objects_model.cpp" line="108"/>
        <source>Content Type</source>
        <translation>내용 유형</translation>
    </message>
    <message>
        <location filename="models/export_objects_model.cpp" line="110"/>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="models/export_objects_model.cpp" line="112"/>
        <source>Filename</source>
        <translation>파일 이름</translation>
    </message>
</context>
<context>
    <name>ExportPDUDialog</name>
    <message>
        <location filename="export_pdu_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="export_pdu_dialog.ui" line="45"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
</context>
<context>
    <name>ExtArgSelector</name>
    <message>
        <location filename="extcap_argument.cpp" line="150"/>
        <source>Reload data</source>
        <translation>데이터 다시 불러오기</translation>
    </message>
</context>
<context>
    <name>ExtcapArgumentFileSelection</name>
    <message>
        <location filename="extcap_argument_file.cpp" line="49"/>
        <source>Clear</source>
        <translation>지우기</translation>
    </message>
    <message>
        <location filename="extcap_argument_file.cpp" line="107"/>
        <source>All Files (</source>
        <translation>모든 파일 (</translation>
    </message>
    <message>
        <location filename="extcap_argument_file.cpp" line="119"/>
        <source>Open File</source>
        <translation>파일 열기</translation>
    </message>
    <message>
        <location filename="extcap_argument_file.cpp" line="128"/>
        <source>Select File</source>
        <translation>파일 선택</translation>
    </message>
</context>
<context>
    <name>ExtcapOptionsDialog</name>
    <message>
        <location filename="extcap_options_dialog.cpp" line="67"/>
        <location filename="extcap_options_dialog.cpp" line="105"/>
        <source>Interface Options</source>
        <oldsource>Extcap Interface Options</oldsource>
        <translation>인터페이스 선택사항</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="72"/>
        <source>Start</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="74"/>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="278"/>
        <source>Default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="346"/>
        <source>Restore default value of the item</source>
        <translation>항목의 기본값을 복구</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="425"/>
        <source>Extcap Help cannot be found</source>
        <translation>Extcap 도움말을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.cpp" line="426"/>
        <source>The help for the extcap interface %1 cannot be found. Given file: %2</source>
        <translation>%1 extcap 인터페이스에 대한 도움말을 찾을 수 없습니다. 주어진 파일: %2</translation>
    </message>
    <message>
        <location filename="extcap_options_dialog.ui" line="33"/>
        <location filename="extcap_options_dialog.cpp" line="275"/>
        <source>Save parameter(s) on capture start</source>
        <translation>캡쳐 시작시 파라미터 저장</translation>
    </message>
</context>
<context>
    <name>FieldFilterEdit</name>
    <message>
        <location filename="widgets/field_filter_edit.cpp" line="50"/>
        <source>Display filter entry</source>
        <translation>표시 필터 항목</translation>
    </message>
    <message>
        <location filename="widgets/field_filter_edit.cpp" line="72"/>
        <source>Enter a field %1</source>
        <translation>%1 필드를 입력</translation>
    </message>
    <message>
        <location filename="widgets/field_filter_edit.cpp" line="105"/>
        <source>Invalid filter: </source>
        <translation>유효하지 않은 필터: </translation>
    </message>
</context>
<context>
    <name>FileSetDialog</name>
    <message>
        <location filename="file_set_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="file_set_dialog.ui" line="30"/>
        <source>Directory:</source>
        <translation>디렉터리:</translation>
    </message>
    <message>
        <location filename="file_set_dialog.cpp" line="111"/>
        <source>No files in Set</source>
        <translation>집합에 파일이 없습니다</translation>
    </message>
    <message>
        <location filename="file_set_dialog.cpp" line="112"/>
        <source>No capture loaded</source>
        <translation>불러온 내용이 없습니다.</translation>
    </message>
    <message numerus="yes">
        <location filename="file_set_dialog.cpp" line="119"/>
        <source>%Ln File(s) in Set</source>
        <oldsource>%1 File%2 in Set</oldsource>
        <translation><numerusform>집합내에 %Ln 개 파일</numerusform></translation>
    </message>
</context>
<context>
    <name>FilesetEntryModel</name>
    <message>
        <location filename="models/fileset_entry_model.cpp" line="76"/>
        <source>Open this capture file</source>
        <translation>이 캡쳐 파일을 열기</translation>
    </message>
    <message>
        <location filename="models/fileset_entry_model.cpp" line="95"/>
        <source>Filename</source>
        <translation>파일 이름</translation>
    </message>
    <message>
        <location filename="models/fileset_entry_model.cpp" line="97"/>
        <source>Created</source>
        <translation>생성됨</translation>
    </message>
    <message>
        <location filename="models/fileset_entry_model.cpp" line="99"/>
        <source>Modified</source>
        <translation>수정됨</translation>
    </message>
    <message>
        <location filename="models/fileset_entry_model.cpp" line="101"/>
        <source>Size</source>
        <translation>크기</translation>
    </message>
</context>
<context>
    <name>FilterAction</name>
    <message>
        <location filename="filter_action.cpp" line="212"/>
        <source>Selected</source>
        <translation>선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="214"/>
        <source>Not Selected</source>
        <translation>선택 안됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="216"/>
        <source>…and Selected</source>
        <translation>…그리고 선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="219"/>
        <source>…or Selected</source>
        <translation>…또는 선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="222"/>
        <source>…and not Selected</source>
        <translation>…그리고 선택되지 않음</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="225"/>
        <source>…or not Selected</source>
        <translation>…또는 선택되지 않음</translation>
    </message>
</context>
<context>
    <name>FilterDialog</name>
    <message>
        <location filename="filter_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="filter_dialog.ui" line="35"/>
        <source>Create a new filter.</source>
        <translation>새로운 필터를 만듭니다.</translation>
    </message>
    <message>
        <location filename="filter_dialog.ui" line="48"/>
        <source>Remove this filter.</source>
        <oldsource>Remove this profile.</oldsource>
        <translation>이 프로파일을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="filter_dialog.ui" line="58"/>
        <source>Copy this filter.</source>
        <oldsource>Copy this profile.</oldsource>
        <translation>이 프로파일을 복사합니다.</translation>
    </message>
    <message>
        <location filename="filter_dialog.cpp" line="67"/>
        <source>Capture Filters</source>
        <translation>캡쳐 필터</translation>
    </message>
    <message>
        <location filename="filter_dialog.cpp" line="72"/>
        <source>Display Filters</source>
        <translation>표시 필터</translation>
    </message>
    <message>
        <location filename="filter_dialog.cpp" line="93"/>
        <source>Open </source>
        <translation>열기 </translation>
    </message>
    <message>
        <location filename="filter_dialog.cpp" line="69"/>
        <location filename="filter_dialog.cpp" line="136"/>
        <source>New capture filter</source>
        <extracomment>This text is automatically filled in when a new filter is created</extracomment>
        <translation>새 캡쳐 필터</translation>
    </message>
    <message>
        <location filename="filter_dialog.cpp" line="74"/>
        <location filename="filter_dialog.cpp" line="140"/>
        <source>New display filter</source>
        <extracomment>This text is automatically filled in when a new filter is created</extracomment>
        <translation>새 표시 필터</translation>
    </message>
</context>
<context>
    <name>FilterExpressionFrame</name>
    <message>
        <location filename="filter_expression_frame.ui" line="26"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="40"/>
        <source>Filter Buttons Preferences…</source>
        <translation>필터 버튼 설정…</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="68"/>
        <source>Label:</source>
        <translation>레이블:</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="87"/>
        <source>Enter a description for the filter button</source>
        <translation>필터 버튼에 대한 설명을 입력합니다</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="98"/>
        <source>Filter:</source>
        <translation>필터:</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="117"/>
        <source>Enter a filter expression to be applied</source>
        <translation>적용할 필터 표현식 입력</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="130"/>
        <source>Comment:</source>
        <translation>의견:</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.ui" line="149"/>
        <source>Enter a comment for the filter button</source>
        <translation>필터 버튼에 대한 주석을 입력합니다</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.cpp" line="173"/>
        <source>Missing label.</source>
        <translation>레이블이 누락되었습니다.</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.cpp" line="175"/>
        <source>Missing filter expression.</source>
        <translation>필터 표현식이 누락되었습니다.</translation>
    </message>
    <message>
        <location filename="filter_expression_frame.cpp" line="177"/>
        <source>Invalid filter expression.</source>
        <translation>유효하지 않은 필터 표현식입니다.</translation>
    </message>
</context>
<context>
    <name>FilterExpressionToolBar</name>
    <message>
        <location filename="widgets/filter_expression_toolbar.cpp" line="117"/>
        <source>Filter Button Preferences...</source>
        <translation>필터 버튼 속성...</translation>
    </message>
    <message>
        <location filename="widgets/filter_expression_toolbar.cpp" line="100"/>
        <source>Edit</source>
        <translation>편집</translation>
    </message>
    <message>
        <location filename="widgets/filter_expression_toolbar.cpp" line="105"/>
        <source>Disable</source>
        <translation>해제</translation>
    </message>
    <message>
        <location filename="widgets/filter_expression_toolbar.cpp" line="110"/>
        <source>Remove</source>
        <translation>제거</translation>
    </message>
</context>
<context>
    <name>FilterListModel</name>
    <message>
        <location filename="models/filter_list_model.cpp" line="128"/>
        <source>Filter Name</source>
        <translation>필터 이름</translation>
    </message>
    <message>
        <location filename="models/filter_list_model.cpp" line="131"/>
        <source>Filter Expression</source>
        <translation>필터 표현식</translation>
    </message>
</context>
<context>
    <name>FindLineEdit</name>
    <message>
        <location filename="widgets/find_line_edit.cpp" line="27"/>
        <source>Textual Find</source>
        <translation>검색어로 찾기</translation>
    </message>
    <message>
        <location filename="widgets/find_line_edit.cpp" line="32"/>
        <source>Regular Expression Find</source>
        <translation>정규표현식으로 찾기</translation>
    </message>
</context>
<context>
    <name>FirewallRulesDialog</name>
    <message>
        <location filename="firewall_rules_dialog.ui" line="22"/>
        <source>Create rules for</source>
        <translation>대상에 대한 규칙 생성</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.ui" line="45"/>
        <source>Inbound</source>
        <translation>인바운드</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.ui" line="68"/>
        <source>Deny</source>
        <translation>거부</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="45"/>
        <source>Firewall ACL Rules</source>
        <translation>방화벽 ACL 규칙</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="47"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="104"/>
        <source>IPv4 source address.</source>
        <translation>IPv4 발신지 주소.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="105"/>
        <source>IPv4 destination address.</source>
        <translation>IPv4 목적지 주소.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="109"/>
        <source>Source port.</source>
        <translation>발신 포트.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="110"/>
        <source>Destination port.</source>
        <translation>목적지 포트.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="115"/>
        <source>IPv4 source address and port.</source>
        <translation>IPv4 발신지 주소와 포트.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="116"/>
        <source>IPv4 destination address and port.</source>
        <translation>IPv4 목적지 주소와 포트.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="120"/>
        <source>MAC source address.</source>
        <translation>MAC 발신지 주소.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="121"/>
        <source>MAC destination address.</source>
        <translation>MAC 목적지 주소.</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="176"/>
        <source>Text file (*.txt);;All Files (</source>
        <translation>텍스트 파일 (*.txt);;모든 파일 (</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="187"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="firewall_rules_dialog.cpp" line="187"/>
        <source>Unable to save %1</source>
        <translation>%1 을(를) 저장할 수 없습니다</translation>
    </message>
</context>
<context>
    <name>FolderListModel</name>
    <message>
        <location filename="about_dialog.cpp" line="182"/>
        <source>&quot;File&quot; dialogs</source>
        <translation>&quot;파일&quot; 대화 상자</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="182"/>
        <source>capture files</source>
        <translation>캡쳐 파일</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="185"/>
        <source>Temp</source>
        <translation>임시</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="185"/>
        <source>untitled capture files</source>
        <translation>이름 없는 캡쳐 파일</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="188"/>
        <source>Personal configuration</source>
        <translation>개인 환경 설정</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="195"/>
        <source>Global configuration</source>
        <translation>전역 설정</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="190"/>
        <source>dfilters, preferences, ethers, …</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="196"/>
        <source>dfilters, preferences, manuf, …</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="200"/>
        <source>System</source>
        <translation>시스템</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="200"/>
        <source>ethers, ipxnets</source>
        <translation>ethers, ipxnets</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="203"/>
        <source>Program</source>
        <translation>프로그램</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="203"/>
        <source>program files</source>
        <translation>프로그램 파일</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="207"/>
        <source>Personal Plugins</source>
        <translation>사용자 플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="207"/>
        <location filename="about_dialog.cpp" line="210"/>
        <source>binary plugins</source>
        <translation>바이너리 플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="210"/>
        <source>Global Plugins</source>
        <translation>전역 플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="215"/>
        <source>Personal Lua Plugins</source>
        <translation>사용자 Lua 플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="218"/>
        <source>Global Lua Plugins</source>
        <translation>전역 Lua 스크립트</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="215"/>
        <location filename="about_dialog.cpp" line="218"/>
        <source>Lua scripts</source>
        <translation>Lua 스크립트</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="222"/>
        <source>Personal Extcap path</source>
        <translation>사용자 Extcap 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="222"/>
        <location filename="about_dialog.cpp" line="223"/>
        <source>external capture (extcap) plugins</source>
        <translation>외부 캡쳐 (extcap) 플러그인</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="223"/>
        <source>Global Extcap path</source>
        <translation>전역 Extcap 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="229"/>
        <source>MaxMind DB path</source>
        <translation>MaxMind DB 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="229"/>
        <source>MaxMind DB database search path</source>
        <translation>MaxMind DB 데이터베이스 검색 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="238"/>
        <source>MIB/PIB path</source>
        <translation>MIB/PIB 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="238"/>
        <source>SMI MIB/PIB search path</source>
        <translation>SMI MIB/PIB 검색 경로</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="244"/>
        <source>macOS Extras</source>
        <translation>macOS 추가판</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="244"/>
        <source>Extra macOS packages</source>
        <translation>추가 macOS 패키지</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="251"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="251"/>
        <source>Location</source>
        <translation>위치</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="251"/>
        <source>Typical Files</source>
        <translation>일반적인 파일</translation>
    </message>
</context>
<context>
    <name>FollowStreamDialog</name>
    <message>
        <location filename="follow_stream_dialog.cpp" line="144"/>
        <source>Filter Out This Stream</source>
        <oldsource>Hide this stream</oldsource>
        <translation>이 스트림을 거르기</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="147"/>
        <source>Print</source>
        <translation>출력</translation>
    </message>
    <message numerus="yes">
        <source>%Ln client pkt(s), </source>
        <translation type="vanished"><numerusform>%Ln 클라이언트 패킷</numerusform></translation>
    </message>
    <message numerus="yes">
        <source>%Ln server pkt(s), </source>
        <translation type="vanished"><numerusform>%Ln 서버 패킷, </numerusform></translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="134"/>
        <source>ASCII</source>
        <translation>ASCII</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="135"/>
        <source>C Arrays</source>
        <translation>C 언어 배열 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="136"/>
        <source>EBCDIC</source>
        <translation>EBCDIC 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="137"/>
        <source>Hex Dump</source>
        <translation>16진수 덤프 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="140"/>
        <source>UTF-8</source>
        <translation>UTF-8 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="141"/>
        <source>YAML</source>
        <translation>YAML 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="138"/>
        <source>Raw</source>
        <translation>Raw(무편집) 형식</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="150"/>
        <source>Save as…</source>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="153"/>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="208"/>
        <source>Packet %1. </source>
        <translation>패킷 %1. </translation>
    </message>
    <message numerus="yes">
        <location filename="follow_stream_dialog.cpp" line="211"/>
        <source>%Ln &lt;span style=&quot;color: %1; background-color:%2&quot;&gt;client&lt;/span&gt; pkt(s), </source>
        <translation><numerusform>%Ln &lt;span style=&quot;color: %1; background-color:%2&quot;&gt;클라이언트&lt;/span&gt; 패킷(초), </numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="follow_stream_dialog.cpp" line="214"/>
        <source>%Ln &lt;span style=&quot;color: %1; background-color:%2&quot;&gt;server&lt;/span&gt; pkt(s), </source>
        <translation><numerusform>%Ln &lt;span style=&quot;color: %1; background-color:%2&quot;&gt;서버&lt;/span&gt; 패킷(초), </numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="follow_stream_dialog.cpp" line="217"/>
        <source>%Ln turn(s).</source>
        <translation><numerusform>%Ln 턴.</numerusform></translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="220"/>
        <source> Click to select.</source>
        <translation> 클릭하여 선택합니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="274"/>
        <source>Regex Find:</source>
        <translation>정규식 검색:</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="953"/>
        <source>No capture file.</source>
        <translation>캡쳐 파일이 없습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="953"/>
        <source>Please make sure you have a capture file opened.</source>
        <translation>캡쳐 파일이 열려 있지 않은지 확인하십시오.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="960"/>
        <location filename="follow_stream_dialog.cpp" line="965"/>
        <source>Error following stream.</source>
        <translation>스트림 따라가기 오류가 발생했습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="960"/>
        <source>Capture file invalid.</source>
        <translation>캡쳐 파일이 유효하지 않습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="965"/>
        <source>Please make sure you have a %1 packet selected.</source>
        <translation>%1 패킷을 선택했는지 확인하십시오.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="987"/>
        <source>%1 stream not found on the selected packet.</source>
        <translation>선택한 패킷으로부터 %1 스트림을 찾을 수 없었습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="1087"/>
        <source>Entire conversation (%1)</source>
        <translation>전체 대화 (%1)</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="1091"/>
        <source>Follow %1 Stream (%2)</source>
        <translation>%1 스트림 (%2) 따라가기</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="986"/>
        <source>Error creating filter for this stream.</source>
        <translation>이 스트림 필터를 만들다 오류가 발생했습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="305"/>
        <source>Save Stream Content As…</source>
        <translation>다른 이름으로 스트림 컨텐츠 저장…</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="634"/>
        <source>[Stream output truncated]</source>
        <translation>[스트림 출력이 잘림]</translation>
    </message>
    <message numerus="yes">
        <location filename="follow_stream_dialog.cpp" line="1021"/>
        <source>%Ln total stream(s).</source>
        <translation><numerusform>%Ln 전체 스트림.</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="follow_stream_dialog.cpp" line="1036"/>
        <source>Max sub stream ID for the selected stream: %Ln</source>
        <translation><numerusform>선택한 스트림에 대한 최대 하위 스트림 ID: %Ln</numerusform></translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.cpp" line="1116"/>
        <source>File closed.</source>
        <translation>파일이 닫혔습니다.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="20"/>
        <source>Follow Stream</source>
        <translation>스트림 따라가기</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="36"/>
        <source>Hint.</source>
        <translation>필터힌트.</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="68"/>
        <source>Show data as</source>
        <oldsource>Show and save data as</oldsource>
        <translation>다음 형식으로 데이터 표시, 저장</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="95"/>
        <source>Stream</source>
        <translation>스트림</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="105"/>
        <source>Substream</source>
        <translation>하위 스트림</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="119"/>
        <location filename="follow_stream_dialog.cpp" line="276"/>
        <source>Find:</source>
        <translation>찾기:</translation>
    </message>
    <message>
        <location filename="follow_stream_dialog.ui" line="129"/>
        <source>Find &amp;Next</source>
        <translation>다음 찾기(&amp;N)</translation>
    </message>
</context>
<context>
    <name>FontColorPreferencesFrame</name>
    <message>
        <location filename="font_color_preferences_frame.ui" line="20"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="31"/>
        <source>Main window font:</source>
        <translation>기본 창 글꼴:</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="38"/>
        <source>Select Font</source>
        <translation>글꼴 선택</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="70"/>
        <source>Colors:</source>
        <translation>색상:</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="125"/>
        <location filename="font_color_preferences_frame.ui" line="185"/>
        <source>System Default</source>
        <translation>시스템 기본값</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="130"/>
        <location filename="font_color_preferences_frame.ui" line="190"/>
        <source>Solid</source>
        <translation>Solid</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="255"/>
        <source>Sample ignored packet text</source>
        <translation>무시된 패킷 텍스트 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="225"/>
        <source>Sample marked packet text</source>
        <translation>표시된 패킷 텍스트 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="107"/>
        <source>Sample active selected item</source>
        <translation>활성 선택된 항목 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="117"/>
        <location filename="font_color_preferences_frame.ui" line="177"/>
        <source>Style:</source>
        <translation>스타일:</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="135"/>
        <location filename="font_color_preferences_frame.ui" line="195"/>
        <source>Gradient</source>
        <translation>그라디언트</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="167"/>
        <source>Sample inactive selected item</source>
        <translation>비활성 선택된 항목 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="285"/>
        <source>Sample &quot;Follow Stream&quot; client text</source>
        <translation>&quot;스트림 따라라기&quot; 클라이언트 텍스트 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="315"/>
        <source>Sample &quot;Follow Stream&quot; server text</source>
        <translation>&quot;스트림 따라가기&quot; 서버 텍스트 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="335"/>
        <source>Sample valid filter</source>
        <translation>유효한 필터 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="355"/>
        <source>Sample invalid filter</source>
        <translation>유효하지 않은 필터 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.ui" line="375"/>
        <source>Sample warning filter</source>
        <oldsource>Sample deprecated filter</oldsource>
        <translation>경고 필터 샘플</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.cpp" line="28"/>
        <source>Example GIF query packets have jumbo window sizes</source>
        <extracomment>These are pangrams. Feel free to replace with nonsense text that spans your alphabet. https://en.wikipedia.org/wiki/Pangram</extracomment>
        <translation>콩고물과 우유가 들어간 빙수는 차게 먹어야 특별한 맛이 잘 표현된다</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.cpp" line="29"/>
        <source>Lazy badgers move unique waxy jellyfish packets</source>
        <translation>겉표지보다 큰 몇 향수류</translation>
    </message>
    <message>
        <location filename="font_color_preferences_frame.cpp" line="320"/>
        <source>Font</source>
        <translation>글꼴</translation>
    </message>
</context>
<context>
    <name>FunnelStringDialog</name>
    <message>
        <location filename="funnel_string_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
</context>
<context>
    <name>FunnelTextDialog</name>
    <message>
        <location filename="funnel_text_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="funnel_text_dialog.ui" line="25"/>
        <location filename="funnel_text_dialog.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter some text or a regular expression. It will be highlighted above.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;검색 대상 텍스트와 정규 표현식을 입력합니다. 일치하는 부분에 강조 표시가 됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="funnel_text_dialog.ui" line="28"/>
        <source>Highlight:</source>
        <translation>강조 표시:</translation>
    </message>
</context>
<context>
    <name>GsmMapSummaryDialog</name>
    <message>
        <location filename="gsm_map_summary_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="54"/>
        <source>GSM MAP Summary</source>
        <translation>GSM 지도 요약</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="96"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="100"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="105"/>
        <source>Length</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="115"/>
        <source>Format</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="121"/>
        <source>Snapshot length</source>
        <translation>스냅샷 길이</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="129"/>
        <source>Data</source>
        <translation>데이터</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="137"/>
        <source>First packet</source>
        <translation>첫 패킷</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="143"/>
        <source>Last packet</source>
        <translation>마지막 패킷</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="163"/>
        <source>Elapsed</source>
        <translation>경과</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="171"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="228"/>
        <source>Invokes</source>
        <translation>Invokes</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="232"/>
        <source>Total number of Invokes</source>
        <translation>총 invoke 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="237"/>
        <source>Average number of Invokes per second</source>
        <translation>초당 평균 invoke 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="242"/>
        <source>Total number of bytes for Invokes</source>
        <translation>Invoke에 대한 총 바이트</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="247"/>
        <source>Average number of bytes per Invoke</source>
        <translation>Invoke당 평균 바이트</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="254"/>
        <source>Return Results</source>
        <translation>반환 결과</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="258"/>
        <source>Total number of Return Results</source>
        <translation>총 반환 결과 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="263"/>
        <source>Average number of Return Results per second</source>
        <translation>초당 평균 반환 결과 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="268"/>
        <source>Total number of bytes for Return Results</source>
        <translation>총 반환 결과 바이트</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="273"/>
        <source>Average number of bytes per Return Result</source>
        <translation>반환 결과당 평균 바이트</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="280"/>
        <source>Totals</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="284"/>
        <source>Total number of GSM MAP messages</source>
        <translation>전체 GSM 지도 메시지 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="289"/>
        <source>Average number of GSM MAP messages per second</source>
        <translation>초당 평균 GSM 지도 메시지 수</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="294"/>
        <source>Total number of bytes for GSM MAP messages</source>
        <translation>총 GSM 지도 메시지 바이트</translation>
    </message>
    <message>
        <location filename="gsm_map_summary_dialog.cpp" line="299"/>
        <source>Average number of bytes per GSM MAP message</source>
        <translation>GSM 지도 메시지당 평균 바이트</translation>
    </message>
</context>
<context>
    <name>IOGraphDialog</name>
    <message>
        <location filename="io_graph_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;x&lt;/th&gt;&lt;td&gt;Zoom in X axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;X&lt;/th&gt;&lt;td&gt;Zoom out X axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;y&lt;/th&gt;&lt;td&gt;Zoom in Y axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Y&lt;/th&gt;&lt;td&gt;Zoom out Y axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;Toggle mouse drag / zoom&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;Toggle capture / session time origin&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;Toggle crosshairs&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;Toggle mouse drag / zoom&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;Toggle capture / session time origin&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;Toggle crosshairs&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;가치있고 놀라운 시간 절약형 키보드 단축키&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;x&lt;/th&gt;&lt;td&gt;X 축 확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;X&lt;/th&gt;&lt;td&gt;X 축 축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;y&lt;/th&gt;&lt;td&gt;Y 축 확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Y&lt;/th&gt;&lt;td&gt;Y 축 축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;초기 상태로 그래프 다시 설정&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;커서 아래쪽 패킷으로 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;마우스 드래그 / 줌 전환&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;캡쳐 / 세션 시간 기점 바꾸기&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;십자선 전환&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="100"/>
        <source>Remove this graph.</source>
        <oldsource>Remove this dissection behavior.</oldsource>
        <translation>이 분해동작을 제거합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="90"/>
        <source>Add a new graph.</source>
        <translation>새로운 그래프를 추가합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="107"/>
        <source>Duplicate this graph.</source>
        <translation>이 그래프를 복사합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="120"/>
        <source>Clear all graphs.</source>
        <translation>모든 그래프를 복사합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="130"/>
        <source>Move this graph upwards.</source>
        <translation>이 그래프를 위로 옮깁니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="140"/>
        <source>Move this graph downwards.</source>
        <translation>이 그래프를 아래로 옮깁니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="160"/>
        <source>Mouse</source>
        <translation>마우스</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="167"/>
        <source>Drag using the mouse button.</source>
        <translation>마우스 버튼을 사용하여 드래그합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="170"/>
        <source>drags</source>
        <translation>드래그</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="180"/>
        <source>Select using the mouse button.</source>
        <translation>마우스 버튼을 사용하여 선택합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="183"/>
        <source>zooms</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="206"/>
        <source>Interval</source>
        <translation>주기</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="229"/>
        <source>Time of day</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="249"/>
        <source>Log scale</source>
        <translation>로그 표기</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="269"/>
        <source>Automatic Update</source>
        <translation>자동 갱신</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="289"/>
        <source>Reset</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="308"/>
        <source>Reset Graph</source>
        <translation>그래프 초기화</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="311"/>
        <source>Reset the graph to its initial state.</source>
        <translation>그래프를 초기 상태로 설정합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="314"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="319"/>
        <location filename="io_graph_dialog.ui" line="322"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="325"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="330"/>
        <location filename="io_graph_dialog.ui" line="333"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="336"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="341"/>
        <location filename="io_graph_dialog.ui" line="344"/>
        <source>Move Up 10 Pixels</source>
        <translation>위쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="347"/>
        <source>Up</source>
        <translation>위쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="352"/>
        <location filename="io_graph_dialog.ui" line="355"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="358"/>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="363"/>
        <location filename="io_graph_dialog.ui" line="366"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="369"/>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="374"/>
        <location filename="io_graph_dialog.ui" line="377"/>
        <source>Move Down 10 Pixels</source>
        <translation>아래쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="380"/>
        <source>Down</source>
        <translation>아래쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="385"/>
        <location filename="io_graph_dialog.ui" line="388"/>
        <source>Move Up 1 Pixel</source>
        <translation>위쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="391"/>
        <source>Shift+Up</source>
        <translation>Shift+위쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="396"/>
        <location filename="io_graph_dialog.ui" line="399"/>
        <source>Move Left 1 Pixel</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="402"/>
        <source>Shift+Left</source>
        <translation>Shift+왼쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="407"/>
        <location filename="io_graph_dialog.ui" line="410"/>
        <source>Move Right 1 Pixel</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="413"/>
        <source>Shift+Right</source>
        <translation>Shift+오른쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="418"/>
        <source>Move Down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="421"/>
        <source>Move down 1 Pixel</source>
        <oldsource>Move down 1 pixel</oldsource>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="424"/>
        <source>Shift+Down</source>
        <translation>Shift+아래쪽</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="429"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="432"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="435"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="440"/>
        <source>Drag / Zoom</source>
        <translation>드래그 / 확대</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="443"/>
        <source>Toggle mouse drag / zoom behavior</source>
        <translation>마우스 드래그 / 확대 동작 전환</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="446"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="451"/>
        <source>Capture / Session Time Origin</source>
        <translation>캡쳐 / 세션 시간 기점</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="454"/>
        <source>Toggle capture / session time origin</source>
        <translation>캡쳐 / 세션 시간 기점 전환</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="457"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="462"/>
        <source>Crosshairs</source>
        <translation>십자선</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="465"/>
        <source>Toggle crosshairs</source>
        <translation>십자선 표시를 전환</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="468"/>
        <source>Space</source>
        <translation>스페이스</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="473"/>
        <location filename="io_graph_dialog.ui" line="476"/>
        <source>Zoom In X Axis</source>
        <translation>X 축 확대</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="479"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="484"/>
        <location filename="io_graph_dialog.ui" line="487"/>
        <source>Zoom Out X Axis</source>
        <translation>X 축 축소</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="490"/>
        <source>Shift+X</source>
        <translation>Shift+X</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="495"/>
        <location filename="io_graph_dialog.ui" line="498"/>
        <source>Zoom In Y Axis</source>
        <translation>Y 축 확대</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="501"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="506"/>
        <location filename="io_graph_dialog.ui" line="509"/>
        <source>Zoom Out Y Axis</source>
        <translation>Y 축 축소</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.ui" line="512"/>
        <source>Shift+Y</source>
        <translation>Shift+Y</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="381"/>
        <source>1 sec</source>
        <translation>1 초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="384"/>
        <source>10 sec</source>
        <translation>10 초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="385"/>
        <source>1 min</source>
        <translation>1 분</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="386"/>
        <source>10 min</source>
        <translation>10 분</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="418"/>
        <source>Time (s)</source>
        <translation>시간 (초)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="330"/>
        <source>I/O Graphs</source>
        <translation>입/출력 그래프</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="351"/>
        <source>Save As…</source>
        <translation>다른이름으로 저장…</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="353"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="356"/>
        <source>Copy graphs from another profile.</source>
        <translation>다른 프로파일로부터 그래프 복사</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="372"/>
        <source>1 ms</source>
        <translation>1 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="373"/>
        <source>2 ms</source>
        <translation>2 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="374"/>
        <source>5 ms</source>
        <translation>5 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="375"/>
        <source>10 ms</source>
        <translation>10 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="376"/>
        <source>20 ms</source>
        <translation>20 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="377"/>
        <source>50 ms</source>
        <translation>50 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="378"/>
        <source>100 ms</source>
        <translation>100 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="379"/>
        <source>200 ms</source>
        <translation>200 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="380"/>
        <source>500 ms</source>
        <translation>500 밀리초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="382"/>
        <source>2 sec</source>
        <translation>2 초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="383"/>
        <source>5 sec</source>
        <translation>5 초</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="426"/>
        <source>Wireshark I/O Graphs: %1</source>
        <translation>Wireshark 입/출력 그래프: %1</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="439"/>
        <location filename="io_graph_dialog.cpp" line="445"/>
        <source>Filtered packets</source>
        <translation>걸러진 패킷</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="570"/>
        <source>All Packets</source>
        <translation>모든 패킷</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="574"/>
        <source>TCP Errors</source>
        <translation>TCP 오류</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1082"/>
        <source>Hover over the graph for details.</source>
        <translation>상세 정보를 보려면 그래프 위에 마우스를 올립니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1084"/>
        <source>No packets in interval</source>
        <translation>이 주기 안에 패킷이 없습니다</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1089"/>
        <source>Click to select packet</source>
        <translation>클릭하여 패킷을 선택합니다</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1089"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1093"/>
        <source>%1 (%2s%3).</source>
        <translation>%1 (%2s%3).</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1104"/>
        <source>Release to zoom, x = %1 to %2, y = %3 to %4</source>
        <translation>줌을 위해 릴리즈, x = %1 ~ %2, y = %3 ~ %4</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1110"/>
        <source>Unable to select range.</source>
        <translation>범위를 선택할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1113"/>
        <source>Click to select a portion of the graph.</source>
        <translation>클릭하여 그래프 비율을 선택합니다.</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1558"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1559"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1560"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1562"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1563"/>
        <source>Comma Separated Values (*.csv)</source>
        <translation>쉼표로 구분된 값 형식 (*.csv)</translation>
    </message>
    <message>
        <location filename="io_graph_dialog.cpp" line="1575"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프를 저장…</translation>
    </message>
</context>
<context>
    <name>Iax2AnalysisDialog</name>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="24"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Forward&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Reverse&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Forward&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;Reverse&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="65"/>
        <source>Forward</source>
        <translation>Forward</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="69"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="74"/>
        <source>Delta (ms)</source>
        <translation>델타 (ms)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="79"/>
        <source>Jitter (ms)</source>
        <translation>지터 (ms)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="84"/>
        <source>Bandwidth</source>
        <translation>대역폭</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="89"/>
        <source>Status</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="94"/>
        <source>Length</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="106"/>
        <source>Reverse</source>
        <translation>Reverse</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="116"/>
        <source>Graph</source>
        <translation>그래프</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="127"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide forward jitter values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;정방향 지터 값을 표시 또는 숨깁니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="130"/>
        <source>Forward Jitter</source>
        <translation>정방향 지터</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="150"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide forward difference values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;정방향 차이 값을 표시 또는 숨깁니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="153"/>
        <source>Forward Difference</source>
        <translation>정방향 차이 값</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide reverse jitter values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;역방향 지터 값을 표시 또는 숨깁니다&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="180"/>
        <source>Reverse Jitter</source>
        <translation>역방향 지터</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="200"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide reverse difference values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;역방향 차이 값을 표시 또는 숨깁니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="203"/>
        <source>Reverse Difference</source>
        <translation>역방향 차이 값</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="233"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="265"/>
        <source>Audio</source>
        <translation>오디오</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="268"/>
        <source>Save the audio data for both channels.</source>
        <translation>두 채널의 오디오 데이터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="273"/>
        <source>Forward Stream Audio</source>
        <translation>정방향 스트림 음성</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="276"/>
        <source>Save the forward stream audio data.</source>
        <translation>정방향 스트림 오디오 데이터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="281"/>
        <source>Reverse Stream Audio</source>
        <translation>역방향 스트림 오디오</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="284"/>
        <source>Save the reverse stream audio data.</source>
        <translation>역방향 스트림 오디오 데이터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="289"/>
        <source>CSV</source>
        <translation>CSV 형식</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="292"/>
        <source>Save both tables as CSV.</source>
        <translation>CSV 형식으로 두 표를 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="297"/>
        <source>Forward Stream CSV</source>
        <translation>정방향 스트림 CSV</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="300"/>
        <source>Save the forward table as CSV.</source>
        <translation>정방향 표를 CSV로 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="305"/>
        <source>Reverse Stream CSV</source>
        <translation>역방향 스트림 CSV</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="308"/>
        <source>Save the reverse table as CSV.</source>
        <translation>역방향 표를 CSV로 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="313"/>
        <source>Save Graph</source>
        <translation>그래프 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="316"/>
        <source>Save the graph image.</source>
        <translation>그래프 그림을 저장합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="321"/>
        <source>Go to Packet</source>
        <translation>다음 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="324"/>
        <source>Select the corresponding packet in the packet list.</source>
        <translation>패킷 목록에서 해당 패킷을 선택합니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="327"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="332"/>
        <source>Next Problem Packet</source>
        <translation>다음 문제 패킷</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="335"/>
        <source>Go to the next problem packet</source>
        <translation>다음 문제의 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.ui" line="338"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="201"/>
        <source>IAX2 Stream Analysis</source>
        <translation>IAX2 스트림 분석</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="266"/>
        <source>Unable to save RTP data.</source>
        <translation>RTP 데이터를 저장할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="309"/>
        <location filename="iax2_analysis_dialog.cpp" line="331"/>
        <source>Please select an IAX2 packet.</source>
        <translation>IAX2 패킷을 선택하십시오.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="435"/>
        <source> G: Go to packet, N: Next problem packet</source>
        <translation> G: 패킷으로 이동, N: 다음 문제 패킷</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="556"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="557"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="558"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="560"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="571"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프를 저장...</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="729"/>
        <source>Can&apos;t save in a file: Wrong length of captured packets.</source>
        <translation>파일에 저장할 수 없습니다: 캡쳐된 패킷의 길이가 다릅니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="741"/>
        <source>Can&apos;t save in a file: File I/O problem.</source>
        <translation>파일에 저장할 수 없습니다: 파일 입/출력 문제입니다.</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="864"/>
        <source>Save forward stream audio</source>
        <translation>정방향 스트림 오디오 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="867"/>
        <source>Save reverse stream audio</source>
        <translation>역방향 스트림 오디오 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="871"/>
        <source>Save audio</source>
        <translation>음성 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="875"/>
        <source>Sun Audio (*.au)</source>
        <translation>Sun 음성 형식 (*.au)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="877"/>
        <source>;;Raw (*.raw)</source>
        <translation>;;Raw(무편집) 형식 (*.raw)</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="894"/>
        <location filename="iax2_analysis_dialog.cpp" line="909"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="894"/>
        <source>Unable to save in that format</source>
        <translation>그 형식으로 저장 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="909"/>
        <source>Unable to save %1</source>
        <translation>%1 을(를) 저장할 수 없습니다</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="913"/>
        <source>Saving %1…</source>
        <translation>%1 저장중…</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="914"/>
        <source>Analyzing IAX2</source>
        <translation>IAX2 분석중</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="1139"/>
        <source>Save forward stream CSV</source>
        <translation>정방향 스트림 CSV 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="1142"/>
        <source>Save reverse stream CSV</source>
        <translation>역방향 스트림 CSV 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="1146"/>
        <source>Save CSV</source>
        <translation>CSV 저장</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="1152"/>
        <source>Comma-separated values (*.csv)</source>
        <translation>쉼표로 구분된 값 (*.csv)</translation>
    </message>
</context>
<context>
    <name>ImportTextDialog</name>
    <message>
        <location filename="import_text_dialog.ui" line="31"/>
        <source>File:</source>
        <translation>파일:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="44"/>
        <source>Set name of text file to import</source>
        <translation>가져올 텍스트 파일 이름을 지정</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="51"/>
        <source>Browse for text file to import</source>
        <translation>가져올 텍스트 파일을 찾기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="54"/>
        <source>Browse…</source>
        <oldsource>Browse...</oldsource>
        <translation>찾아보기…</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="73"/>
        <source>Hex Dump</source>
        <translation>16진수 덤프 형식</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="76"/>
        <source>Import a standard hex dump as exported by Wireshark</source>
        <translation>Wireshark에서 내보낸 표준 16진수 덤프를 가져오기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="89"/>
        <source>Offsets in the text file are in octal notation</source>
        <translation>텍스트 파일의 오프셋을 8진수로 표시</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="92"/>
        <source>Octal</source>
        <translation>8진수</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="99"/>
        <source>Offsets:</source>
        <translation>오프셋:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="106"/>
        <source>Offsets in the text file are in hexadecimal notation</source>
        <translation>텍스트 파일의 오프셋을 16진수로 표시</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="109"/>
        <source>Hexadecimal</source>
        <translation>16진수</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="119"/>
        <source>Offsets in the text file are in decimal notation</source>
        <translation>텍스트 파일의 오프셋을 10진수로 표시</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="122"/>
        <source>Decimal</source>
        <translation>10진수</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="202"/>
        <location filename="import_text_dialog.ui" line="212"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Whether to do extra processing detecting the start of the ASCII representation at the end of a hex+ASCII line even if it looks like hex bytes.&lt;/p&gt;&lt;p&gt;Do not enable if the hex dump does not contain ASCII.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;16진수 바이트 표기로 보이더라도 hex+ASCII 의 끝에 ASCII 표현의 시작점을 감지하면 추가 처리를 할지 여부.&lt;/p&gt;&lt;p&gt;hex 덤프에 ASCII를 포함하지 않는다면 사용하지 마십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>

    </message>
    <message>
        <location filename="import_text_dialog.ui" line="205"/>
        <source>ASCII identification:</source>
        <translation>ASCII 식별:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="250"/>
        <source>Regular Expression</source>
        <translation>정규표현식</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="253"/>
        <source>Import a file formatted according to a custom regular expression</source>
        <translation>사용자가 지정한 정규표현식에 따라 파일 형식을 가져오기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="261"/>
        <source>Packet format regular expression</source>
        <translation>패킷 형식 정규표현식</source>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="292"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perl compatible regular expression capturing a single packet in the file with named groups identifieing data to import. Anchors ^ and $ also match before/after newlines &lt;/p&gt;&lt;p&gt;Required is only a data group, also supported are time, dir and seqno.&lt;/p&gt;&lt;p&gt;Regex flags: DUPNAMES, MULTILINE and NOEMPTY&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;불러오기 위해, 구분하기 위해 이름붙여진 파일에서 단일 패킷을 구분하기 위한 Perl 호환 정규표현식. 엥커 ^ 와 $ 는 새 행의 이전과 이후에도 매칭됩니다. &lt;/p&gt;&lt;p&gt;요구되는 내용은 데이터 그룹 뿐이며, 지원된다면, 시간, dir, seqno. 도 요구합니다.&lt;/p&gt;&lt;p&gt;정규표현식 플래그: DUPNAMES, MULTILINE, NOEMPTY&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="311"/>
        <source>This is regexHintLabel, it will be set to default_regex_hint</source>
        <translation>이것은 정규표현식 힌트레이블이며, default_regex_hint 로 지정될 것입니다.</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="326"/>
        <source>Data encoding:</source>
        <translation>데이터 인코딩</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="333"/>
        <source>How data is encoded</source>
        <translation>어떻게 데이터가 인코딩 되는가</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="347"/>
        <source>encodingRegexExample</source>
        <translation>인코딩 정규표현식 예제</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="390"/>
        <source>List of characters indicating incoming packets</source>
        <translation>들어오는 패킷을 표시하는 문자들의 목록</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="396"/>
        <source>iI&lt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="406"/>
        <source>List of characters indicating outgoing packets</source>
        <translation>나가는 패킷을 표시하는 문자들의 목록</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="409"/>
        <source>oO&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="442"/>
        <source>Timestamp format:</source>
        <translation>타임스탬프 형식:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="168"/>
        <location filename="import_text_dialog.ui" line="178"/>
        <source>Whether or not the file contains information indicating the direction (inbound or outbound) of the packet.</source>
        <translation>파일에 패킷의 방향 (입력 또는 출력)을 나타내는 정보가 포함되어 있는지 여부입니다.</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="171"/>
        <location filename="import_text_dialog.ui" line="377"/>
        <source>Direction indication:</source>
        <translation>방향 지시:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="615"/>
        <source>ExportPDU</source>
        <translation>PDU 내보내기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="751"/>
        <source>IP version:</source>
        <translation>IP 버전:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="772"/>
        <source>Interface name:</source>
        <translation>인터페이스 이름:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="779"/>
        <source>The name of the interface to write to the import capture file</source>
        <translation>캡쳐파일을 불러오는데 쓰기 위한 인터페이스 이름</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="782"/>
        <source>Fake IF, Import from Hex Dump</source>
        <translation>Hex 덤프로부터 불러오기 위한 가짜 IF</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="793"/>
        <source>Maximum frame length:</source>
        <translation>최대 프레임 길이:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="481"/>
        <source>Encapsulation</source>
        <translation>캡슐화</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="142"/>
        <source>The text file has no offset</source>
        <translation>텍스트 파일에 오프셋이 없음</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="145"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="340"/>
        <source>&lt;small&gt;&lt;i&gt;recommended regex:&lt;/small&gt;&lt;/i&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt; 추천 정규표현식:&lt;/small&gt;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="439"/>
        <source>The format in which to parse timestamps in the text file (e.g. %H:%M:%S.). Format specifiers are based on strptime(3)</source>
        <translation>텍스트 파일에서 타임스탬프를 구문 분석할 형식입니다(예: %H:%M:%S). 형식 지정자는 strptime(3)을 기반으로 합니다</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="449"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The format in which to parse timestamps in the text file (e.g. %H:%M:%S.%f).&lt;/p&gt;&lt;p&gt;Format specifiers are based on strptime(3) with the addition of %f for second fractions. The precision of %f is determined from its length.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;텍스트 파일에서 타임스탬프를 분석하기 위한 형식입니다. (e.g. %H:%M:%S.%f).&lt;/p&gt;&lt;p&gt; 형식지정자는 strptime(3)을 기반으로 추가로 초 미만의 시간을 지원하는 %f 를 추가로 지원합니다. %f 의 정확도는 길이에 따라 결정됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="452"/>
        <source>%H:%M:%S.%f</source>
        <translation>&H:&M:%S.%f</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="459"/>
        <source>timestampExampleLabel</source>
        <translation>타임스탬프 예제 레이블</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="495"/>
        <source>Encapsulation Type:</source>
        <translation>캡슐화 유형:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="502"/>
        <source>Encapsulation type of the frames in the import capture file</source>
        <translation>가져온 캡쳐 파일 프레임의 캡슐화 유형</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="565"/>
        <source>Prefix each frame with an Ethernet and IP header</source>
        <translation>이더넷과 IP헤더로 각 프레임별 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="568"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="575"/>
        <source>Prefix each frame with an Ethernet, IP and UDP header</source>
        <translation>이더넷, IP, UDP 헤더로 각 프레임 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="585"/>
        <source>Prefix each frame with an Ethernet, IP and TCP header</source>
        <translation>Ethernet, IT, TCP 헤더로 각 프레임 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="595"/>
        <source>Prefix each frame with an Ethernet, IP and SCTP header</source>
        <translation>이더넷, IP, SCTP 헤더로 각 프레임 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="605"/>
        <source>Prefix each frame with an Ethernet, IP and SCTP (DATA) header</source>
        <translation>이더넷, IP, SCTP(데이터) 헤더로 각 프레임 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="636"/>
        <source>Source address:</source>
        <translation>발신 주소:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="643"/>
        <source>Destination address:</source>
        <translation>목적지 주소:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="678"/>
        <source>Dissector</source>
        <translation>분해자</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="695"/>
        <source>The IP protocol ID for each frame</source>
        <translation>각 프레임별 IP 프로토콜 ID</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="702"/>
        <source>The IP source address for each frame</source>
        <translation>각 프레임별 IP 소스 주소</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="709"/>
        <source>The IP destination address for each frame</source>
        <translation>각 프레임별 IP 목적지 주소</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="716"/>
        <source>The UDP, TCP or SCTP source port for each frame</source>
        <translation>프레임 별 UDP, TCP, SCTP 발신 포트</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="737"/>
        <source>The SCTP DATA payload protocol identifier for each frame</source>
        <translation>프레임마다의 SCTP 데이터 페이로드의 프로토콜 식별자</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="723"/>
        <source>The UDP, TCP or SCTP destination port for each frame</source>
        <translation>프레임 별 UDP, TCP, SCTP 목적지 포트</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="555"/>
        <source>Prefix each frame with an Ethernet header</source>
        <translation>이더넷 헤더로 각 프레임 접두어</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="558"/>
        <source>Ethernet</source>
        <translation>이더넷</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="598"/>
        <source>SCTP</source>
        <translation>SCTP</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="671"/>
        <source>PPI:</source>
        <translation>PPI:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="629"/>
        <source>Protocol (dec):</source>
        <translation>프로토콜 (10진수):</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="529"/>
        <source>Leave frames unchanged</source>
        <translation>프레임을 그대로 유지</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="532"/>
        <source>No dummy header</source>
        <translation>더미 헤더 없음</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="664"/>
        <source>Tag:</source>
        <translation>Tag:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="578"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="650"/>
        <source>Source port:</source>
        <translation>발신지 포트:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="685"/>
        <source>The Ethertype value of each frame</source>
        <translation>각 프레임의 이더넷 유형값</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="588"/>
        <source>TCP</source>
        <translation>TCP</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="730"/>
        <source>The SCTP verification tag for each frame</source>
        <translation>각 프레임의 SCTP 검증 태그</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="657"/>
        <source>Destination port:</source>
        <translation>목적지 포트:</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="622"/>
        <source>Ethertype (hex):</source>
        <translation>이더넷유형 (16진수):</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="608"/>
        <source>SCTP (Data)</source>
        <translation>SCTP (데이터)</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="744"/>
        <source>The dissector to use for each frame</source>
        <translation>각 프레임에 대해 사용하기 위한 분해자</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="758"/>
        <source>The IP Version to use for the dummy IP header</source>
        <translation>더미 IP 헤더에 대해 사용하기 위한 IP 버전</translation>
    </message>
    <message>
        <location filename="import_text_dialog.ui" line="800"/>
        <source>The maximum size of the frames to write to the import capture file (max 256kiB)</source>
        <translation>가져올 캡쳐 파일에 쓸 프레임의 최대 크기(최대 256kiB)</translation>
    </message>
    <message>
        <location filename="import_text_dialog.cpp" line="47"/>
        <source>Supported fields are data, dir, time, seqno</source>
        <translation>지원되는 필드는 data, dir, time, seqno</translation>
    </message>
    <message>
        <location filename="import_text_dialog.cpp" line="48"/>
        <source>Missing capturing group data (use (?</source>
        <translation>캡쳐링 그룹 데이터 없음 (사용 (?</translation>
    </message>
    <message>
        <location filename="import_text_dialog.cpp" line="79"/>
        <source>Import From Hex Dump</source>
        <translation>16진수 덤프에서 가져오기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.cpp" line="83"/>
        <source>Import</source>
        <translation>가져오기</translation>
    </message>
    <message>
        <location filename="import_text_dialog.cpp" line="630"/>
        <source>Import Text File</source>
        <translation>텍스트 파일 가져오기</translation>
    </message>
</context>
<context>
    <name>InterfaceFrame</name>
    <message>
        <location filename="interface_frame.ui" line="20"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="82"/>
        <source>Wired</source>
        <translation>유선</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="83"/>
        <source>AirPCAP</source>
        <translation>AirPCAP</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="84"/>
        <source>Pipe</source>
        <translation>Pipe</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="85"/>
        <source>STDIN</source>
        <translation>STDIN(표준입력)</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="86"/>
        <source>Bluetooth</source>
        <translation>블루투스</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="87"/>
        <source>Wireless</source>
        <translation>무선</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="88"/>
        <source>Dial-Up</source>
        <translation>Dial-Up</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="89"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="90"/>
        <source>External Capture</source>
        <translation>외부 캡쳐</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="91"/>
        <source>Virtual</source>
        <translation>가상</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="150"/>
        <source>Remote interfaces</source>
        <translation>원격 인터페이스</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="159"/>
        <source>Show hidden interfaces</source>
        <translation>숨은 인터페이스 표시</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="236"/>
        <source>External capture interfaces disabled.</source>
        <translation>외부 캡쳐 인터페이스 비활성화됩니다.</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="286"/>
        <source>&lt;p&gt;Local interfaces are unavailable because no packet capture driver is installed.&lt;/p&gt;&lt;p&gt;You can fix this by installing &lt;a href=&quot;https://npcap.com/&quot;&gt;Npcap&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;설치된 패킷 캡쳐 드라이버가 없어서 사용 가능한 로컬 인터페이스가 없습니다.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://npcap.com/&quot;&gt;Npcap&lt;/a&gt;을 설치하는 것으로 고칠 수 있습니다.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="293"/>
        <source>&lt;p&gt;Local interfaces are unavailable because the packet capture driver isn&apos;t loaded.&lt;/p&gt;&lt;p&gt;You can fix this by running &lt;pre&gt;net start npcap&lt;/pre&gt; if you have Npcap installed or &lt;pre&gt;net start npf&lt;/pre&gt; if you have WinPcap installed. Both commands must be run as Administrator.&lt;/p&gt;</source>
        <translation>&lt;p&gt;패킷 캡쳐 드라이버를 불러오지 않았기 때문에 로컬 인터페이스를 사용할 수 없습니다.&lt;/p&gt;&lt;p&gt;Npcap이 설치된 경우 &lt;pre&gt;net start npcap&lt;/pre&gt;을 실행하거나 WinPcap이 설치된 경우 &lt;pre&gt;net start npf&lt;/pre&gt;를 실행하여 이 문제를 해결할 수 있습니다. 두 명령 모두 관리자 권한으로 실행해야 합니다.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="314"/>
        <source>&lt;p&gt;You don&apos;t have permission to capture on local interfaces.&lt;/p&gt;&lt;p&gt;You can fix this by &lt;a href=&quot;file://%1&quot;&gt;installing ChmodBPF&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;로컬 인터페이스에서 캡쳐 할 수 있는 권한이 없습니다.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;file://%1&quot;&gt;ChmodBPF를 설치&lt;/a&gt;하여 이 문제를 해결할 수 있습니다.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="327"/>
        <source>You don&apos;t have permission to capture on local interfaces.</source>
        <translation>로컬 인터페이스에서 캡쳐할 수 있는 권한이 없습니다.</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="333"/>
        <source>No interfaces found.</source>
        <translation>인터페이스를 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="336"/>
        <source>Interfaces not loaded (due to preference). Go to Capture </source>
        <translation>(기본 설정으로 인해) 인터페이스를 불러올수 없습니다. 캡쳐로 이동 </translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="499"/>
        <source>Start capture</source>
        <translation>캡쳐 시작</translation>
    </message>
    <message>
        <location filename="interface_frame.cpp" line="520"/>
        <source>Hide Interface</source>
        <translation>인터페이스 숨기기</translation>
    </message>
</context>
<context>
    <name>InterfaceSortFilterModel</name>
    <message>
        <location filename="models/interface_sort_filter_model.cpp" line="381"/>
        <source>No interfaces to be displayed. %1 interfaces hidden.</source>
        <translation>표시할 인터페이스가 없습니다. %1 인터페이스를 숨깁니다.</translation>
    </message>
</context>
<context>
    <name>InterfaceToolbar</name>
    <message>
        <location filename="interface_toolbar.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="interface_toolbar.ui" line="32"/>
        <location filename="interface_toolbar.ui" line="45"/>
        <source>Select interface</source>
        <translation>인터페이스 선택</translation>
    </message>
    <message>
        <location filename="interface_toolbar.ui" line="35"/>
        <source>Interface</source>
        <translation>인터페이스</translation>
    </message>
</context>
<context>
    <name>InterfaceToolbarLineEdit</name>
    <message>
        <location filename="widgets/interface_toolbar_lineedit.cpp" line="31"/>
        <source>Apply changes</source>
        <translation>변경 적용</translation>
    </message>
</context>
<context>
    <name>InterfaceTreeModel</name>
    <message>
        <location filename="models/interface_tree_model.cpp" line="281"/>
        <source>Show</source>
        <translation>표시</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="289"/>
        <location filename="models/interface_tree_model.cpp" line="293"/>
        <source>Friendly Name</source>
        <translation>친근한 이름</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="285"/>
        <source>Interface Name</source>
        <translation>인터페이스 이름</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="80"/>
        <source>No interfaces found.</source>
        <translation>인터페이스가 없습니다.</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="92"/>
        <source>This version of Wireshark was built without packet capture support.</source>
        <translation>이 버전의 와이어 샤크는 패킷 캡쳐 지원없이 구축되었습니다.</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="297"/>
        <source>Local Pipe Path</source>
        <translation>로컬 파이프 경로</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="301"/>
        <source>Comment</source>
        <translation>주석</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="305"/>
        <source>Link-Layer Header</source>
        <translation>링크 레이어 헤더</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="309"/>
        <source>Promiscuous</source>
        <translation>무차별</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="313"/>
        <source>Snaplen (B)</source>
        <translation>Snaplen (바이트)</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="318"/>
        <source>Buffer (MB)</source>
        <translation>버퍼 (메가 바이트)</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="324"/>
        <source>Monitor Mode</source>
        <translation>모니터 모드</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="329"/>
        <source>Capture Filter</source>
        <translation>캡쳐 필터</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="382"/>
        <source>Addresses</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="382"/>
        <source>Address</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="388"/>
        <source>Extcap interface: %1</source>
        <translation>Extcap 인터페이스: %1</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="392"/>
        <source>No addresses</source>
        <translation>주소 없음</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="399"/>
        <source>No capture filter</source>
        <translation>캡쳐 필터 없음</translation>
    </message>
    <message>
        <location filename="models/interface_tree_model.cpp" line="404"/>
        <source>Capture filter</source>
        <translation>캡쳐 필터</translation>
    </message>
</context>
<context>
    <name>LBMLBTRMTransportDialog</name>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="14"/>
        <source>LBT-RM Transport Statistics</source>
        <translation>LBT-RM 전송 통계</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="33"/>
        <source>Sources</source>
        <translation>발신지</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="62"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="314"/>
        <source>Address/Transport</source>
        <translation>주소/전송</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="67"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="466"/>
        <source>Data frames</source>
        <translation>데이터 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="72"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="477"/>
        <source>Data bytes</source>
        <translation>데이터 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="77"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="488"/>
        <source>Data frames/bytes</source>
        <translation>데이터 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="82"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="565"/>
        <source>Data rate</source>
        <translation>데이터 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="87"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="499"/>
        <source>RX data frames</source>
        <translation>수신 데이터 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="92"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="510"/>
        <source>RX data bytes</source>
        <translation>수신 데이터 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="97"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="521"/>
        <source>RX data frames/bytes</source>
        <translation>수신 데이터 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="102"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="576"/>
        <source>RX data rate</source>
        <translation>수신 데이터 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="107"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="532"/>
        <source>NCF frames</source>
        <translation>NCF 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="112"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="554"/>
        <source>NCF count</source>
        <translation>NCF 카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="117"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="543"/>
        <source>NCF bytes</source>
        <translation>NCF 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="122"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="587"/>
        <source>NCF frames/bytes</source>
        <translation>NCF 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="127"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="598"/>
        <source>NCF count/bytes</source>
        <translation>NCF 카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="132"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="609"/>
        <source>NCF frames/count</source>
        <translation>NCF 프레임/카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="137"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="620"/>
        <source>NCF frames/count/bytes</source>
        <translation>NCF 프레임/카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="142"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="631"/>
        <source>NCF rate</source>
        <translation>NCF 속도</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="147"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="642"/>
        <source>SM frames</source>
        <translation>SM 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="152"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="653"/>
        <source>SM bytes</source>
        <translation>SM 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="157"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="664"/>
        <source>SM frames/bytes</source>
        <translation>SM 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="162"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="675"/>
        <source>SM rate</source>
        <translation>SM 속도</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="173"/>
        <source>Show</source>
        <translation>표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="181"/>
        <source>Data</source>
        <translation>데이터</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="186"/>
        <source>RX Data</source>
        <translation>RX 데이터</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="191"/>
        <source>NCF</source>
        <extracomment>Nak ConFirmation</extracomment>
        <translation>NCF</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="196"/>
        <source>SM</source>
        <extracomment>Session Message</extracomment>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="204"/>
        <source>sequence numbers for transport</source>
        <translation>전송 순서 번호</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="211"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="352"/>
        <source>XXXXX:XXX.XXX.XXX.XXX:XXXXX:XXXXXXXX:XXX.XXX.XXX.XXX:XXXXX</source>
        <translation>XXXXX:XXX.XXX.XXX.XXX:XXXXX:XXXXXXXX:XXX.XXX.XXX.XXX:XXXXX</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="250"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="378"/>
        <source>SQN</source>
        <translation>SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="255"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="278"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="383"/>
        <source>Count</source>
        <translation>카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="260"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="283"/>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="388"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="273"/>
        <source>SQN/Reason</source>
        <translation>SQN/원인</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="300"/>
        <source>Receivers</source>
        <translation>수신자</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="319"/>
        <source>NAK frames</source>
        <translation>NAK 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="324"/>
        <source>NAK count</source>
        <translation>NAK 카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="329"/>
        <source>NAK bytes</source>
        <translation>NAK 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="334"/>
        <source>NAK rate</source>
        <translation>NAK 속도</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="345"/>
        <source>NAK sequence numbers for transport</source>
        <translation>전송 NAK 순서 번호</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="409"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="419"/>
        <source>Regenerate statistics using this display filter</source>
        <translation>이 표시 필터를 사용하여 통계를 재생성</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="422"/>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="441"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="444"/>
        <source>Copy the tree as CSV</source>
        <translation>트리를 CSV로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="452"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="455"/>
        <source>Copy the tree as YAML</source>
        <translation>트리를 YAML로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="469"/>
        <source>Show the data frames column</source>
        <translation>데이터 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="480"/>
        <source>Show the data bytes column</source>
        <translation>데이터 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="491"/>
        <source>Show the data frames/bytes column</source>
        <translation>데이터 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="502"/>
        <source>Show the RX data frames column</source>
        <translation>RX 데이터 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="513"/>
        <source>Show the RX data bytes column</source>
        <translation>RX 데이터 바이트의 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="524"/>
        <source>Show the RX data frames/bytes column</source>
        <translation>RX 데이터 프레임/바이트의 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="535"/>
        <source>Show the NCF frames column</source>
        <translation>NCF 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="546"/>
        <source>Show the NCF bytes column</source>
        <translation>NCF 바이트의 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="557"/>
        <source>Show the NCF count column</source>
        <translation>NCF 카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="568"/>
        <source>Show the data rate column</source>
        <translation>데이터 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="579"/>
        <source>Show the RX data rate column</source>
        <translation>RX 데이터 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="590"/>
        <source>Show the NCF frames/bytes column</source>
        <translation>NCF 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="601"/>
        <source>Show the NCF count/bytes column</source>
        <translation>NCF 카운트/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="612"/>
        <source>Show the NCF frames/count column</source>
        <translation>NCF 프레임/카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="623"/>
        <source>Show the NCF frames/count/bytes column</source>
        <translation>NCF 프레임/카운트/바이트의 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="634"/>
        <source>Show the NCF rate column</source>
        <translation>NCF 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="645"/>
        <source>Show the SM frames column</source>
        <translation>SM 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="656"/>
        <source>Show the SM bytes column</source>
        <translation>SM 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="667"/>
        <source>Show the SM frames/bytes column</source>
        <translation>SM 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="678"/>
        <source>Show the SM rate column</source>
        <translation>SM 전송율 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="683"/>
        <source>Auto-resize columns to content</source>
        <translation>내용에 맞추어 열을 자동 조정</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.ui" line="686"/>
        <source>Resize columns to content size</source>
        <translation>내용 크기에 맞춰 열을 조정</translation>
    </message>
    <message>
        <location filename="lbm_lbtrm_transport_dialog.cpp" line="1299"/>
        <source>LBT-RM Statistics failed to attach to tap</source>
        <translation>LBT-RM 통계 탭 할당에 실패했습니다</translation>
    </message>
</context>
<context>
    <name>LBMLBTRUTransportDialog</name>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="14"/>
        <source>LBT-RU Transport Statistics</source>
        <translation>LBT-RU 전송 통계</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="33"/>
        <source>Sources</source>
        <translation>발신지</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="62"/>
        <source>Address/Transport/Client</source>
        <translation>주소/전송/클라이언트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="67"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="670"/>
        <source>Data frames</source>
        <translation>데이터 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="72"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="681"/>
        <source>Data bytes</source>
        <translation>데이터 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="77"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="692"/>
        <source>Data frames/bytes</source>
        <translation>데이터 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="82"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="703"/>
        <source>Data rate</source>
        <translation>데이터 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="87"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="714"/>
        <source>RX data frames</source>
        <translation>RX 데이터 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="92"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="725"/>
        <source>RX data bytes</source>
        <translation>RX 데이터 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="97"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="736"/>
        <source>RX data frames/bytes</source>
        <translation>RX 데이터 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="102"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="747"/>
        <source>RX data rate</source>
        <translation>RX 데이터 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="107"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="758"/>
        <source>NCF frames</source>
        <translation>NCF 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="112"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="769"/>
        <source>NCF count</source>
        <translation>NCF 카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="117"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="780"/>
        <source>NCF bytes</source>
        <translation>NCF 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="122"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="813"/>
        <source>NCF frames/count</source>
        <translation>NCF 프레임/카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="127"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="791"/>
        <source>NCF frames/bytes</source>
        <translation>NCF 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="132"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="802"/>
        <source>NCF count/bytes</source>
        <translation>NCF 카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="137"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="824"/>
        <source>NCF frames/count/bytes</source>
        <translation>NCF 프레임/카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="142"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1115"/>
        <source>NCF rate</source>
        <translation>NCF 속도</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="147"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="835"/>
        <source>SM frames</source>
        <translation>SM 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="152"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="846"/>
        <source>SM bytes</source>
        <translation>SM 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="157"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="857"/>
        <source>SM frames/bytes</source>
        <translation>SM 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="162"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="868"/>
        <source>SM rate</source>
        <translation>SM 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="167"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="879"/>
        <source>RST frames</source>
        <translation>RST 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="172"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="890"/>
        <source>RST bytes</source>
        <translation>RST 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="177"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="901"/>
        <source>RST frames/bytes</source>
        <translation>RST 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="182"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="912"/>
        <source>RST rate</source>
        <translation>RST 전송률</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="193"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="471"/>
        <source>Show</source>
        <translation>표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="213"/>
        <source>Data SQN</source>
        <translation>데이터 SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="218"/>
        <source>RX Data SQN</source>
        <translation>RX 데이터 SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="223"/>
        <source>NCF SQN</source>
        <translation>NCF SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="228"/>
        <source>SM SQN</source>
        <translation>SM SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="233"/>
        <source>RST reason</source>
        <translation>RST 원인</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="241"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="503"/>
        <source>details for transport</source>
        <translation>전송 상세 정보</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="248"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="510"/>
        <source>XXXXX:XXX.XXX.XXX.XXX:XXXXX:XXXXXXXX:XXX.XXX.XXX.XXX:XXXXX</source>
        <translation>XXXXX:XXX.XXX.XXX.XXX:XXXXX:XXXXXXXX:XXX.XXX.XXX.XXX:XXXXX</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="287"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="549"/>
        <source>SQN</source>
        <translation>SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="292"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="315"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="338"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="554"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="583"/>
        <source>Count</source>
        <translation>카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="297"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="320"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="343"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="559"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="588"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="310"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="578"/>
        <source>Reason</source>
        <translation>원인</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="333"/>
        <source>SQN/Reason</source>
        <translation>SQN/원인</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="366"/>
        <source>Receivers</source>
        <translation>수신지</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="380"/>
        <source>Address/Transport</source>
        <translation>주소/전송</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="385"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="923"/>
        <source>NAK frames</source>
        <translation>NAK 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="390"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="934"/>
        <source>NAK count</source>
        <translation>NAK 카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="395"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="945"/>
        <source>NAK bytes</source>
        <translation>NAK 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="400"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="956"/>
        <source>NAK frames/count</source>
        <translation>NAK 프레임/카운트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="405"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="967"/>
        <source>NAK count/bytes</source>
        <translation>NAK 카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="410"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="978"/>
        <source>NAK frames/bytes</source>
        <translation>NAK 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="415"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="989"/>
        <source>NAK frames/count/bytes</source>
        <translation>NAK 프레임/카운트/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="420"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1000"/>
        <source>NAK rate</source>
        <translation>NAK 비율</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="425"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1011"/>
        <source>ACK frames</source>
        <translation>ACK 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="430"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1022"/>
        <source>ACK bytes</source>
        <translation>ACK 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="435"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1033"/>
        <source>ACK frames/bytes</source>
        <translation>ACK 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="440"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1044"/>
        <source>ACK rate</source>
        <translation>ACK 비율</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="445"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1055"/>
        <source>CREQ frames</source>
        <translation>CREQ 프레임</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="450"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1066"/>
        <source>CREQ bytes</source>
        <translation>CREQ 바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="455"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1077"/>
        <source>CREQ frames/bytes</source>
        <translation>CREQ 프레임/바이트</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="460"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1088"/>
        <source>CREQ rate</source>
        <translation>CREQ 비율</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="485"/>
        <source>NAK SQN</source>
        <translation>NAK SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="490"/>
        <source>ACK SQN</source>
        <translation>ACK SQN</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="495"/>
        <source>CREQ request</source>
        <translation>CREQ 요청</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="613"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="623"/>
        <source>Regenerate statistics using this display filter</source>
        <translation>이 표시 필터를 사용하여 통계를 다시 만듬</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="626"/>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="645"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="648"/>
        <source>Copy the tree as CSV</source>
        <translation>CSV로 트리를 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="656"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="659"/>
        <source>Copy the tree as YAML</source>
        <translation>YAML로 트리를 복사</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="673"/>
        <source>Show the data frames column</source>
        <translation>데이터 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="684"/>
        <source>Show the data bytes column</source>
        <translation>데이터 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="695"/>
        <source>Show the data frames/bytes column</source>
        <translation>데이터 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="706"/>
        <source>Show the data rate column</source>
        <translation>데이터 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="717"/>
        <source>Show the RX data frames column</source>
        <translation>수신 데이터 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="728"/>
        <source>Show the RX data bytes column</source>
        <translation>수신 데이터 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="739"/>
        <source>Show the RX data frames/bytes column</source>
        <translation>수신 데이터 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="750"/>
        <source>Show the RX data rate column</source>
        <translation>수신 데이터 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="761"/>
        <source>Show the NCF frames column</source>
        <translation>NCF 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="772"/>
        <source>Show the NCF count column</source>
        <translation>NCF 카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="783"/>
        <source>Show the NCF bytes column</source>
        <translation>NCF 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="794"/>
        <source>Show the NCF frames/bytes column</source>
        <translation>NCF 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="805"/>
        <source>Show the NCF count/bytes column</source>
        <translation>NCF 카운트/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="816"/>
        <source>Show the NCF frames/count column</source>
        <translation>NCF 프레임/카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="827"/>
        <source>Show the NCF frames/count/bytes column</source>
        <translation>NCF 프레임/카운트/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="838"/>
        <source>Show the SM frames column</source>
        <translation>SM 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="849"/>
        <source>Show the SM bytes column</source>
        <translation>SM 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="860"/>
        <source>Show the SM frames/bytes column</source>
        <translation>SM 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="871"/>
        <source>Show the SM rate column</source>
        <translation>SM 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="882"/>
        <source>Show the RST frames column</source>
        <translation>RST 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="893"/>
        <source>Show the RST bytes column</source>
        <translation>RST 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="904"/>
        <source>Show the RST frames/bytes column</source>
        <translation>RST 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="915"/>
        <source>Show the RST rate column</source>
        <translation>RST 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="926"/>
        <source>Show the NAK frames column</source>
        <translation>NAK 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="937"/>
        <source>Show the NAK count column</source>
        <translation>NAK 카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="948"/>
        <source>Show the NAK bytes column</source>
        <translation>NAK 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="959"/>
        <source>Show the NAK frames/count column</source>
        <translation>NAK 프레임/카운트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="970"/>
        <source>Show the NAK count/bytes column</source>
        <translation>NAK 카운트/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="981"/>
        <source>Show the NAK frames/bytes column</source>
        <translation>NAK 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="992"/>
        <source>Show the NAK frames/count/bytes column</source>
        <translation>NAK 프레임/카운트/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1003"/>
        <source>Show the NAK rate column</source>
        <translation>NAK 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1014"/>
        <source>Show the ACK frames column</source>
        <translation>ACK 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1025"/>
        <source>Show the ACK bytes column</source>
        <translation>ACK 바이트 내용을 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1036"/>
        <source>Show the ACK frames/bytes column</source>
        <translation>ACK 프레임/바이트를 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1047"/>
        <source>Show the ACK rate column</source>
        <translation>ACK 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1058"/>
        <source>Show the CREQ frames column</source>
        <translation>CREQ 프레임 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1069"/>
        <source>Show the CREQ bytes column</source>
        <translation>CREQ 바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1080"/>
        <source>Show the CREQ frames/bytes column</source>
        <translation>CREQ 프레임/바이트 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1091"/>
        <source>Show the CREQ rate column</source>
        <translation>CREQ 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1096"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1104"/>
        <source>Auto-resize columns to content</source>
        <translation>내용에 따라 열을 자동 조정</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1099"/>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1107"/>
        <source>Resize columns to content size</source>
        <translation>내용의 크기에 맞게 열을 조정</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.ui" line="1118"/>
        <source>Show the NCF rate column</source>
        <translation>NCF 전송률 열 표시</translation>
    </message>
    <message>
        <location filename="lbm_lbtru_transport_dialog.cpp" line="1718"/>
        <source>LBT-RU Statistics failed to attach to tap</source>
        <translation>LBT-RU 통계 탭에 할당에 실패했습니다</translation>
    </message>
</context>
<context>
    <name>LBMStreamDialog</name>
    <message>
        <location filename="lbm_stream_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="21"/>
        <source>Stream</source>
        <translation>스트림</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="26"/>
        <source>Endpoint A</source>
        <translation>종단점 A</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="31"/>
        <source>Endpoint B</source>
        <translation>종단점 B</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="36"/>
        <source>Messages</source>
        <translation>메시지</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="44"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="49"/>
        <source>First Frame</source>
        <translation>첫 프레임</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="54"/>
        <source>Last Frame</source>
        <translation>마지막 프레임</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="64"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="74"/>
        <source>Regenerate statistics using this display filter</source>
        <translation>표시 필터를 사용하여 통계를 다시 작성</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="77"/>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="96"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="99"/>
        <source>Copy the tree as CSV</source>
        <translation>트리를 CSV로 복사</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="107"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.ui" line="110"/>
        <source>Copy the tree as YAML</source>
        <translation>트리를 YAML로 복사</translation>
    </message>
    <message>
        <location filename="lbm_stream_dialog.cpp" line="378"/>
        <source>LBM Stream failed to attach to tap</source>
        <translation>LBM 스트림을 탭에 할당하는데 실패했습니다</translation>
    </message>
</context>
<context>
    <name>LBMUIMFlowDialog</name>
    <message numerus="yes">
        <source>%Ln node(s)</source>
        <translation type="obsolete">
            <numerusform>%Ln node</numerusform>
            <numerusform>%Ln nodes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%Ln item(s)</source>
        <translation type="obsolete">
            <numerusform>%Ln item</numerusform>
            <numerusform>%Ln items</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LayoutPreferencesFrame</name>
    <message>
        <location filename="layout_preferences_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="181"/>
        <source>Pane 1:</source>
        <translation>평면 1:</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="188"/>
        <location filename="layout_preferences_frame.ui" line="249"/>
        <location filename="layout_preferences_frame.ui" line="310"/>
        <source>Packet List</source>
        <translation>패킷 목록</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="198"/>
        <location filename="layout_preferences_frame.ui" line="259"/>
        <location filename="layout_preferences_frame.ui" line="320"/>
        <source>Packet Details</source>
        <translation>패킷 상세 정보</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="208"/>
        <location filename="layout_preferences_frame.ui" line="269"/>
        <location filename="layout_preferences_frame.ui" line="330"/>
        <source>Packet Bytes</source>
        <translation>패킷 바이트</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="218"/>
        <location filename="layout_preferences_frame.ui" line="279"/>
        <location filename="layout_preferences_frame.ui" line="340"/>
        <source>Packet Diagram</source>
        <translation>패킷 다이어그램</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="228"/>
        <location filename="layout_preferences_frame.ui" line="289"/>
        <location filename="layout_preferences_frame.ui" line="350"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="242"/>
        <source>Pane 2:</source>
        <translation>평면 2:</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="303"/>
        <source>Pane 3:</source>
        <translation>평면 3:</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="380"/>
        <source>Packet List settings:</source>
        <translation>패킷 목록 설정:</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="387"/>
        <source>Show packet separator</source>
        <translation>패킷 구분자 표시</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="394"/>
        <source>Show column definition in column context menu</source>
        <translation>열 상황에 맞는 메뉴에 열 정의 표시</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="401"/>
        <source>Allow the list to be sorted</source>
        <translation>정렬될 목록 허용</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="410"/>
        <source>Maximum number of cached rows (affects sorting)</source>
        <translation>캐쉬 될 행의 최대 갯수(정렬 속도에 영향을 미침)</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="413"/>
        <location filename="layout_preferences_frame.ui" line="420"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If more than this many rows are displayed, then sorting by columns that require packet dissection will be disabled. Increasing this number increases memory consumption by caching column values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;이보다 더 많은 행을 표시한다면, 패킷 분해에 필요한 열을 정렬하는 데에 사용하지 않을 것입니다. 열값을 캐싱하는 데에 더 많은 메모리를 사용하도록 이 수를 증가하십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="436"/>
        <source>Enable mouse-over colorization</source>
        <translation>마우스 아래 색상 사용하기</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="459"/>
        <source>Status Bar settings:</source>
        <translation>상태 막대 설정:</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="466"/>
        <source>Show selected packet number</source>
        <translation>선택된 패킷 숫자 표시</translation>
    </message>
    <message>
        <location filename="layout_preferences_frame.ui" line="473"/>
        <source>Show file load time</source>
        <translation>파일 불러오기 시간 표시</translation>
    </message>
</context>
<context>
    <name>LteMacStatisticsDialog</name>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="498"/>
        <source>LTE Mac Statistics</source>
        <translation>LTE MAC 통계</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="533"/>
        <source>Include SR frames in filter</source>
        <translation>필터에 있는 SR 프레임을 포함</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="535"/>
        <source>Include RACH frames in filter</source>
        <translation>필터의 RACH 프레임을 포함</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="914"/>
        <source>MAC Statistics</source>
        <translation>MAC 통계</translation>
    </message>
</context>
<context>
    <name>LteRlcGraphDialog</name>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;Toggle mouse drag / zoom&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;Toggle capture / session time origin&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;Toggle crosshairs&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;가치있고 놀라운 시간 절약 키보드 단축키&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;초기 상태 그래프를 다시 설정&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 1 픽셀 이동l&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;커서 아래쪽의 패킷으로 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;마우스 드래그/확대 전환&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;캡쳐 / 세션 시간 기점 바꾸기&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;십자선 전환&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="63"/>
        <source>Mouse</source>
        <translation>마우스</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="70"/>
        <source>Drag using the mouse button.</source>
        <translation>마우스 버튼를 사용하여 드래그합니다.</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="73"/>
        <source>drags</source>
        <translation>끌기</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="83"/>
        <source>Select using the mouse button.</source>
        <translation>마우스 버튼를 사용하여 선택합니다.</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="86"/>
        <source>zooms</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset the graph to its initial state.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;그래프를 초기 상태로 다시 설정합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="112"/>
        <source>Reset</source>
        <translation>다시 설정</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="119"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Switch the direction of the connection (view the opposite flow).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;연결 방향 전환하기 (역방향 흐름으로 보기).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="122"/>
        <location filename="lte_rlc_graph_dialog.ui" line="361"/>
        <source>Switch Direction</source>
        <translation>방향 바꾸기</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="141"/>
        <source>Reset Graph</source>
        <translation>그래프 초기화</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="144"/>
        <source>Reset the graph to its initial state.</source>
        <translation>그래프를 초기 상태로 다시 설정합니다.</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="147"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="152"/>
        <location filename="lte_rlc_graph_dialog.ui" line="155"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="158"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="163"/>
        <location filename="lte_rlc_graph_dialog.ui" line="166"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="169"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="174"/>
        <location filename="lte_rlc_graph_dialog.ui" line="177"/>
        <source>Move Up 10 Pixels</source>
        <translation>위쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="180"/>
        <source>Up</source>
        <translation>위쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="185"/>
        <location filename="lte_rlc_graph_dialog.ui" line="188"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="191"/>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="196"/>
        <location filename="lte_rlc_graph_dialog.ui" line="199"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="202"/>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="207"/>
        <location filename="lte_rlc_graph_dialog.ui" line="210"/>
        <source>Move Down 10 Pixels</source>
        <translation>아래쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="213"/>
        <source>Down</source>
        <translation>아래쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="218"/>
        <location filename="lte_rlc_graph_dialog.ui" line="221"/>
        <source>Move Up 1 Pixel</source>
        <translation>위쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="224"/>
        <source>Shift+Up</source>
        <translation>Shift+위쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="229"/>
        <location filename="lte_rlc_graph_dialog.ui" line="232"/>
        <source>Move Left 1 Pixel</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="235"/>
        <source>Shift+Left</source>
        <translation>Shift+왼쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="240"/>
        <location filename="lte_rlc_graph_dialog.ui" line="243"/>
        <source>Move Right 1 Pixel</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="246"/>
        <source>Shift+Right</source>
        <translation>Shift+오른쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="251"/>
        <source>Move Down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="254"/>
        <source>Move down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="257"/>
        <source>Shift+Down</source>
        <translation>Shift+아래쪽</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="262"/>
        <source>Drag / Zoom</source>
        <translation>드래그 / 확대</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="265"/>
        <source>Toggle mouse drag / zoom behavior</source>
        <translation>마우스 드래그 / 확대 동작을 전환</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="268"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="273"/>
        <source>Crosshairs</source>
        <translation>십자선</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="276"/>
        <source>Toggle crosshairs</source>
        <translation>십자선의 표시를 바꾸기</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="279"/>
        <source>Space</source>
        <translation>스페이스</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="284"/>
        <location filename="lte_rlc_graph_dialog.ui" line="287"/>
        <location filename="lte_rlc_graph_dialog.ui" line="295"/>
        <location filename="lte_rlc_graph_dialog.ui" line="298"/>
        <source>Move Up 100 Pixels</source>
        <translation>위쪽으로 100 픽셀 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="290"/>
        <source>PgUp</source>
        <translation>페이지 업</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="301"/>
        <source>PgDown</source>
        <translation>페이지 다운</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="306"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="309"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="312"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="317"/>
        <location filename="lte_rlc_graph_dialog.ui" line="320"/>
        <source>Zoom In X Axis</source>
        <translation>X 축 확대</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="323"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="328"/>
        <location filename="lte_rlc_graph_dialog.ui" line="331"/>
        <source>Zoom Out Y Axis</source>
        <translation>Y 축 축소</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="334"/>
        <source>Shift+Y</source>
        <translation>Shift+Y</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="339"/>
        <location filename="lte_rlc_graph_dialog.ui" line="342"/>
        <source>Zoom In Y Axis</source>
        <translation>Y 축 확대</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="345"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="350"/>
        <location filename="lte_rlc_graph_dialog.ui" line="353"/>
        <source>Zoom Out X Axis</source>
        <translation>X 축 축소</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="356"/>
        <source>Shift+X</source>
        <translation>Shift+X</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="364"/>
        <source>Switch direction (swap between UL and DL)</source>
        <translation>방향 바꾸기 (UL과 DL을 바꾸기)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.ui" line="367"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="62"/>
        <source>Time</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="63"/>
        <source>Sequence Number</source>
        <translation>순서 번호</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="138"/>
        <source>LTE RLC Graph (UE=%1 chan=%2%3 %4 - %5)</source>
        <translation>LTE RLC 그래프 (UE=%1 chan=%2%3 %4 - %5)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="147"/>
        <source>LTE RLC Graph - no channel selected</source>
        <translation>LTE RLC 그래프 - 선택된 채널 없음</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="171"/>
        <source>Save As…</source>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="618"/>
        <source>%1 %2 (%3s seq %4 len %5)</source>
        <translation>%1 %2 (%3s seq %4 len %5)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="619"/>
        <source>Click to select packet</source>
        <translation>클릭하여 패킷을 선택합니다</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="619"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="636"/>
        <source>Release to zoom, x = %1 to %2, y = %3 to %4</source>
        <translation>확대하기 위해 릴리즈, x = %1 ~ %2, y = %3 ~ %4</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="642"/>
        <source>Unable to select range.</source>
        <translation>범위를 선택할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="645"/>
        <source>Click to select a portion of the graph.</source>
        <translation>클릭하여 그래프 비율을 선택합니다.</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="854"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="855"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="856"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="858"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="lte_rlc_graph_dialog.cpp" line="865"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프 저장하기…</translation>
    </message>
</context>
<context>
    <name>LteRlcStatisticsDialog</name>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="657"/>
        <source>LTE RLC Statistics</source>
        <translation>LTE RLC 통계</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="681"/>
        <source>Include SR frames in filter</source>
        <translation>필터에 있는 SR 프레임을 포함</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="683"/>
        <source>Include RACH frames in filter</source>
        <translation>필터에 있는 RACH 프레임을 포함</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="686"/>
        <source>Use RLC frames only from MAC frames</source>
        <translation>MAC 프레임에서 RLC 프레임만</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="696"/>
        <source>UL Frames</source>
        <translation>UL 프레임</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="696"/>
        <source>UL Bytes</source>
        <translation>UL 바이트</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="696"/>
        <source>UL MB/s</source>
        <translation>초당 UL 메가 바이트</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="697"/>
        <source>UL ACKs</source>
        <translation>UL ACK 개수</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="697"/>
        <source>UL NACKs</source>
        <translation>UL NACK 수</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="697"/>
        <source>UL Missing</source>
        <translation>UL 누락</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="698"/>
        <source>DL Frames</source>
        <translation>DL 프레임</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="698"/>
        <source>DL Bytes</source>
        <translation>DL 바이트</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="698"/>
        <source>DL MB/s</source>
        <translation>초당 DL 메가 바이트</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="699"/>
        <source>DL ACKs</source>
        <translation>DL ACK 개수</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="699"/>
        <source>DL NACKs</source>
        <translation>DL NACK 수</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="699"/>
        <source>DL Missing</source>
        <translation>DL 누락</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="996"/>
        <source>RLC Statistics</source>
        <translation>RLC 통계</translation>
    </message>
</context>
<context>
    <name>MainStatusBar</name>
    <message>
        <location filename="main_status_bar.cpp" line="87"/>
        <source>Ready to load or capture</source>
        <translation>불러오거나 캡쳐할 준비</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="89"/>
        <source>Ready to load file</source>
        <translation>파일 읽기 준비</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="134"/>
        <source>Open the Capture File Properties dialog</source>
        <translation>캡쳐 파일 속성 대화 상자를 열기</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="361"/>
        <source>Profile: %1</source>
        <translation>프로파일: %1</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="557"/>
        <source>Manage Profiles…</source>
        <translation>프로파일 관리하기…</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="561"/>
        <source>New…</source>
        <translation>새로 생성…</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="563"/>
        <source>Edit…</source>
        <translation>편집…</source>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="572"/>
        <location filename="main_status_bar.cpp" line="594"/>
        <source>Import</source>
        <translation>가져오기</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="581"/>
        <source>Export</source>
        <translation>내보내기</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="566"/>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="554"/>
        <source>Switch to</source>
        <translation>전환</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="198"/>
        <source> is the highest expert information level</source>
        <oldsource> is the highest expert info level</oldsource>
        <translation> 최고 정보 수준입니다</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="203"/>
        <source>ERROR</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="207"/>
        <source>WARNING</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="211"/>
        <source>NOTE</source>
        <translation>주의</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="215"/>
        <source>CHAT</source>
        <translation>대화</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="222"/>
        <source>No expert information</source>
        <oldsource>No expert info</oldsource>
        <translation>전문가 정보가 없습니다</translation>
    </message>
    <message numerus="yes">
        <location filename="main_status_bar.cpp" line="300"/>
        <source>%Ln byte(s)</source>
        <oldsource>, %1 bytes</oldsource>
        <translation><numerusform>%Ln 바이트</numerusform></translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="317"/>
        <source>Byte %1</source>
        <translation>바이트 %1</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="319"/>
        <source>Bytes %1-%2</source>
        <translation>바이트 %1-%2</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="389"/>
        <location filename="main_status_bar.cpp" line="397"/>
        <source>Selected Packet: %1 %2 </source>
        <translation>선택된 패킷: %1 %2 </translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="401"/>
        <source>Packets: %1 %4 Displayed: %2 (%3%)</source>
        <oldsource>Packets: %1 %4 Displayed: %2 %4 Marked: %3</oldsource>
        <translation>패킷 수: %1 %4 표시: %2 (%3%)</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="407"/>
        <source> %1 Selected: %2 (%3%)</source>
        <translation> %1 선택됨: %2 (%3%)</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="413"/>
        <source> %1 Marked: %2 (%3%)</source>
        <oldsource> %1 Dropped: %2</oldsource>
        <translation> %1 표시됨: %2 (%3%)</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="419"/>
        <source> %1 Dropped: %2 (%3%)</source>
        <translation> %1 누락됨: %2 (%3%)</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="425"/>
        <source> %1 Ignored: %2 (%3%)</source>
        <translation> %1 무시됨: %2 (%3%)</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="431"/>
        <source> %1 Comments: %2</source>
        <translation> %1 의견: %2</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="438"/>
        <source> %1  Load time: %2:%3.%4</source>
        <translation> %1  불러오기 시간: %2: %3.%4</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="449"/>
        <source>No Packets</source>
        <translation>패킷 없음</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="573"/>
        <source>From Zip File...</source>
        <translation>Zip 파일로부터...</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="575"/>
        <source>From Directory...</source>
        <translation>디렉터리로부터...</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="584"/>
        <source>Selected Personal Profile...</source>
        <translation>선택한 개인 프로파일...</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="588"/>
        <source>All Personal Profiles...</source>
        <translation>모든 개인 프로파일...</translation>
    </message>
    <message>
        <location filename="main_status_bar.cpp" line="393"/>
        <source>Packets: %1</source>
        <translation>패킷: %1</translation>
    </message>
</context>
<context>
    <name>MainWelcome</name>
    <message numerus="yes">
        <source>%n interface(s) shown, %1 hidden</source>
        <oldsource>%Ln interface(s) shown</oldsource>
        <translation type="obsolete">
            <numerusform>%n interface shown, %1 hidden</numerusform>
            <numerusform>%n interfaces shown, %1 hidden</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainWindowPreferencesFrame</name>
    <message>
        <location filename="main_window_preferences_frame.ui" line="26"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="41"/>
        <source>Checking this will save the size, position, and maximized state of the main window.</source>
        <translation>이것을 선택하여 기본 창의 크기, 위치, 최대화 된 상태가 저장됩니다.</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="44"/>
        <source>Remember main window size and placement</source>
        <translation>기본 창의 크기와 위치를 기억</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="51"/>
        <source>Open files in</source>
        <translation>파일을 열기</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="60"/>
        <source>This folder:</source>
        <translation>이 폴더:</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="73"/>
        <source>Browse…</source>
        <oldsource>Browse...</oldsource>
        <translation>찾아보기…</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="80"/>
        <source>The most recently used folder</source>
        <translation>가장 최근에 사용한 폴더</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="92"/>
        <source>Show up to</source>
        <translation>여기까지 표시</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="104"/>
        <source>filter entries</source>
        <translation>필터 항목</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="131"/>
        <source>recent files</source>
        <translation>최근 파일</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="153"/>
        <source>Confirm unsaved capture files</source>
        <translation>저장하지 않은 캡쳐 파일의 확인</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="160"/>
        <source>Display autocompletion for filter text</source>
        <translation>필터 텍스트 자동 완성 표시</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="169"/>
        <source>Main toolbar style:</source>
        <translation>주요 도구 막대 스타일:</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="177"/>
        <source>Icons only</source>
        <translation>아이콘 만</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="182"/>
        <source>Text only</source>
        <translation>텍스트 만</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="187"/>
        <source>Icons &amp; Text</source>
        <translation>아이콘 &amp; 텍스트</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="212"/>
        <source>Window title</source>
        <translation>창 제목</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="219"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Custom window title to be appended to the existing title&lt;br/&gt;%F = file path of the capture file&lt;br/&gt;%P = profile name&lt;br/&gt;%S = a conditional separator (&amp;quot; - &amp;quot;) that only shows when surrounded by variables with values or static text&lt;br/&gt;%V = version info&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;기존 제목에 추가할 사용자 지정 창 제목&lt;br/&gt;%F = 캡쳐 파일의 경로&lt;br/&gt;%P = 프로파일 이름&lt;br/&gt;%S = 값 또는 정적 텍스트가 있는 변수에 둘러싸인 경우에만 표시되는 조건부 구분 기호 (&amp;quot; - &amp;quot;)&lt;br/&gt;%V = 버전 정보&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="230"/>
        <source>Prepend window title</source>
        <translation>창 제목 준비</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Custom window title to be prepended to the existing title&lt;br/&gt;%F = file path of the capture file&lt;br/&gt;%P = profile name&lt;br/&gt;%S = a conditional separator (&amp;quot; - &amp;quot;) that only shows when surrounded by variables with values or static text&lt;br/&gt;%V = version info&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;기존 제목에 붙일 사용자 지정 창 제목&lt;br/&gt;%F = 캡쳐 파일의 경로&lt;br/&gt;%P = 프로파일 이름&lt;br/&gt;%S =  값 또는 정적 텍스트가 있는 변수에 둘러싸인 경우에만 표시되는 조건부 구분 기호 (&amp;quot; - &amp;quot;)&lt;br/&gt;%V = 버전 정보&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="248"/>
        <source>Language: </source>
        <translation>언어: </translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="265"/>
        <source>Use system setting</source>
        <translation>시스템 설정을 사용</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="290"/>
        <source>Debounce Timer</source>
        <translation>Debounce 타이머</translation>
    </message>
    <message>
        <location filename="main_window_preferences_frame.ui" line="297"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How long to wait (in milliseconds) before processing user input&lt;br/&gt;If you type quickly, consider lowering the value for a &apos;snappier&apos; experience.&lt;br/&gt; If you type slowly, consider increasing the value to avoid performance issues.&lt;br/&gt;This is currently used to delay searches in View -&gt; Internals -&gt; Supported Protocols and Preferences -&gt; Advanced menu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;사용자 입력 처리 전 얼마나 기다릴 것인가(밀리초 단위)&lt;br/&gt;빠르게 타이핑 할 경우, &apos;snappier&apos; 경험에 대한 값보다 낮출 것을 고려하시오.&lt;br/&gt; 느리게 타이핑 할 경우, 성능 이슈를 피하기 위해 값을 증가시키길 고려하시오.&lt;br/&gt;이 값은 현재 검색을 지연할 때 사용합니다. 보기 -&gt; 내부 -&gt; 지원 프로토콜 및 preferences -&gt; 고급 메뉴.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>

    </message>
    <message>
        <location filename="main_window_preferences_frame.cpp" line="173"/>
        <source>Open Files In</source>
        <translation>파일을 열기</translation>
    </message>
</context>
<context>
    <name>ManageInterfacesDialog</name>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="17"/>
        <source>Manage Interfaces</source>
        <translation>인터페이스 관리</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="30"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click the checkbox to hide or show a hidden interface.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;체크박스를 클릭하여 인터페이스를 표시하거나 숨길 수 있습니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="33"/>
        <source>Local Interfaces</source>
        <translation>로컬 인터페이스</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="147"/>
        <source>Show</source>
        <translation>표시</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="59"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a pipe to capture from or remove an existing pipe from the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;목록으로부터 캡쳐하기 위한 파이프를 추가하거나 기존의 파이프을 삭제합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="62"/>
        <source>Pipes</source>
        <translation>파이프</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a new pipe using default settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;기본 설정을 사용하여 새 파이프을 추가합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove the selected pipe from the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;목록으로부터 선택한 파이프을 삭제합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="137"/>
        <source>Remote Interfaces</source>
        <translation>원격 인터페이스</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="152"/>
        <source>Host / Device URL</source>
        <translation>호스트/장치 URL</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="162"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a remote host and its interfaces&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;원격 호스트와의 인터페이스를 추가합니다&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="172"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove the selected host from the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt; 목록에서 선택한 호스트을 삭제합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="195"/>
        <source>Remote Settings</source>
        <translation>원격 설정</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.ui" line="208"/>
        <source>&lt;small&gt;&lt;i&gt;&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="237"/>
        <source>This version of Wireshark does not save pipe settings.</source>
        <translation>이 버전의 Wireshark는 파이프의 설정을 저장하지 않습니다.</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="241"/>
        <source>This version of Wireshark does not save remote settings.</source>
        <translation>이 버전의 Wireshark는 원격 설정을 저장하지 않습니다.</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="243"/>
        <source>This version of Wireshark does not support remote interfaces.</source>
        <translation>이 버전의 Wireshark는 원격 인터페이스를 지원하지 않습니다.</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="274"/>
        <source>New Pipe</source>
        <translation>새로운 파이프</translation>
    </message>
</context>
<context>
    <name>ModulePreferencesScrollArea</name>
    <message>
        <location filename="module_preferences_scroll_area.ui" line="14"/>
        <source>ScrollArea</source>
        <translation>스크롤 영역</translation>
    </message>
</context>
<context>
    <name>Mtp3SummaryDialog</name>
    <message>
        <location filename="mtp3_summary_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="57"/>
        <source>MTP3 Summary</source>
        <translation>MTP3 요약</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="100"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="104"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="109"/>
        <source>Length</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="119"/>
        <source>Format</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="125"/>
        <source>Snapshot length</source>
        <translation>스냅샷 길이</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="133"/>
        <source>Data</source>
        <translation>데이터</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="141"/>
        <source>First packet</source>
        <translation>첫 패킷</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="147"/>
        <source>Last packet</source>
        <translation>마지막 패킷</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="167"/>
        <source>Elapsed</source>
        <translation>경과</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="175"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="187"/>
        <source>Service Indicator (SI) Totals</source>
        <translation>서비스 표시기 (SI) 합계</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="191"/>
        <source>SI</source>
        <translation>SI</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="192"/>
        <source>MSUs</source>
        <translation>MSUs</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="193"/>
        <location filename="mtp3_summary_dialog.cpp" line="257"/>
        <source>MSUs/s</source>
        <translation>MSUs/초</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="194"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="195"/>
        <source>Bytes/MSU</source>
        <translation>바이트/MSU</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="196"/>
        <source>Bytes/s</source>
        <translation>바이트/초</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="248"/>
        <source>Totals</source>
        <translation>총</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="252"/>
        <source>Total MSUs</source>
        <translation>MSU 합계</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="262"/>
        <source>Total Bytes</source>
        <translation>총 바이트</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="267"/>
        <source>Average Bytes/MSU</source>
        <translation>평균 바이트/MSU</translation>
    </message>
    <message>
        <location filename="mtp3_summary_dialog.cpp" line="272"/>
        <source>Average Bytes/s</source>
        <translation>평균 바이트/초</translation>
    </message>
</context>
<context>
    <name>MulticastStatisticsDialog</name>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="169"/>
        <source>UDP Multicast Streams</source>
        <translation>UDP 멀티 캐스트 스트림</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="178"/>
        <source>Source Address</source>
        <translation>발신지 주소</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="178"/>
        <source>Source Port</source>
        <translation>발신지 포트</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="179"/>
        <source>Destination Address</source>
        <translation>목적지 주소</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="179"/>
        <source>Destination Port</source>
        <translation>목적지 포트</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="180"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="180"/>
        <source>Packets/s</source>
        <translation>패킷/초</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="181"/>
        <source>Avg BW (bps)</source>
        <translation>평균 대역폭 (초당 비트)</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="181"/>
        <source>Max BW (bps)</source>
        <translation>최대 대역폭 (초당 비트)</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="182"/>
        <source>Max Burst</source>
        <translation>최대 버스트</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="182"/>
        <source>Burst Alarms</source>
        <translation>버스트 경고</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="183"/>
        <source>Max Buffers (B)</source>
        <translation>최대 버퍼 (B)</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="183"/>
        <source>Buffer Alarms</source>
        <translation>버퍼 경고</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="209"/>
        <source>Burst measurement interval (ms):</source>
        <translation>버스트 측정 간격 (밀리초):</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="211"/>
        <source>Burst alarm threshold (packets):</source>
        <translation>버스트 경고 한계 값 (패킷 수):</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="213"/>
        <source>Buffer alarm threshold (B):</source>
        <translation>버퍼 경고 한계 값 (B):</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="216"/>
        <source>Stream empty speed (Kb/s):</source>
        <oldsource>Stream empty speed (Kb/s:</oldsource>
        <translation>스트림 배출 속도 (초당 킬로 바이트):</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="218"/>
        <source>Total empty speed (Kb/s):</source>
        <oldsource>Total empty speed (Kb/s:</oldsource>
        <translation>총 배출 속도 (초당 킬로 바이트):</translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="336"/>
        <source>The burst interval must be between 1 and 1000. </source>
        <translation>버스트 간격은 1에서 1000 사이가 아니면 안됩니다. </translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="345"/>
        <source>The burst alarm threshold isn&apos;t valid. </source>
        <translation>버스트 경고 경계값이 올바르지 않습니다. </translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="354"/>
        <source>The buffer alarm threshold isn&apos;t valid. </source>
        <translation>버퍼 경고 경계값이 올바르지 않습니다. </translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="363"/>
        <source>The stream empty speed should be between 1 and 10000000. </source>
        <translation>스트림 여유 속도는 1에서 10000000 사이 여야합니다. </translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="372"/>
        <source>The total empty speed should be between 1 and 10000000. </source>
        <translation>총 배출 속도는 1에서 10000000 사이 여야합니다. </translation>
    </message>
    <message>
        <location filename="multicast_statistics_dialog.cpp" line="386"/>
        <source>%1 streams, avg bw: %2bps, max bw: %3bps, max burst: %4 / %5ms, max buffer: %6B</source>
        <translation>%1 스트림 평균 대역폭: %2bps 최대 대역폭: %3bps 최대 버스트: %4/%5ms 최대 버퍼: %6B</translation>
    </message>
</context>
<context>
    <name>PacketCommentDialog</name>
    <message>
        <location filename="packet_comment_dialog.cpp" line="21"/>
        <source>Edit Packet Comment</source>
        <translation>패킷 주석 편집</translation>
    </message>
    <message>
        <location filename="packet_comment_dialog.cpp" line="22"/>
        <source>Add Packet Comment</source>
        <translation>패킷 주석 추가</translation>
    </message>
</context>
<context>
    <name>PacketDiagram</name>
    <message>
        <location filename="packet_diagram.cpp" line="355"/>
        <source>Packet diagram</source>
        <translation>패킷 다이어그램</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="473"/>
        <source>Show Field Values</source>
        <translation>필드 값 표시</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="480"/>
        <source>Save Diagram As…</source>
        <translation>다른 이름으로 다이어그램 저장…</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="483"/>
        <source>Copy as Raster Image</source>
        <translation>래스터 이미지로 복사</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="487"/>
        <source>…as SVG</source>
        <translation>…SVG</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="767"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="768"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="770"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="773"/>
        <source>Scalable Vector Graphics (*.svg)</source>
        <translation>SVG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="778"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프 저장…</translation>
    </message>
</context>
<context>
    <name>PacketDialog</name>
    <message>
        <location filename="packet_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="packet_dialog.ui" line="33"/>
        <source>&lt;small&gt;&lt;i&gt;&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="packet_dialog.ui" line="40"/>
        <source>Show packet bytes</source>
        <translation>패킷 바이트 보기</translation>
    </message>
    <message>
        <location filename="packet_dialog.cpp" line="54"/>
        <source>Packet %1</source>
        <translation>패킷 %1</translation>
    </message>
    <message>
        <location filename="packet_dialog.cpp" line="129"/>
        <source>[%1 closed] </source>
        <translation>[%1 닫음] </translation>
    </message>
    <message>
        <location filename="packet_dialog.cpp" line="151"/>
        <source>Byte %1</source>
        <translation>바이트 %1</translation>
    </message>
    <message>
        <location filename="packet_dialog.cpp" line="153"/>
        <source>Bytes %1-%2</source>
        <translation>바이트 %1-%2</translation>
    </message>
</context>
<context>
    <name>PacketFormatGroupBox</name>
    <message>
        <location filename="packet_format_group_box.ui" line="14"/>
        <source>GroupBox</source>
        <translation>그룹상자</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="17"/>
        <source>Packet Format</source>
        <translation>패킷 형식</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="23"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Packet summary lines similar to the packet list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;패킷 목록과 유사한 패킷 요약 행&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="26"/>
        <source>Summary line</source>
        <translation>요약 행</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="36"/>
        <source>Include column headings</source>
        <translation>열 머리글 포함</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="46"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Packet details similar to the protocol tree&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;프로토콜 트리와 유사한 패킷 상세 정보&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="49"/>
        <source>Details:</source>
        <translation>상세 정보:</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export only top-level packet detail items&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;최상위 패킷 상세 정보 항목만 내보내기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="68"/>
        <source>All co&amp;llapsed</source>
        <translation>모두 닫기(&amp;l)</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="81"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Expand and collapse packet details as they are currently displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;현재 표시된 대로 패킷 상세 정보를 확장 및 축소합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="84"/>
        <source>As displa&amp;yed</source>
        <translation>표시된대로(&amp;y)</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="100"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export all packet detail items&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;모든 패킷 항목을 내보내기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="103"/>
        <source>All e&amp;xpanded</source>
        <translation>모두 펼치기(&amp;x)</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="110"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export a hexdump of the packet data similar to the packet bytes view&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;패킷 바이트 열 표시와 같은 패킷 데이터의 16진수 덤프를 내보내기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="113"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="120"/>
        <source>Include secondary data sources</source>
        <translation>2차 데이터 발신지 포함</translation>
    </message>
    <message>
        <location filename="packet_format_group_box.ui" line="123"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generate hexdumps for secondary data sources like reassembled or decrypted buffers in addition to the frame&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;프레임에 재조립하거나 복호화 한 버퍼로 2차 데이터 발신지에 대한 hex 덤프를 생성합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>PacketList</name>
    <message>
        <location filename="packet_list.cpp" line="219"/>
        <source>Protocol Preferences</source>
        <translation>프로토콜 설정</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="713"/>
        <source>Summary as Text</source>
        <translation>텍스트로 요약</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="716"/>
        <source>…as CSV</source>
        <translation>…CSV</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="719"/>
        <source>…as YAML</source>
        <translation>…YAML</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="734"/>
        <source>Decode As…</source>
        <translation>다음으로 디코딩…</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="1521"/>
        <source>Frame %1: %2

</source>
        <translation>프레임 %1: %2

</translation>
    </message>
    <message>
        <location filename="packet_list.cpp" line="1523"/>
        <source>[ Comment text exceeds %1. Stopping. ]</source>
        <translation>[ 주석 텍스트 초과 %1. 중지됨. ]</translation>
    </message>
</context>
<context>
    <name>PacketListHeader</name>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="188"/>
        <source>Align Left</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="192"/>
        <source>Align Center</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="196"/>
        <source>Align Right</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="207"/>
        <source>Edit Column</source>
        <translation>열 편집</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="209"/>
        <source>Resize to Contents</source>
        <translation>내용에 맞게 크기 조정</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="205"/>
        <source>Column Preferences…</source>
        <translation>열 설정…</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="211"/>
        <source>Resize Column to Width…</source>
        <translation>다음 폭으로 열 재조정…</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="214"/>
        <source>Resolve Names</source>
        <translation>이름 해석</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="247"/>
        <source>Remove this Column</source>
        <translation>이 열 삭제</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="371"/>
        <source>Column %1</source>
        <translation>열 %1</translation>
    </message>
    <message>
        <location filename="widgets/packet_list_header.cpp" line="371"/>
        <source>Width:</source>
        <translation>폭:</translation>
    </message>
</context>
<context>
    <name>PacketListModel</name>
    <message>
        <location filename="models/packet_list_model.cpp" line="386"/>
        <source>Column</source>
        <translation>열</translation>
    </message>
    <message>
        <location filename="models/packet_list_model.cpp" line="388"/>
        <source>%1 can only be sorted with %2 or fewer visible rows; increase cache size in Layout preferences</source>
        <translation>%1 은 오직 %2 로 정렬되며, 더 적게 표시될 것입니다; 레이아웃 설정에서 캐시 크기를 증가하십시오</translation>
    </message>
    <message>
        <location filename="models/packet_list_model.cpp" line="411"/>
        <source>Sorting &quot;%1&quot;…</source>
        <translation>정렬 중 &quot;%1&quot;…</translation>
    </message>
    <message>
        <location filename="models/packet_list_model.cpp" line="413"/>
        <source>Sorting …</source>
        <translation>정렬 중 …</translation>
    </message>
</context>
<context>
    <name>PacketRangeGroupBox</name>
    <message>
        <location filename="packet_range_group_box.ui" line="14"/>
        <source>Form</source>
        <translation>양식</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="17"/>
        <source>Packet Range</source>
        <translation>패킷 범위</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="23"/>
        <location filename="packet_range_group_box.ui" line="46"/>
        <location filename="packet_range_group_box.ui" line="76"/>
        <location filename="packet_range_group_box.ui" line="93"/>
        <location filename="packet_range_group_box.ui" line="113"/>
        <location filename="packet_range_group_box.ui" line="133"/>
        <location filename="packet_range_group_box.ui" line="143"/>
        <location filename="packet_range_group_box.ui" line="153"/>
        <location filename="packet_range_group_box.ui" line="186"/>
        <location filename="packet_range_group_box.ui" line="219"/>
        <location filename="packet_range_group_box.ui" line="229"/>
        <location filename="packet_range_group_box.ui" line="239"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="33"/>
        <source>Displayed</source>
        <translation>표시된 패킷</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="56"/>
        <source>&amp;Marked packets only</source>
        <translation>마크된 패킷만(&amp;M)</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="66"/>
        <source>&amp;Range:</source>
        <translation>범위(&amp;R):</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="86"/>
        <source>Remove &amp;ignored packets</source>
        <translation>무시된 패킷을 삭제(&amp;i)</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="103"/>
        <source>First &amp;to last marked</source>
        <translation>처음부터 마지막 표기 까지(&amp;t)</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="123"/>
        <source>&amp;All packets</source>
        <translation>모든 패킷(&amp;A)</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="163"/>
        <source>&amp;Selected packets only</source>
        <translation>선택된 패킷만(&amp;t)</translation>
    </message>
    <message>
        <location filename="packet_range_group_box.ui" line="173"/>
        <source>Captured</source>
        <translation>캡쳐된 패킷</translation>
    </message>
</context>
<context>
    <name>PathSelectionDelegate</name>
    <message>
        <location filename="models/path_selection_delegate.cpp" line="21"/>
        <source>Open a pipe</source>
        <translation>파이프 열기</translation>
    </message>
</context>
<context>
    <name>PathSelectionEdit</name>
    <message>
        <location filename="widgets/path_selection_edit.cpp" line="36"/>
        <source>Browse</source>
        <translation>탐색</translation>
    </message>
    <message>
        <location filename="widgets/path_selection_edit.cpp" line="53"/>
        <source>Select a path</source>
        <translation>경로를 선택</translation>
    </message>
</context>
<context>
    <name>PluginListModel</name>
    <message>
        <location filename="about_dialog.cpp" line="144"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="144"/>
        <source>Version</source>
        <translation>버전</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="144"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="144"/>
        <source>Path</source>
        <translation>경로</translation>
    </message>
</context>
<context>
    <name>PortsModel</name>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="198"/>
        <source>All entries</source>
        <translation>모든 항목</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="198"/>
        <source>tcp</source>
        <translation>tcp</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="198"/>
        <source>udp</source>
        <translation>udp</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="198"/>
        <source>sctp</source>
        <translation>sctp</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="198"/>
        <source>dccp</source>
        <translation>dccp</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="203"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="203"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="models/resolved_addresses_models.cpp" line="203"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
</context>
<context>
    <name>PreferenceEditorFrame</name>
    <message>
        <location filename="preference_editor_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="preference_editor_frame.ui" line="32"/>
        <source>…</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="preference_editor_frame.ui" line="52"/>
        <source>a preference</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="preference_editor_frame.ui" line="69"/>
        <source>Browse…</source>
        <translation>찾아보기…</translation>
    </message>
    <message>
        <location filename="preference_editor_frame.cpp" line="69"/>
        <source>Open %1 preferences…</source>
        <translation>%1 설정 열기…</source>
    </message>
    <message>
        <location filename="preference_editor_frame.cpp" line="287"/>
        <source>Invalid value.</source>
        <translation>유효하지 않은 값입니다.</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="preferences_dialog.ui" line="61"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="preferences_dialog.cpp" line="105"/>
        <source>Preferences</source>
        <translation>설정</translation>
    </message>
</context>
<context>
    <name>PrefsModel</name>
    <message>
        <location filename="models/pref_models.cpp" line="321"/>
        <source>Advanced</source>
        <translation>고급</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="322"/>
        <source>Appearance</source>
        <translation>모양</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="323"/>
        <source>Layout</source>
        <translation>레이아웃</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="324"/>
        <source>Columns</source>
        <translation>열</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="325"/>
        <source>Font and Colors</source>
        <translation>글꼴 및 색상</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="326"/>
        <source>Capture</source>
        <translation>캡쳐</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="327"/>
        <source>Expert</source>
        <translation>전문가</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="328"/>
        <source>Filter Buttons</source>
        <translation>필터 버튼</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="329"/>
        <source>RSA Keys</source>
        <translation>RSA 키</translation>
    </message>
</context>
<context>
    <name>PrintDialog</name>
    <message>
        <location filename="print_dialog.ui" line="21"/>
        <source>Packet Format</source>
        <translation>패킷 형식</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="34"/>
        <source>Print each packet on a new page</source>
        <translation>패킷마다 새로운 페이지에 출력</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="41"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Print capture file information on each page&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;각 페이지에 캡쳐 파일 정보 인쇄&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="44"/>
        <source>Capture information header</source>
        <translation>캡쳐 정보 헤더</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="70"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the &amp;quot;+&amp;quot; and &amp;quot;-&amp;quot; keys to zoom the preview in and out. Use the &amp;quot;0&amp;quot; key to reset the zoom level.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&amp;quot;+&amp;quot; 키와 &amp;quot;-&amp;quot; 키로 미리보기를 확대 또는 축소합니다. &amp;quot;0&amp;quot; 키에서 줌을 리셋합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;+ and - zoom, 0 resets&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;+와 -로 확대 축소, 0 으로 다시 설정&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="print_dialog.ui" line="84"/>
        <source>Packet Range</source>
        <translation>패킷 범위</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="81"/>
        <source>Print</source>
        <translation>출력</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="73"/>
        <source>&amp;Print…</source>
        <translation>출력…(&amp;P)</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="117"/>
        <source>Page &amp;Setup…</source>
        <translation>페이지 설정…(&amp;S)</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="160"/>
        <source>%1 %2 total packets, %3 shown</source>
        <translation>%1 %2 전체 패킷, %3 표시됨</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="282"/>
        <source>Print Error</source>
        <translation>출력 오류</translation>
    </message>
    <message>
        <location filename="print_dialog.cpp" line="283"/>
        <source>Unable to print to %1.</source>
        <translation>%1에 출력 할 수 없습니다.</translation>
    </message>
</context>
<context>
    <name>ProfileDialog</name>
    <message>
        <location filename="profile_dialog.ui" line="19"/>
        <source>Search for profile …</source>
        <translation>프로파일 찾기 …</translation>
    </message>
    <message>
        <location filename="profile_dialog.ui" line="61"/>
        <source>Create a new profile using default settings.</source>
        <translation>기본 설정을 사용하여 새로운 프로파일 파일을 만듭니다.</translation>
    </message>
    <message>
        <location filename="profile_dialog.ui" line="75"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove this profile. System provided profiles cannot be removed. The default profile will be reset upon deletion.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;이 프로파일을 제거하십시오. 시스템 제공 프로파일을 제거할 수 없습니다. 삭제 시 기본 프로파일이 다시 설정됩니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="profile_dialog.ui" line="86"/>
        <source>Copy this profile.</source>
        <translation>이 프로파일을 복사합니다.</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="61"/>
        <source>Configuration Profiles</source>
        <translation>설정 프로파일</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="78"/>
        <source>Import</source>
        <comment>noun</comment>
        <translation>가져오기</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="81"/>
        <source>Export</source>
        <comment>noun</comment>
        <translation>내보내기</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="84"/>
        <source>From Zip File...</source>
        <translation>Zip 파일로부터...</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="86"/>
        <source>From Directory...</source>
        <translation>디렉터리로부터...</translation>
    </message>
    <message numerus="yes">
        <location filename="profile_dialog.cpp" line="91"/>
        <location filename="profile_dialog.cpp" line="296"/>
        <location filename="profile_dialog.cpp" line="314"/>
        <source>%Ln Selected Personal Profile(s)...</source>
        <translation><numerusform>선택된 %Ln 개 개인 프로파일...</numerusform></translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="94"/>
        <source>All Personal Profiles...</source>
        <translation>모든 개인 프로파일...</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="387"/>
        <source>New profile</source>
        <translation>새로운 프로파일</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="489"/>
        <source>Profile Error</source>
        <translation>프로파일 오류</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="604"/>
        <location filename="profile_dialog.cpp" line="622"/>
        <location filename="profile_dialog.cpp" line="632"/>
        <source>Exporting profiles</source>
        <translation>프로파일을 내보내고 있습니다</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="601"/>
        <source>No profiles found for export</source>
        <translation>내보낼 프로파일을 찾을 수 없음</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="608"/>
        <source>Select zip file for export</source>
        <translation>내보낼 ZIP 파일 선택</translation>
    </message>
    <message numerus="yes">
        <source>… %Ln selected personal profile(s)</source>
        <translation type="vanished"><numerusform>… %Ln 선택된 개인 프로파일</numerusform></translation>
    </message>
    <message numerus="yes">
        <source>%Ln selected personal profile(s)</source>
        <translation type="vanished"><numerusform>%Ln 선택된 개인 프로파일</numerusform></translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="243"/>
        <source>An import of profiles is not allowed, while changes are pending</source>
        <translation>변경 사항이 보류중인 동안에는 프로파일 가져오기는 허용되지 않습니다</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="248"/>
        <source>An import is pending to be saved. Additional imports are not allowed</source>
        <translation>가져오기를 저장하려고 보류하고 있습니다. 추가적인 가져오기는 허용되지 않습니다</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="268"/>
        <source>An export of profiles is only allowed for personal profiles</source>
        <translation>프로파일 내보내기는 개인 프로파일에만 허용됩니다</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="270"/>
        <source>An export of profiles is not allowed, while changes are pending</source>
        <translation>변경 사항이 보류중인 동안에는 프로파일 내보내기가 허용되지 않습니다</translation>
    </message>
    <message numerus="yes">
        <location filename="profile_dialog.cpp" line="619"/>
        <source>%Ln profile(s) exported</source>
        <translation><numerusform>%Ln 프로파일 내보냄</numerusform></translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="639"/>
        <source>Select zip file for import</source>
        <translation>가져올 ZIP 파일 선택</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="655"/>
        <source>Select directory for import</source>
        <translation>가져올 디렉터리 선택</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="608"/>
        <location filename="profile_dialog.cpp" line="639"/>
        <source>Zip File (*.zip)</source>
        <translation>ZIP 파일 (*.zip)</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="528"/>
        <location filename="profile_dialog.cpp" line="631"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="629"/>
        <source>An error has occurred while exporting profiles</source>
        <translation>프로파일을 내보내는 동안 오류가 발생했습니다</translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="676"/>
        <source>No profiles found for import in %1</source>
        <translation>%1에서 가져올 프로파일을 찾을 수 없음</translation>
    </message>
    <message numerus="yes">
        <location filename="profile_dialog.cpp" line="681"/>
        <source>%Ln profile(s) imported</source>
        <translation><numerusform>%Ln 프로파일 가져오기됨</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="profile_dialog.cpp" line="603"/>
        <location filename="profile_dialog.cpp" line="621"/>
        <location filename="profile_dialog.cpp" line="683"/>
        <source>, %Ln profile(s) skipped</source>
        <translation><numerusform>, %Ln 프로파일 건너뜀</numerusform></translation>
    </message>
    <message>
        <location filename="profile_dialog.cpp" line="685"/>
        <source>Importing profiles</source>
        <translation>프로파일을 가져오고 있습니다</translation>
    </message>
    <message numerus="yes">
        <source>%Ln profile(s) selected</source>
        <translation type="obsolete">
            <numerusform>%Ln profile selected</numerusform>
            <numerusform>%Ln profiles selected</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="models/profile_model.cpp" line="497"/>
        <source>Resetting to default</source>
        <translation>기본값을 다시 설정중입니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="489"/>
        <source>Imported profile</source>
        <translation>프로파일을 가져왔습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="451"/>
        <source>This is a system provided profile</source>
        <translation>이것은 시스템 제공된 프로파일입니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="471"/>
        <source>A profile change for this name is pending</source>
        <translation>이 이름의 프로파일 변경을 보류하고 있습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="473"/>
        <source> (See: %1)</source>
        <translation> (참조: %1)</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="477"/>
        <source>This is an invalid profile definition</source>
        <translation>잘못된 프로파일 정의입니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="481"/>
        <source>A profile already exists with this name</source>
        <translation>이 이름의 프로파일이 이미 있습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="485"/>
        <source>A profile with this name is being deleted</source>
        <translation>이 이름의 프로파일이 삭제되고 있습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="516"/>
        <source>Created from default settings</source>
        <translation>기본 설정으로 만들었습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="541"/>
        <source>system provided</source>
        <translation>시스템 제공</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="557"/>
        <source>deleted</source>
        <translation>삭제됨</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="774"/>
        <location filename="models/profile_model.cpp" line="782"/>
        <location filename="models/profile_model.cpp" line="788"/>
        <source>copy</source>
        <comment>noun</comment>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1067"/>
        <source>Exporting profiles while changes are pending is not allowed</source>
        <translation>변경 사항이 보류중인 동안 프로파일을 내보낼 수 없습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1078"/>
        <source>No profiles found to export</source>
        <translation>내보낼 프로파일을 찾지 못했습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1228"/>
        <source>Can&apos;t delete profile directory</source>
        <translation>프로파일 디렉터리를 삭제할 수 없습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1268"/>
        <source>A profile name cannot contain the following characters: %1</source>
        <translation>프로파일 이름에는 다음 문자를 사용할 수 없습니다: %1</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1275"/>
        <source>A profile name cannot contain the &apos;/&apos; character</source>
        <translation>프로파일 이름에는 &apos;/&apos; 문자를 사용할 수 없습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="1272"/>
        <source>A profile cannot start or end with a period (.)</source>
        <translation>프로파일은 마침표(.)로 시작하거나 끝낼 수 없습니다</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="274"/>
        <source>Default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="276"/>
        <source>Global</source>
        <translation>전역</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="278"/>
        <source>Personal</source>
        <translation>개인</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="525"/>
        <source>Renamed from: %1</source>
        <translation>다음으로부터 이름 변경: %1</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="536"/>
        <source>Copied from: %1</source>
        <translation>다음으로부터 복사됨: %1</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="562"/>
        <source>renamed to %1</source>
        <translation>%1로 이름 변경</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="641"/>
        <source>Profile</source>
        <translation>프로파일</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="643"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
</context>
<context>
    <name>ProfileSortModel</name>
    <message>
        <location filename="models/profile_model.cpp" line="88"/>
        <source>All profiles</source>
        <translation>모든 프로파일</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="89"/>
        <source>Personal profiles</source>
        <translation>개인 프로파일</translation>
    </message>
    <message>
        <location filename="models/profile_model.cpp" line="90"/>
        <source>Global profiles</source>
        <translation>전역 프로파일</translation>
    </message>
</context>
<context>
    <name>ProgressFrame</name>
    <message>
        <location filename="progress_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="progress_frame.ui" line="38"/>
        <source>Loading</source>
        <translation>로딩중</translation>
    </message>
</context>
<context>
    <name>ProtoTree</name>
    <message>
        <location filename="proto_tree.cpp" line="57"/>
        <source>Packet details</source>
        <translation>패킷 상세 정보</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="225"/>
        <source>Not a field or protocol</source>
        <translation>필드나 프로토콜이 아님</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="226"/>
        <location filename="proto_tree.cpp" line="365"/>
        <source>No field reference available for text labels.</source>
        <translation>텍스트 레이블에 사용가능한 필드 레퍼런스가 없습니다.</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="270"/>
        <source>Expand Subtrees</source>
        <translation>하위 트리 펼침</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="272"/>
        <source>Collapse Subtrees</source>
        <translation>하위 트리 접음</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="274"/>
        <source>Expand All</source>
        <translation>모두 펼침</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="275"/>
        <source>Collapse All</source>
        <translation>모두 접음</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="323"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="324"/>
        <source>All Visible Items</source>
        <translation>모든 표시 가능한 항목</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="325"/>
        <source>All Visible Selected Tree Items</source>
        <translation>모든 보이는 선택된 트리 항목</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="327"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="329"/>
        <source>Field Name</source>
        <translation>필드 이름</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="331"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="334"/>
        <source>As Filter</source>
        <translation>필터로</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="351"/>
        <source>Wiki Protocol Page</source>
        <translation>위키 프로토콜 페이지</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="354"/>
        <source>Filter Field Reference</source>
        <translation>필터 필드 참조</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="655"/>
        <source>Copied </source>
        <translation>복사됨 </translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="207"/>
        <source>Wiki Page for %1</source>
        <translation>%1 위키 페이지</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="208"/>
        <source>&lt;p&gt;The Wireshark Wiki is maintained by the community.&lt;/p&gt;&lt;p&gt;The page you are about to load might be wonderful, incomplete, wrong, or nonexistent.&lt;/p&gt;&lt;p&gt;Proceed to the wiki?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wireshark 위키는 커뮤니티에 의해 운영되고 있습니다.&lt;/p&gt;&lt;p&gt;지금 보고있는 페이지는 훌륭하거나 불완전하거나 잘못되었거나 존재 하지 않을지도 모릅니다.&lt;/p&gt;&lt;p&gt;위키로 이동 하시겠습니까?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="proto_tree.cpp" line="303"/>
        <source>Colorize with Filter</source>
        <translation>필터에 색상</translation>
    </message>
</context>
<context>
    <name>ProtocolHierarchyDialog</name>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="30"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="35"/>
        <source>Percent Packets</source>
        <translation>패킷 비율</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="40"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="45"/>
        <source>Percent Bytes</source>
        <translation>바이트 비율</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="50"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="55"/>
        <source>Bits/s</source>
        <translation>비트/초</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="60"/>
        <source>End Packets</source>
        <translation>마지막 패킷</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="65"/>
        <source>End Bytes</source>
        <translation>마지막 바이트</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="70"/>
        <source>End Bits/s</source>
        <translation>초당 마지막 비트레이트</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="75"/>
        <source>PDUs</source>
        <translation>PDUs</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="83"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;smallgt;&lt;igt;필터힌트.&lt;/igt;&lt;/smallgt;</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="103"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="106"/>
        <source>Copy stream list as CSV.</source>
        <translation>CSV로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="111"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.ui" line="114"/>
        <source>Copy stream list as YAML.</source>
        <translation>YAML로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="177"/>
        <source>Protocol Hierarchy Statistics</source>
        <translation>프로토콜 계층 통계</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="229"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="233"/>
        <source>as CSV</source>
        <translation>CSV로</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="236"/>
        <source>as YAML</source>
        <translation>YAML로</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="302"/>
        <source>No display filter.</source>
        <translation>표시 필터가 없습니다.</translation>
    </message>
    <message>
        <location filename="protocol_hierarchy_dialog.cpp" line="304"/>
        <source>Display filter: %1</source>
        <translation>표시 필터: %1</translation>
    </message>
</context>
<context>
    <name>ProtocolPreferencesMenu</name>
    <message>
        <location filename="protocol_preferences_menu.cpp" line="172"/>
        <source>Protocol Preferences</source>
        <translation>프로토콜 설정</translation>
    </message>
    <message>
        <location filename="protocol_preferences_menu.cpp" line="199"/>
        <source>No protocol preferences available</source>
        <translation>프로토콜 설정을 사용할 수 없습니다</translation>
    </message>
    <message>
        <location filename="protocol_preferences_menu.cpp" line="204"/>
        <source>Disable %1</source>
        <translation>%1 해제</translation>
    </message>
    <message>
        <location filename="protocol_preferences_menu.cpp" line="210"/>
        <source>%1 has no preferences</source>
        <translation>%1에는 설정이 없습니다</translation>
    </message>
    <message>
        <location filename="protocol_preferences_menu.cpp" line="219"/>
        <source>Open %1 preferences…</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="70"/>
        <source>Average Throughput (bits/s)</source>
        <translation>평균 처리량(비트/초)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="71"/>
        <source>Round Trip Time (ms)</source>
        <translation>왕복 지연 시간 (밀리초)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="72"/>
        <source>Segment Length (B)</source>
        <translation>세그먼트 길이 (바이트)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="73"/>
        <source>Sequence Number (B)</source>
        <translation>순서 번호 (바이트)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="74"/>
        <source>Time (s)</source>
        <translation>시간 (초)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="75"/>
        <source>Window Size (B)</source>
        <translation>창 크기 (바이트)</translation>
    </message>
    <message>
        <location filename="capture_file.cpp" line="80"/>
        <source>[no capture file]</source>
        <translation>[캡쳐 파일 없음]</translation>
    </message>
    <message>
        <location filename="conversation_dialog.cpp" line="72"/>
        <source>Conversation</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="models/atap_data_model.cpp" line="765"/>
        <source>Bars show the relative timeline for each conversation.</source>
        <translation>막대는 각 대화에 대한 상대적인 시간 표시열(타임 라인)을 표시합니다.</translation>
    </message>
    <message>
        <location filename="endpoint_dialog.cpp" line="60"/>
        <source>Endpoint</source>
        <translation>종단점</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="68"/>
        <location filename="filter_action.cpp" line="237"/>
        <source>Apply as Filter</source>
        <translation>필터로 적용</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="71"/>
        <location filename="filter_action.cpp" line="237"/>
        <source>Prepare as Filter</source>
        <translation>필터로 준비</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="74"/>
        <source>Find</source>
        <translation>찾기</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="77"/>
        <source>Colorize</source>
        <translation>색상화</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="80"/>
        <source>Look Up</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="83"/>
        <location filename="filter_action.cpp" line="280"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="86"/>
        <location filename="filter_action.cpp" line="138"/>
        <location filename="filter_action.cpp" line="189"/>
        <source>UNKNOWN</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="120"/>
        <source>Selected</source>
        <translation>선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="123"/>
        <source>Not Selected</source>
        <translation>선택되어 있지 않습니다</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="126"/>
        <source>…and Selected</source>
        <translation>…그리고 선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="129"/>
        <source>…or Selected</source>
        <translation>…또는 선택됨</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="132"/>
        <source>…and not Selected</source>
        <translation>…그리고 선택되지 않음</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="135"/>
        <source>…or not Selected</source>
        <translation>…또는 선택되지 않음</translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="162"/>
        <location filename="filter_action.cpp" line="165"/>
        <location filename="filter_action.cpp" line="171"/>
        <location filename="filter_action.cpp" line="174"/>
        <source>A </source>
        <translation>A </translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="168"/>
        <location filename="filter_action.cpp" line="186"/>
        <source>B </source>
        <translation>B </translation>
    </message>
    <message>
        <location filename="filter_action.cpp" line="177"/>
        <location filename="filter_action.cpp" line="180"/>
        <location filename="filter_action.cpp" line="183"/>
        <source>Any </source>
        <translation>모두 </translation>
    </message>
    <message>
        <location filename="simple_dialog.cpp" line="350"/>
        <source>Don&apos;t show this message again.</source>
        <translation>이 메시지는 다시 표시되지 않습니다.</translation>
    </message>
    <message>
        <location filename="simple_dialog.cpp" line="327"/>
        <source>Multiple problems found</source>
        <translation>여러 가지 문제가 발견됨</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="119"/>
        <source>%1 (%L2%)</source>
        <translation>%1 (%L2%)</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="33"/>
        <source>No entries.</source>
        <translation>항목이 없습니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="34"/>
        <source>%1 entries.</source>
        <translation>%1 항목.</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="136"/>
        <source>Base station</source>
        <translation>기지국</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="221"/>
        <source>&lt;Broadcast&gt;</source>
        <translation>&lt;방송&gt;</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="223"/>
        <source>&lt;Hidden&gt;</source>
        <translation>&lt;숨김&gt;</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="482"/>
        <source>BSSID</source>
        <translation>BSSID</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="483"/>
        <source>Beacons</source>
        <translation>비콘</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="484"/>
        <source>Data Pkts</source>
        <translation>데이터 패킷</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="485"/>
        <source>Protection</source>
        <translation>보호</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="487"/>
        <source>Address</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="488"/>
        <source>Pkts Sent</source>
        <translation>발신 패킷</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="489"/>
        <source>Pkts Received</source>
        <translation>수신 패킷</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="490"/>
        <source>Comment</source>
        <translation>주석</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="89"/>
        <source>Wrong sequence number</source>
        <translation>잘못된 순서 번호</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="92"/>
        <source>Payload changed to PT=%1</source>
        <translation>페이로드가 PT=%1 에 바뀌었습니다</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="95"/>
        <source>Incorrect timestamp</source>
        <translation>잘못된 타임스탬프</translation>
    </message>
    <message>
        <location filename="iax2_analysis_dialog.cpp" line="103"/>
        <source>Marker missing?</source>
        <translation>마커 부족?</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="210"/>
        <location filename="lte_mac_statistics_dialog.cpp" line="413"/>
        <source>C-RNTI</source>
        <translation>C-RNTI</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="210"/>
        <location filename="lte_mac_statistics_dialog.cpp" line="413"/>
        <source>SPS-RNTI</source>
        <translation>SPS-RNTI</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="470"/>
        <source>RNTI</source>
        <translation>RNTI</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="470"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="470"/>
        <source>UEId</source>
        <translation>UEId</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="471"/>
        <source>UL Frames</source>
        <translation>UL 프레임</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="471"/>
        <source>UL Bytes</source>
        <translation>UL 바이트</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="471"/>
        <source>UL MB/s</source>
        <translation>UL MB/s</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="472"/>
        <source>UL Padding %</source>
        <translation>UL 패딩 %</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="472"/>
        <source>UL Re TX</source>
        <translation>UL Re TX</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="473"/>
        <source>DL Frames</source>
        <translation>DL 프레임</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="473"/>
        <source>DL Bytes</source>
        <translation>DL 바이트</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="473"/>
        <source>DL MB/s</source>
        <translation>DL MB/s</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="474"/>
        <source>DL Padding %</source>
        <translation>DL 패딩 %</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="474"/>
        <source>DL CRC Failed</source>
        <translation>DL CRC 실패</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="475"/>
        <source>DL ReTX</source>
        <translation>DL ReTX</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="481"/>
        <source>LCID 1</source>
        <translation>LCID 1</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="481"/>
        <source>LCID 2</source>
        <translation>LCID 2</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="481"/>
        <source>LCID 3</source>
        <translation>LCID 3</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="482"/>
        <source>LCID 4</source>
        <translation>LCID 4</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="482"/>
        <source>LCID 5</source>
        <translation>LCID 5</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="482"/>
        <source>LCID 6</source>
        <translation>LCID 6</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="483"/>
        <source>LCID 7</source>
        <translation>LCID 7</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="483"/>
        <source>LCID 8</source>
        <translation>LCID 8</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="483"/>
        <source>LCID 9</source>
        <translation>LCID 9</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="484"/>
        <source>LCID 10</source>
        <translation>LCID 10</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="484"/>
        <source>LCID 32</source>
        <translation>LCID 32</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="484"/>
        <source>LCID 33</source>
        <translation>LCID 33</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="485"/>
        <source>LCID 34</source>
        <translation>LCID 34</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="485"/>
        <source>LCID 35</source>
        <translation>LCID 35</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="485"/>
        <source>LCID 36</source>
        <translation>LCID 36</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="486"/>
        <source>LCID 37</source>
        <translation>LCID 37</translation>
    </message>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="486"/>
        <source>LCID 38</source>
        <translation>LCID 38</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="124"/>
        <source>TM</source>
        <translation>TM</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="127"/>
        <source>UM</source>
        <translation>UM</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="130"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="133"/>
        <source>Predef</source>
        <translation>규정</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="137"/>
        <source>Unknown (%1)</source>
        <translation>알 수 없음 (%1)</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="144"/>
        <source>CCCH</source>
        <translation>CCCH</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="147"/>
        <source>SRB-%1</source>
        <translation>SRB-%1</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="150"/>
        <source>DRB-%1</source>
        <translation>DRB-%1</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="154"/>
        <location filename="models/pref_models.cpp" line="223"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="638"/>
        <source>UE Id</source>
        <translation>UE Id</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="642"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="643"/>
        <source>Mode</source>
        <translation>모드</translation>
    </message>
    <message>
        <location filename="lte_rlc_statistics_dialog.cpp" line="644"/>
        <source>Priority</source>
        <translation>우선 순위</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="114"/>
        <location filename="models/interface_tree_model.cpp" line="34"/>
        <source>default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="capture_options_dialog.cpp" line="122"/>
        <location filename="models/interface_tree_model.cpp" line="189"/>
        <source>DLT %1</source>
        <translation>DLT %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="990"/>
        <source>Invalid Display Filter</source>
        <translation>잘못된 표시 필터</translation>
    </message>
    <message>
        <location filename="main.cpp" line="991"/>
        <source>The filter expression %1 isn&apos;t a valid display filter. (%2).</source>
        <translation>필터 표현식 %1 은(는) 올바른 표시 필터가 아닙니다. (%2).</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="106"/>
        <location filename="manage_interfaces_dialog.cpp" line="109"/>
        <location filename="manage_interfaces_dialog.cpp" line="112"/>
        <location filename="manage_interfaces_dialog.cpp" line="115"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="106"/>
        <source>No remote interfaces found.</source>
        <translation>원격 인터페이스를 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="112"/>
        <source>PCAP not found</source>
        <translation>PCAP을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="manage_interfaces_dialog.cpp" line="115"/>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="226"/>
        <source>Default</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="228"/>
        <source>Changed</source>
        <translation>바뀜</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="419"/>
        <source>Has this preference been changed?</source>
        <translation>이 환경 설정이 바뀌었습니까?</translation>
    </message>
    <message>
        <location filename="models/pref_models.cpp" line="434"/>
        <source>Default value is empty</source>
        <translation>기본값이 비었습니다</translation>
    </message>
    <message>
        <location filename="packet_diagram.cpp" line="179"/>
        <source>Gap in dissection</source>
        <translation>분해 간격</translation>
    </message>
    <message>
        <location filename="module_preferences_scroll_area.cpp" line="204"/>
        <source>Edit…</source>
        <translation>편집…</translation>
    </message>
    <message>
        <location filename="module_preferences_scroll_area.cpp" line="232"/>
        <source>Browse…</source>
        <translation>찾아가기…</translation>
    </message>
</context>
<context>
    <name>QObject::QObject</name>
    <message>
        <location filename="lte_mac_statistics_dialog.cpp" line="480"/>
        <source>CCCH</source>
        <translation>CCCH</translation>
    </message>
</context>
<context>
    <name>RemoteCaptureDialog</name>
    <message>
        <location filename="remote_capture_dialog.ui" line="14"/>
        <source>Remote Interface</source>
        <translation>원격 인터페이스</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="27"/>
        <source>Host:</source>
        <translation>호스트:</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="41"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="62"/>
        <source>Authentication</source>
        <translation>인증</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="71"/>
        <source>Null authentication</source>
        <translation>Null 인증</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="81"/>
        <source>Password authentication</source>
        <translation>암호 인증</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="93"/>
        <source>Username:</source>
        <translation>사용자 이름:</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.ui" line="110"/>
        <source>Password:</source>
        <translation>암호:</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.cpp" line="48"/>
        <location filename="remote_capture_dialog.cpp" line="80"/>
        <location filename="remote_capture_dialog.cpp" line="133"/>
        <source>Clear list</source>
        <translation>목록 지우기</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.cpp" line="120"/>
        <location filename="remote_capture_dialog.cpp" line="122"/>
        <location filename="remote_capture_dialog.cpp" line="124"/>
        <location filename="remote_capture_dialog.cpp" line="126"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.cpp" line="120"/>
        <source>No remote interfaces found.</source>
        <translation>원격 인터페이스를 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="remote_capture_dialog.cpp" line="124"/>
        <source>PCAP not found</source>
        <translation>PCAP을 찾을 수 없습니다</translation>
    </message>
</context>
<context>
    <name>RemoteSettingsDialog</name>
    <message>
        <location filename="remote_settings_dialog.ui" line="20"/>
        <source>Remote Capture Settings</source>
        <translation>원격 캡쳐 설정</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="31"/>
        <source>Capture Options</source>
        <translation>캡쳐 선택 사항</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="38"/>
        <source>Do not capture own RPCAP traffic</source>
        <translation>자신의 RPCAP 트래픽은 캡쳐하지 않습니다</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="45"/>
        <source>Use UDP for data transfer</source>
        <translation>데이터 전송에 UDP을 사용</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="52"/>
        <source>Sampling Options</source>
        <translation>표본 선택 사항</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="62"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="71"/>
        <source>1 of</source>
        <translation>1 의</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="85"/>
        <source>packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="96"/>
        <source>1 every </source>
        <translation>1 매 </translation>
    </message>
    <message>
        <location filename="remote_settings_dialog.ui" line="110"/>
        <source>milliseconds</source>
        <translation>밀리초</translation>
    </message>
</context>
<context>
    <name>ResolvedAddressesDialog</name>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="24"/>
        <source>Hosts</source>
        <translation>호스트</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="32"/>
        <source>Search for entry (min 3 characters)</source>
        <translation>항목 검색(최소 3자)</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="61"/>
        <source>Ports</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="69"/>
        <source>Search for port or name</source>
        <translation>포트 또는 이름을 검색</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="92"/>
        <source>Capture File Comments</source>
        <translation>캡쳐 파일 의견</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="121"/>
        <source>Comment</source>
        <translation>의견</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="124"/>
        <source>Show the comment.</source>
        <translation>의견을 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="132"/>
        <source>IPv4 Hash Table</source>
        <translation>IPv4 해시 표</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="135"/>
        <source>Show the IPv4 hash table entries.</source>
        <translation>IPv4 해시 표 항목을 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="143"/>
        <source>IPv6 Hash Table</source>
        <translation>IPv6 해시 표</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="146"/>
        <source>Show the IPv6 hash table entries.</source>
        <translation>IPv6 해시 표 항목을 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="151"/>
        <source>Show All</source>
        <translation>모두 표시</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="154"/>
        <source>Show all address types.</source>
        <translation>모든 주소 유형을 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="159"/>
        <source>Hide All</source>
        <translation>모두 숨기기</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="162"/>
        <source>Hide all address types.</source>
        <translation>모든 주소 유형을 숨깁니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="173"/>
        <source>IPv4 and IPv6 Addresses (hosts)</source>
        <translation>IPv4 와 IPv6 주소 (호스트)</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="176"/>
        <source>Show resolved IPv4 and IPv6 host names in &quot;hosts&quot; format.</source>
        <translation>해석된 IPv4 와 IPv6 호스트 이름을 &quot;호스트&quot; 형식으로 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="187"/>
        <source>Port names (services)</source>
        <translation>포트 이름 (서비스)</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="190"/>
        <source>Show resolved port names in &quot;services&quot; format.</source>
        <oldsource>Show resolved port names names in &quot;servies&quot; format.</oldsource>
        <translation>해석한 포트 이름을 &quot;services&quot; 형식으로 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="201"/>
        <source>Ethernet Addresses</source>
        <translation>이더넷 주소</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="204"/>
        <source>Show resolved Ethernet addresses in &quot;ethers&quot; format.</source>
        <translation>해석된 이더넷 주소를 &quot;ethers&quot; 형식으로 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="215"/>
        <source>Ethernet Well-Known Addresses</source>
        <translation>이더넷 알려진 주소</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="218"/>
        <source>Show well-known Ethernet addresses in &quot;ethers&quot; format.</source>
        <translation>&quot;ether&quot; 형식으로 알려진 이더넷 주소를 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="229"/>
        <source>Ethernet Manufacturers</source>
        <translation>이더넷 제조업체</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.ui" line="232"/>
        <source>Show Ethernet manufacturers in &quot;ethers&quot; format.</source>
        <translation>&quot;ethers&quot; 형식으로 이더넷 제조업체를 표시합니다.</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="39"/>
        <source>[no file]</source>
        <translation>[파일 없음]</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="44"/>
        <source>Resolved Addresses</source>
        <translation>해석한 주소</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="187"/>
        <source># Resolved addresses found in %1</source>
        <translation># %1에서 해석한 주소를 발견</translation>
    </message>
    <message>
        <location filename="resolved_addresses_dialog.cpp" line="191"/>
        <source># Comments
#
# </source>
        <translation># 주석 
#
# </translation>
    </message>
</context>
<context>
    <name>ResponseTimeDelayDialog</name>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="154"/>
        <source>%1 Response Time Delay Statistics</source>
        <translation>%1 응답 시간 지연 통계</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="160"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="160"/>
        <source>Messages</source>
        <translation>메시지</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="161"/>
        <source>Min SRT</source>
        <translation>최소 SRT</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="161"/>
        <source>Max SRT</source>
        <translation>최대 SRT</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="161"/>
        <source>Avg SRT</source>
        <translation>평균 SRT</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="162"/>
        <source>Min in Frame</source>
        <translation>프레임의 최소</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="162"/>
        <source>Max in Frame</source>
        <translation>프레임의 최대</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="163"/>
        <source>Open Requests</source>
        <translation>열기 요청</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="163"/>
        <source>Discarded Responses</source>
        <translation>취소된 응답</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="164"/>
        <source>Repeated Requests</source>
        <translation>반복된 요청</translation>
    </message>
    <message>
        <location filename="response_time_delay_dialog.cpp" line="164"/>
        <source>Repeated Responses</source>
        <translation>반복된 응답</translation>
    </message>
</context>
<context>
    <name>RpcServiceResponseTimeDialog</name>
    <message>
        <location filename="rpc_service_response_time_dialog.cpp" line="109"/>
        <source>&lt;small&gt;&lt;i&gt;Select a program and version and enter a filter if desired, then press Apply.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;프로그램과 버전을 선택하고, 필요한 경우 필터를 입력하고 적용을 클릭하십시오.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="rpc_service_response_time_dialog.cpp" line="117"/>
        <source>Version:</source>
        <translation>버전:</translation>
    </message>
    <message>
        <location filename="rpc_service_response_time_dialog.cpp" line="119"/>
        <source>Program:</source>
        <translation>프로그램:</translation>
    </message>
    <message>
        <location filename="rpc_service_response_time_dialog.cpp" line="122"/>
        <source>DCE-RPC Service Response Times</source>
        <translation>DCE-RPC 서비스 응답 시간</translation>
    </message>
    <message>
        <location filename="rpc_service_response_time_dialog.cpp" line="133"/>
        <source>ONC-RPC Service Response Times</source>
        <translation>ONC-RPC 서비스 응답 시간</translation>
    </message>
</context>
<context>
    <name>RsaKeysFrame</name>
    <message>
        <location filename="rsa_keys_frame.ui" line="17"/>
        <source>RSA Keys</source>
        <translation>RSA 키</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="23"/>
        <source>RSA private keys are loaded from a file or PKCS #11 token.</source>
        <translation>RSA 개인 키는 파일 또는 PKCS #11 토큰에서 불러옵니다.</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="35"/>
        <source>Add new keyfile…</source>
        <translation>새로운 키 파일 추가…</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="42"/>
        <source>Add new token…</source>
        <translation>새로운 토큰 추가…</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="52"/>
        <source>Remove key</source>
        <translation>키 제거</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="68"/>
        <source>PKCS #11 provider libraries.</source>
        <translation>PKCS #11 공급자 라이브러리입니다.</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="87"/>
        <source>Add new provider…</source>
        <translation>새로운 공급자 추가…</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.ui" line="97"/>
        <source>Remove provider</source>
        <translation>공급자 제거</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="108"/>
        <location filename="rsa_keys_frame.cpp" line="149"/>
        <source>Add PKCS #11 token or key</source>
        <translation>PKCS #11 토큰 또는 키 추가</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="109"/>
        <source>No new PKCS #11 tokens or keys found, consider adding a PKCS #11 provider.</source>
        <translation>새로운 PKCS #11 토큰 또는 키를 찾을 수 없습니다. PKCS #11 공급자 추가하는 것이 좋습니다.</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="116"/>
        <source>Select a new PKCS #11 token or key</source>
        <translation>새로운 PKCS #11 토큰 또는 키를 선택</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="117"/>
        <source>PKCS #11 token or key</source>
        <translation>PKCS #11 토큰 또는 키</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="139"/>
        <source>Enter PIN or password for %1 (it will be stored unencrypted)</source>
        <translation>%1에 대한 PIN 또는 암호를 입력합니다 (암호화되지 않은 상태로 저장됩니다)</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="140"/>
        <source>Enter PIN or password for key</source>
        <translation>에 대한 PIN 또는 암호를 입력합니다</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="150"/>
        <source>Key could not be added: %1</source>
        <translation>키를 추가할 수 없습니다: %1</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="161"/>
        <source>RSA private key (*.pem *.p12 *.pfx *.key);;All Files (</source>
        <translation>RSA 개인 키 (*.pem *.p12 *.pfx *.key);;모든 파일 (</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="163"/>
        <location filename="rsa_keys_frame.cpp" line="181"/>
        <source>Select RSA private key file</source>
        <translation>RSA 개인 키 파일 선택</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="228"/>
        <source>Libraries (*.dll)</source>
        <translation>라이브러리 (*.dll)</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="230"/>
        <source>Libraries (*.so)</source>
        <translation>라이브러리 (*.so)</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="232"/>
        <source>Select PKCS #11 Provider Library</source>
        <translation>PKCS #11 공급자 라이브러리 선택</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="264"/>
        <source>Changes will apply after a restart</source>
        <translation>다시 시작한 후 변경 사항이 적용됩니다</translation>
    </message>
    <message>
        <location filename="rsa_keys_frame.cpp" line="265"/>
        <source>PKCS #11 provider %1 will be removed after the next restart.</source>
        <translation>PKCS #11 공급자 %1 은(는) 다음 다시 시작하면 제거됩니다.</translation>
    </message>
</context>
<context>
    <name>RtpAnalysisDialog</name>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="387"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="388"/>
        <source>Sequence</source>
        <translation>순서</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="389"/>
        <source>Delta (ms)</source>
        <translation>간격 (밀리초)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="390"/>
        <source>Jitter (ms)</source>
        <oldsource>Jitter</oldsource>
        <translation>지터 (밀리초)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="391"/>
        <source>Skew</source>
        <translation>Skew</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="392"/>
        <source>Bandwidth</source>
        <translation>대역폭</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="393"/>
        <source>Marker</source>
        <translation>마커</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="394"/>
        <source>Status</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="402"/>
        <location filename="rtp_analysis_dialog.cpp" line="430"/>
        <source>Stream %1</source>
        <translation>스트림 %s</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="438"/>
        <source>Stream %1 Jitter</source>
        <translation>스트림 %1 Jitter</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="446"/>
        <source>Stream %1 Difference</source>
        <translation>스트림 %1 차이</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="454"/>
        <source>Stream %1 Delta</source>
        <translation>스트림 %1 Delta</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="539"/>
        <source> %1 streams, </source>
        <translation> %1 스트림, </translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="895"/>
        <source>Save one stream CSV</source>
        <translation>한 개의 스트림 CSV에 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="899"/>
        <source>Save all stream&apos;s CSV</source>
        <translation>모든 스트림의 CSV에 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1145"/>
        <source>&amp;Analyze</source>
        <translation>분석(&amp;A)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1146"/>
        <source>Open the analysis window for the selected stream(s)</source>
        <translation>선택한 스트림에 대한 분석창 열기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1154"/>
        <source>&amp;Set List</source>
        <translation>목록 설정(&amp;S)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1157"/>
        <source>&amp;Add to List</source>
        <translation>목록에 추가(&amp;A)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1160"/>
        <source>&amp;Remove from List</source>
        <translation>목록으로부터 제거(&amp;R)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1155"/>
        <source>Replace existing list in RTP Analysis Dialog with new one</source>
        <translation>RTP 분석 대화상자에 존재하는 목록을 새로운 목록으로 교체</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1158"/>
        <source>Add new set to existing list in RTP Analysis Dialog</source>
        <translation>RTP 분석 대화상자에 존재하는 목록에 새로운 집합 추가</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="1161"/>
        <source>Remove selected streams from list in RTP Analysis Dialog</source>
        <translation>RTP 분석 대화상자에 있는 목록으로부터 선택한 스트림 제거</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="26"/>
        <source>Graph</source>
        <translation>그래프</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="70"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="102"/>
        <source>&amp;Export</source>
        <translation>내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="105"/>
        <source>Open export menu</source>
        <translation>내보내기 메뉴 열기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="110"/>
        <source>CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="113"/>
        <source>Save tables as CSV.</source>
        <translation>CSV 형식으로 테이블 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="118"/>
        <source>Current Tab Stream CSV</source>
        <translation>현재 탭 스트림 CSV</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="121"/>
        <source>Save the table on the current tab as CSV.</source>
        <translation>CSV 형식으로 현재 탭 상의 테이블 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="126"/>
        <source>All Tab Streams CSV</source>
        <translation>모든 탭 스트림 CSV</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="129"/>
        <source>Save the table from all tabs as CSV.</source>
        <translation>CSV 형식으로 모든 탭으로부터 테이블을 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="134"/>
        <source>Save Graph</source>
        <translation>그래프 저장</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="137"/>
        <source>Save the graph image.</source>
        <translation>그래프의 그림을 저장합니다.</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="142"/>
        <source>Go to Packet</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="145"/>
        <source>Select the corresponding packet in the packet list.</source>
        <translation>패킷 목록에서 관련 패킷을 선택합니다.</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="148"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="153"/>
        <source>Next Problem Packet</source>
        <translation>다음 문제 패킷</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="156"/>
        <source>Go to the next problem packet</source>
        <translation>다음 문제 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="159"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="164"/>
        <location filename="rtp_analysis_dialog.ui" line="172"/>
        <source>Prepare &amp;Filter</source>
        <translation>필터 준비(&amp;F)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="167"/>
        <source>Prepare a filter matching the selected stream(s).</source>
        <translation>선택된 스트림과 매칭된 필터를 준비합니다.</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="182"/>
        <source>&amp;Current Tab</source>
        <translation>현재 탭(&amp;C)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="185"/>
        <source>Prepare a filter matching current tab.</source>
        <translation>현재 탭과 일치하는 필터를 준비합니다.</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="190"/>
        <source>&amp;All Tabs</source>
        <translation>모든 탭(&amp;A)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.ui" line="193"/>
        <source>Prepare a filter matching all tabs.</source>
        <translation>모든 탭과 일치하는 필터를 준비합니다.</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="265"/>
        <source>RTP Stream Analysis</source>
        <translation>RTP 스트림 분석</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="638"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프 저장…</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="540"/>
        <source> G: Go to packet, N: Next problem packet</source>
        <translation> G: 패킷으로 이동, N: 다음 문제의 패킷</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="623"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="624"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="625"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="627"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="rtp_analysis_dialog.cpp" line="905"/>
        <source>Comma-separated values (*.csv)</source>
        <translation>쉼표로 구분된 값 형식 (*.csv)</translation>
    </message>
</context>
<context>
    <name>RtpAudioStream</name>
    <message>
        <location filename="rtp_audio_stream.cpp" line="758"/>
        <location filename="rtp_audio_stream.cpp" line="761"/>
        <source>%1 does not support PCM at %2. Preferred format is %3</source>
        <translation>%1 은(는) %2 에서 PCM을 지원하지 않습니다. 기본 형식은 %3 입니다</translation>
    </message>
</context>
<context>
    <name>RtpPlayerDialog</name>
    <message>
        <location filename="rtp_player_dialog.ui" line="14"/>
        <location filename="rtp_player_dialog.cpp" line="183"/>
        <source>RTP Player</source>
        <translation>RTP 플레이어</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="44"/>
        <location filename="rtp_player_dialog.ui" line="630"/>
        <source>Play</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="52"/>
        <source>Source Address</source>
        <translation>발신지 주소</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="57"/>
        <source>Source Port</source>
        <translation>발신지 포트</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="62"/>
        <source>Destination Address</source>
        <translation>목적지 주소</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="67"/>
        <source>Destination Port</source>
        <translation>목적지 포트</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="72"/>
        <source>SSRC</source>
        <translation>SSRC</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="77"/>
        <source>Setup Frame</source>
        <translation>설치 프레임</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="82"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="87"/>
        <source>Time Span (s)</source>
        <translation>시간 간격 (초)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="108"/>
        <source>Payloads</source>
        <translation>페이로드</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="117"/>
        <source>&lt;small&gt;&lt;i&gt;No audio&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;오디오 없음&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="132"/>
        <source>Start playback of all unmuted streams</source>
        <translation>모든 언뮤트된 스트림의 재생을 시작합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="142"/>
        <source>Pause/unpause playback</source>
        <translation>일시정지/시작</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="168"/>
        <source>Stop playback</source>
        <translation>정지</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="194"/>
        <source>Enable/disable skipping of silence during playback</source>
        <translation>재생 중 침묵 구간을 건너뛰기/건너뛰지 않기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="217"/>
        <source>Min silence:</source>
        <translation>최소 침묵:</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="224"/>
        <source>Minimum silence duration to skip in seconds</source>
        <translation>초 단위 최소스킵 지속시간</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="259"/>
        <source>Output Device:</source>
        <translation>출력 장치:</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="285"/>
        <source>Output Audio Rate:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="299"/>
        <source>Jitter Buffer:</source>
        <translation>지터 버퍼:</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="306"/>
        <source>The simulated jitter buffer in milliseconds.</source>
        <translation>시뮬레이션된 지터 버퍼 밀리초 입니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="338"/>
        <source>Playback Timing:</source>
        <translation>재생 시간:</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="345"/>
        <source>&lt;strong&gt;Jitter Buffer&lt;/strong&gt;: Use jitter buffer to simulate the RTP stream as heard by the end user.
&lt;br/&gt;
&lt;strong&gt;RTP Timestamp&lt;/strong&gt;: Use RTP Timestamp instead of the arriving packet time. This will not reproduce the RTP stream as the user heard it, but is useful when the RTP is being tunneled and the original packet timing is missing.
&lt;br/&gt;
&lt;strong&gt;Uninterrupted Mode&lt;/strong&gt;: Ignore the RTP Timestamp. Play the stream as it is completed. This is useful when the RTP timestamp is missing.</source>
        <oldsource>&lt;strong&gt;Jitter Buffer&lt;/strong&gt;: Use jitter buffer to simulate the RTP stream as heard by the end user.
&lt;br/&gt;
&lt;strong&gt;RTP Timestamp&lt;/strong&gt;: Use RTP Timestamp instead of the arriving packet time. This will not reproduce the RTP stream as the user heard it, but is useful when the RTP is being tunneled and the original packet timing is missing.
&lt;br/&gt;
&lt;strong&gt;Uniterrupted Mode&lt;/strong&gt;: Ignore the RTP Timestamp. Play the stream as it is completed. This is useful when the RTP timestamp is missing.</oldsource>
        <translation>&lt;strong&gt;지터 버퍼&lt;/strong&gt;: 사용자가 들을 수 있는 RTP 스트림을 시뮬레이션 하기 위해 지터 버퍼을 사용하십시오.
&lt;br/&gt;
&lt;strong&gt;RTP 타임스탬프&lt;/strong&gt;: 패킷 도착시간 대신 RTP 타임스탬프를 사용합니다. 이것은 사용자가 들을 RTP 스트림으로 재생성하지는 않겠지만, RTP가 터널링되어 있거나, 원래의 패킷 타이밍을 손실한 경우에 유용합니다.
&lt;br/&gt;
&lt;strong&gt;비 인터럽트 모드&lt;/strong&gt;: RTP 타임스탬프를 무시합니다. 스트림을 완성시킨 것처럼 재생합니다. 이것은 RTP 타임스탬프가 없는 경우에 사용됩니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="353"/>
        <source>Jitter Buffer</source>
        <translation>지터 버퍼</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="358"/>
        <source>RTP Timestamp</source>
        <translation>RTP 시간 찍기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="363"/>
        <source>Uninterrupted Mode</source>
        <translation>비 인터럽트 모드</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="384"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;View the timestamps as time of day (checked) or seconds since beginning of capture (unchecked).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;타임스탬프를 시간으로 표시(체크됨)하거나 캡쳐 시작 시점기준 경과된 초 단위 시간(체크해제됨)으로 표시합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="387"/>
        <source>Time of Day</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="419"/>
        <location filename="rtp_player_dialog.ui" line="427"/>
        <source>&amp;Export</source>
        <translation>내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="422"/>
        <source>Export audio of all unmuted selected channels or export payload of one channel.</source>
        <translation>모든 언뮤트 된 선택된 채널로부터 오디오를 내보내거나 한 채널의 페이로드를 내보내기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="440"/>
        <source>From &amp;cursor</source>
        <translation>커서로부터(&amp;c)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="443"/>
        <source>Save audio data started at the cursor</source>
        <translation>커서에서 시작되는 오디오 데이터 저장하기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="448"/>
        <source>&amp;Stream Synchronized Audio</source>
        <translation>동기화 된 오디오 스트림(&amp;S)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="451"/>
        <source>Save audio data synchronized to start of the earliest stream.</source>
        <translation>가장 최초의 스트림 시작점과 동기화 한 오디오 데이터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="456"/>
        <source>&amp;File Synchronized Audio</source>
        <translation>파일 동기화 오디오(&amp;F)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="459"/>
        <source>Save audio data synchronized to start of the capture file.</source>
        <translation>캡쳐 파일의 시작점에 동기화된 오디오 데이터를 저장합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="464"/>
        <source>&amp;Payload</source>
        <translation>페이로드(&amp;P)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="467"/>
        <source>Save RTP payload of selected stream.</source>
        <translation>선택한 스트림의 RTP 페이로드를 저장합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="472"/>
        <source>Reset Graph</source>
        <translation>그래프를 다시 설정</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="475"/>
        <source>Reset the graph to its initial state.</source>
        <translation>그래프를 초기 상태로 다시 설정합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="560"/>
        <location filename="rtp_player_dialog.ui" line="571"/>
        <source>Go To Setup Packet</source>
        <translation>설정 패킷으로 가기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="563"/>
        <location filename="rtp_player_dialog.ui" line="574"/>
        <source>Go to setup packet of stream currently under the cursor</source>
        <translation>현재 커서 아래 스트림의 패킷을 설정하기 위해 이동합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="597"/>
        <source>Mute</source>
        <translation>음소거</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="600"/>
        <source>Mute selected streams</source>
        <translation>선택한 스트림을 음소거</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="608"/>
        <source>Unmute</source>
        <translation>음소거해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="611"/>
        <source>Unmute selected streams</source>
        <translation>선택된 스트림을 음소거 해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="622"/>
        <source>Invert muting of selected streams</source>
        <translation>선택된 스트림의 음소거를 해제함</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="641"/>
        <source>Route audio to left channel of selected streams</source>
        <translation>선택된 스트림을 왼쪽 채널로 오디오 라우팅</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="649"/>
        <source>Route audio to left and right channel of selected streams</source>
        <translation>선택된 스트림의 왼쪽 채널을 오른쪽 채널로 오디오 라우팅</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="657"/>
        <source>Route audio to right channel of selected streams</source>
        <translation>선택된 스트림을 오른쪽 채널로 라우팅</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="662"/>
        <source>Remove Streams</source>
        <translation>스트림 제거</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="665"/>
        <source>Remove selected streams from the list</source>
        <translation>목록으로부터 선택된 스트림 제거</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="684"/>
        <source>All</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="687"/>
        <source>Select all</source>
        <translation>모두 선택</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="695"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="698"/>
        <source>Clear selection</source>
        <translation>선택내용 해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="706"/>
        <source>Invert</source>
        <translation>반전</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="709"/>
        <source>Invert selection</source>
        <translation>선택 반전</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="717"/>
        <source>Play/Pause</source>
        <translation>재생/일시정지</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="720"/>
        <source>Start playing or pause playing</source>
        <translation>재생 시작 혹은 재생 일시정지</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="728"/>
        <source>Stop</source>
        <translation>정지</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="731"/>
        <source>Stop playing</source>
        <translation>재생 정지</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="739"/>
        <source>I&amp;naudible streams</source>
        <translation>비가청음역 스트림(&amp;I)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="742"/>
        <source>Select/Deselect inaudible streams</source>
        <translation>비가청음역 스트림 선택/선택해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="747"/>
        <source>Inaudible streams</source>
        <translation>비가청음역 스트림</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="757"/>
        <source>&amp;Select</source>
        <translation>선택(&amp;S)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="760"/>
        <source>Select inaudible streams</source>
        <translation>비가청음역 스트림 선택</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="768"/>
        <source>&amp;Deselect</source>
        <translation>선택해제(&amp;D)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="771"/>
        <source>Deselect inaudible streams</source>
        <translation>비가청음역 스트림 선택해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="779"/>
        <source>Prepare &amp;Filter</source>
        <translation>필터 준비(&amp;F)</translation> 
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="782"/>
        <source>Prepare a filter matching the selected stream(s).</source>
        <translation>선택된 스트림에 매칭하는 필터를 준비합니다.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="787"/>
        <source>R&amp;efresh streams</source>
        <translation>스트림 회생(&amp;e)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="790"/>
        <source>Read captured packets from capture in progress to player</source>
        <translation>재생기에 재생중인 캡쳐로부터 캡쳐한 패킷을 읽기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="483"/>
        <location filename="rtp_player_dialog.ui" line="486"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="92"/>
        <source>SR (Hz)</source>
        <translation>SR (Hz)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="95"/>
        <source>Sample rate of codec</source>
        <translation>코덱 샘플레이트</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="100"/>
        <source>PR (Hz)</source>
        <translation>PR (Hz)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="103"/>
        <source>Play rate of decoded audio (depends e. g. on selected sound card)</source>
        <translation>디코딩한 오디오의 재생 샘플레이트 (선택한 사운드 카드에 의존)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="494"/>
        <location filename="rtp_player_dialog.ui" line="497"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="505"/>
        <location filename="rtp_player_dialog.ui" line="508"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="516"/>
        <location filename="rtp_player_dialog.ui" line="519"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="527"/>
        <location filename="rtp_player_dialog.ui" line="530"/>
        <source>Move Left 1 Pixels</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="538"/>
        <location filename="rtp_player_dialog.ui" line="541"/>
        <source>Move Right 1 Pixels</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="549"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="552"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치의 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="633"/>
        <source>Play the stream</source>
        <translation>스트림 재생</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="638"/>
        <source>To Left</source>
        <translation>왼쪽으로</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="646"/>
        <source>Left + Right</source>
        <translation>왼쪽 + 오른쪽</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="654"/>
        <source>To Right</source>
        <translation>오른쪽으로</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="619"/>
        <source>Invert Muting</source>
        <translation>음소거 반전</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="309"/>
        <source>No devices available</source>
        <translation>사용 가능한 장치가 없습니다</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="673"/>
        <source>Select</source>
        <translation>선택</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="582"/>
        <source>Audio Routing</source>
        <translation>오디오 라우팅</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="362"/>
        <source>&amp;Play Streams</source>
        <translation>스트림 재생(&amp;P)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="363"/>
        <source>Open RTP player dialog</source>
        <translation>RTP 재생기 대화상자 열기</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="373"/>
        <source>&amp;Set playlist</source>
        <translation>재생목록 설정(&amp;S)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="374"/>
        <source>Replace existing playlist in RTP Player with new one</source>
        <translation>RTP 재생기에 있는 재생목록을 새로운 것으로 교체</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="376"/>
        <source>&amp;Add to playlist</source>
        <translation>재생목록에 추가하기(&amp;A)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="377"/>
        <source>Add new set to existing playlist in RTP Player</source>
        <translation>RTP 재생기에서 존재하는 재생목록에 새 셋트를 추가</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="379"/>
        <source>&amp;Remove from playlist</source>
        <translation>재생목록으로부터 제거(&amp;R)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="380"/>
        <source>Remove selected streams from playlist in RTP Player</source>
        <translation>RTP 재생기 내의 재생목록으로부터 선택한 스트림 제거</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="385"/>
        <source>No Audio</source>
        <translation>오디오 없음</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="437"/>
        <location filename="rtp_player_dialog.cpp" line="481"/>
        <source>Decoding streams...</source>
        <translation>스트림 디코딩 중...</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="612"/>
        <source>Out of Sequence</source>
        <translation>순서 외</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="628"/>
        <source>Jitter Drops</source>
        <translation>지터 누락</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="644"/>
        <source>Wrong Timestamps</source>
        <translation>잘못된 타임스탬프</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="660"/>
        <source>Inserted Silence</source>
        <translation>무음 추가</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="759"/>
        <source>Double click on cell to change audio routing</source>
        <translation>오디오 라우팅을 변경하기 위한 셀을 더블 클릭</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1211"/>
        <source>%1 streams</source>
        <translation>%1 스트림</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1215"/>
        <source>, %1 selected</source>
        <translation>, %1 선택됨</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1226"/>
        <source>, %1 not muted</source>
        <translation>, %1 음소거 해제</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1230"/>
        <source>, start: %1. Double click on graph to set start of playback.</source>
        <translation>, 시작: %1. 재생하기 위한 지점을 설정하기 위해 그래프 상에서 더블클릭 하시오.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1233"/>
        <source>, start: %1, cursor: %2. Press &quot;G&quot; to go to packet %3. Double click on graph to set start of playback.</source>
        <translation>, 시작: %1, 커서: %2, &quot;G&quot; 를 눌러서 패킷 %3로 이동하십시오. 재생의 시작을 설정하기 위해 그래프 상에 더블클릭 하시오.</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1285"/>
        <source>Playback of stream %1 failed!</source>
        <translation>스트림 %1 재생이 실패하였습니다!</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1958"/>
        <source>Automatic</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2443"/>
        <source>WAV (*.wav)</source>
        <translation>WAV (*.wav)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2444"/>
        <source>Sun Audio (*.au)</source>
        <translation>Sun 오디오 (*.au)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2451"/>
        <source>Save audio</source>
        <translation>오디오 저장</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2469"/>
        <source>Raw (*.raw)</source>
        <translation>Raw (*.raw)</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2474"/>
        <source>Save payload</source>
        <translation>페이로드 저장</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2536"/>
        <location filename="rtp_player_dialog.cpp" line="2600"/>
        <location filename="rtp_player_dialog.cpp" line="2608"/>
        <location filename="rtp_player_dialog.cpp" line="2618"/>
        <location filename="rtp_player_dialog.cpp" line="2622"/>
        <location filename="rtp_player_dialog.cpp" line="2632"/>
        <location filename="rtp_player_dialog.cpp" line="2636"/>
        <location filename="rtp_player_dialog.cpp" line="2660"/>
        <location filename="rtp_player_dialog.cpp" line="2671"/>
        <location filename="rtp_player_dialog.cpp" line="2673"/>
        <source>Warning</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2536"/>
        <source>No stream selected or none of selected streams provide audio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2544"/>
        <location filename="rtp_player_dialog.cpp" line="2613"/>
        <location filename="rtp_player_dialog.cpp" line="2627"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2544"/>
        <source>All selected streams must use same play rate. Manual set of Output Audio Rate might help.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2600"/>
        <source>No streams are suitable for save</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2608"/>
        <location filename="rtp_player_dialog.cpp" line="2618"/>
        <location filename="rtp_player_dialog.cpp" line="2622"/>
        <location filename="rtp_player_dialog.cpp" line="2632"/>
        <location filename="rtp_player_dialog.cpp" line="2636"/>
        <location filename="rtp_player_dialog.cpp" line="2671"/>
        <location filename="rtp_player_dialog.cpp" line="2673"/>
        <source>Save failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2613"/>
        <source>Can&apos;t write header of AU file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2627"/>
        <source>Can&apos;t write header of WAV file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="2660"/>
        <source>Payload save works with just one audio stream.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="rtp_player_dialog.ui" line="47"/>
        <source>Double click to change audio routing</source>
        <translation>오디오 라우팅을 변경하려면 두 번 클릭하십시오</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1382"/>
        <source>Preparing to play...</source>
        <translation>재생을 준비 중...</translation>
    </message>
    <message>
        <location filename="rtp_player_dialog.cpp" line="1927"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
</context>
<context>
    <name>RtpStreamDialog</name>
    <message>
        <location filename="rtp_stream_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="45"/>
        <source>Source Address</source>
        <translation>발신지 주소</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="50"/>
        <source>Source Port</source>
        <translation>발신지 포트</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="55"/>
        <source>Destination Address</source>
        <translation>목적지 주소</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="60"/>
        <source>Destination Port</source>
        <translation>목적지 포트</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="65"/>
        <source>SSRC</source>
        <translation>SSRC</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="70"/>
        <source>Start Time</source>
        <translation>시작 시간</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="75"/>
        <source>Duration</source>
        <translation>지속시간</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="80"/>
        <source>Payload</source>
        <translation>페이로드</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="85"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="90"/>
        <source>Lost</source>
        <translation>누락</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="105"/>
        <source>Max Delta (ms)</source>
        <translation>최대 간격 (ms)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="120"/>
        <source>Max Jitter</source>
        <translation>최대 지터</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="115"/>
        <source>Mean Jitter</source>
        <translation>평균 지터</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="125"/>
        <source>Status</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="133"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only show conversations matching the current display filter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;현재 표시 필터와 일치하는 대화만 보이기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="148"/>
        <source>Limit to display filter</source>
        <translation>표시 필터에 대한 제한</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="155"/>
        <source>Time of Day</source>
        <translation>주간시간표기</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="187"/>
        <location filename="rtp_stream_dialog.ui" line="195"/>
        <location filename="rtp_stream_dialog.ui" line="228"/>
        <source>Find &amp;Reverse</source>
        <translation>역방향 찾기(&amp;R)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="305"/>
        <source>Prepare &amp;Filter</source>
        <translation>필터 준비(&amp;F)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="316"/>
        <source>&amp;Export</source>
        <translation>내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="327"/>
        <source>&amp;Analyze</source>
        <translation>분석(&amp;A)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="330"/>
        <source>Open the analysis window for the selected stream(s) and add it to it</source>
        <translation>선택한 스트림에 대한 분석창을 열고 스트림을 추가합니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="198"/>
        <source>Find the reverse stream matching the selected forward stream.</source>
        <translation>선택한 정방향 스트림에 부합하는 역방향 스트림을 찾습니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="95"/>
        <source>Min Delta (ms)</source>
        <translation>최소 델타 (ms)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="100"/>
        <source>Mean Delta (ms)</source>
        <translation>평균 델타 (ms)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="110"/>
        <source>Min Jitter</source>
        <translation>최소 지터</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="190"/>
        <source>All forward/reverse stream actions</source>
        <translation>모든 정방향/역방향 스트림 동작</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="201"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="206"/>
        <source>Find All &amp;Pairs</source>
        <translation>모든 페어 찾기(&amp;P)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="209"/>
        <source>Select all streams which are paired in forward/reverse relation</source>
        <translation>정방향/역방향 관계와 연결된 모든 스트림 선택하기</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="212"/>
        <source>Shift+R</source>
        <translation>Shift+R</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="217"/>
        <source>Find Only &amp;Singles</source>
        <translation>싱글만 찾기(&amp;S)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="220"/>
        <source>Find all streams which don&apos;t have paired reverse stream</source>
        <translation>역방향 스트림과 연결되지 않은 모든 스트림을 찾기</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="223"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="239"/>
        <source>Mark Packets</source>
        <translation>마크 패킷</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="242"/>
        <source>Mark the packets of the selected stream(s).</source>
        <translation>선택한 스트림의 패킷을 마크합니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="245"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="261"/>
        <source>All</source>
        <translation>모두</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="264"/>
        <source>Select all</source>
        <translation>모두 선택</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="272"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="275"/>
        <source>Clear selection</source>
        <translation>선택 해제</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="283"/>
        <source>Invert</source>
        <translation>반전</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="286"/>
        <source>Invert selection</source>
        <translation>선택 반전</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="294"/>
        <source>Go To Setup</source>
        <translation>설정으로 이동</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="297"/>
        <source>Go to the setup packet for this stream.</source>
        <translation>이 스트림에 대한 설정 패킷으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="300"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="308"/>
        <source>Prepare a filter matching the selected stream(s).</source>
        <translation>선택한 스트림과 일치하는 필터를 준비하십시오.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="311"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="319"/>
        <source>Export the stream payload as rtpdump</source>
        <translation>rtpdump로 스트림 페이로드를 내보내기</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="322"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="333"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="338"/>
        <source>Cop&amp;y</source>
        <translation>복사(&amp;y)</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="341"/>
        <source>Open copy menu</source>
        <translation>복사 메뉴 열기</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="346"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="349"/>
        <source>Copy stream list as CSV.</source>
        <translation>CSV로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="354"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="357"/>
        <source>Copy stream list as YAML.</source>
        <translation>YAML로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="294"/>
        <source>RTP Streams</source>
        <translation>RTP 스트림</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.ui" line="250"/>
        <source>Select</source>
        <translation>선택</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="337"/>
        <source>as CSV</source>
        <translation>CSV로</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="340"/>
        <source>as YAML</source>
        <translation>YAML로</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="591"/>
        <source>%1 streams</source>
        <translation>%1 스트림</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="601"/>
        <source>, %1 selected, %2 total packets</source>
        <translation>, %1 선택, %2 전체 패킷</translation>
    </message>
    <message>
        <location filename="rtp_stream_dialog.cpp" line="753"/>
        <source>Save RTPDump As…</source>
        <translation>다른 이름으로 RTP덤프 저장하기…</translation>
    </message>
</context>
<context>
    <name>SCTPAllAssocsDialog</name>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="14"/>
        <source>Wireshark - SCTP Associations</source>
        <translation>Wireshark - SCTP 연결</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="59"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="64"/>
        <source>Port 1</source>
        <translation>포트 1</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="69"/>
        <source>Port 2</source>
        <translation>포트 2</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="74"/>
        <source>Number of Packets</source>
        <translation>패킷 수</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="79"/>
        <source>Number of DATA Chunks</source>
        <translation>데이터 덩어리 수</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="84"/>
        <source>Number of Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="103"/>
        <source>Filter Selected Association</source>
        <translation>선택한 연결을 필터</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="116"/>
        <source>Analyze</source>
        <translation>분석</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="148"/>
        <source>Reset Graph</source>
        <translation>그래프를 다시 설정</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="151"/>
        <source>Reset the graph to its initial state.</source>
        <translation>그래프를 초기 상태로 다시 설정합니다.</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="154"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="159"/>
        <location filename="sctp_all_assocs_dialog.ui" line="162"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="165"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="170"/>
        <location filename="sctp_all_assocs_dialog.ui" line="173"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="176"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="181"/>
        <location filename="sctp_all_assocs_dialog.ui" line="184"/>
        <source>Move Up 10 Pixels</source>
        <translation>위쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="187"/>
        <source>Up</source>
        <translation>위쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="192"/>
        <location filename="sctp_all_assocs_dialog.ui" line="195"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="198"/>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="203"/>
        <location filename="sctp_all_assocs_dialog.ui" line="206"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="209"/>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="214"/>
        <location filename="sctp_all_assocs_dialog.ui" line="217"/>
        <source>Move Down 10 Pixels</source>
        <translation>아래쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="220"/>
        <source>Down</source>
        <translation>아래쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="225"/>
        <location filename="sctp_all_assocs_dialog.ui" line="228"/>
        <source>Move Up 1 Pixel</source>
        <translation>위쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="231"/>
        <source>Shift+Up</source>
        <translation>Shift+위쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="236"/>
        <location filename="sctp_all_assocs_dialog.ui" line="239"/>
        <source>Move Left 1 Pixel</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="242"/>
        <source>Shift+Left</source>
        <translation>Shift+왼쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="247"/>
        <location filename="sctp_all_assocs_dialog.ui" line="250"/>
        <source>Move Right 1 Pixel</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="253"/>
        <source>Shift+Right</source>
        <translation>Shift+오른쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="258"/>
        <location filename="sctp_all_assocs_dialog.ui" line="261"/>
        <source>Move Down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="264"/>
        <source>Shift+Down</source>
        <translation>Shift+아래쪽</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="269"/>
        <source>Next Stream</source>
        <translation>다음 스트림</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="272"/>
        <source>Go to the next stream in the capture</source>
        <translation>캡쳐에서 다음 스트림으로 이동합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="275"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="280"/>
        <source>Previous Stream</source>
        <translation>이전 스트림</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="283"/>
        <source>Go to the previous stream in the capture</source>
        <translation>캡쳐의 이전 스트림으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="286"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="291"/>
        <source>Switch Direction</source>
        <translation>방향 전환</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="294"/>
        <source>Switch direction (swap TCP endpoints)</source>
        <translation>방향 전환 (TCP 종단점 바꾸기)</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="297"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="302"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="305"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치의 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="308"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="313"/>
        <source>Drag / Zoom</source>
        <translation>드래그 / 확대</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="316"/>
        <source>Toggle mouse drag / zoom behavior</source>
        <translation>마우스 드래그 / 확대 동작을 전환합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="319"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="324"/>
        <source>Relative / Absolute Sequence Numbers</source>
        <translation>상대적/절대적 순서 번호를 바꾸기</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="327"/>
        <source>Toggle relative / absolute sequence numbers</source>
        <translation>상대적/절대적 순서 번호를 바꿉니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="330"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="335"/>
        <source>Capture / Session Time Origin</source>
        <translation>캡쳐 / 세션 시간 기점</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="338"/>
        <source>Toggle capture / session time origin</source>
        <translation>캡쳐 / 세션 시간 기점 전환</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="341"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="346"/>
        <source>Crosshairs</source>
        <translation>십자선</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="349"/>
        <source>Toggle crosshairs</source>
        <translation>십자선의 표시를 전환합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="352"/>
        <source>Space</source>
        <translation>스페이스</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="357"/>
        <source>Round Trip Time</source>
        <translation>왕복 지연 시간</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="360"/>
        <source>Switch to the Round Trip Time graph</source>
        <translation>왕복 지연 시간 그래프로 바꿉니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="363"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="368"/>
        <source>Throughput</source>
        <translation>처리량</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="371"/>
        <source>Switch to the Throughput graph</source>
        <translation>처리량 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="374"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="379"/>
        <source>Time / Sequence (Stevens)</source>
        <translation>시간 / 순서 (스티븐스)</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="382"/>
        <source>Switch to the Stevens-style Time / Sequence graph</source>
        <translation>스티븐스 스타일 시간/순서 그래프로 바꿉니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="385"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="390"/>
        <source>Window Scaling</source>
        <translation>창 확대</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="393"/>
        <source>Switch to the Window Scaling graph</source>
        <translation>창 확대 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="396"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="401"/>
        <source>Time / Sequence (tcptrace)</source>
        <translation>시간 / 순서 (tcptrace)</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="404"/>
        <source>Switch to the tcptrace-style Time / Sequence graph</source>
        <translation>tcptrace 스타일 시간/순서 그래프로 전환</translation>
    </message>
    <message>
        <location filename="sctp_all_assocs_dialog.ui" line="407"/>
        <source>4</source>
        <translation>4</translation>
    </message>
</context>
<context>
    <name>SCTPAssocAnalyseDialog</name>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="20"/>
        <source>Wireshark - Analyse Association</source>
        <translation>Wireshark - 분석 연결</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="35"/>
        <source>TabWidget</source>
        <translation>탭위젯</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="45"/>
        <source>Statistics</source>
        <translation>통계</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="155"/>
        <source>Chunk Statistics</source>
        <translation>덩어리 통계</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="145"/>
        <source>Filter Association</source>
        <translation>필터 연결</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="165"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="338"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="511"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="109"/>
        <source>Number of Data Chunks from EP2 to EP1: </source>
        <translation>EP2에서 EP1에 데이터 덩어리 수: </translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="81"/>
        <source>Checksum Type:</source>
        <translation>체크섬 유형:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="123"/>
        <source>Number of Data Chunks from EP1 to EP2: </source>
        <translation>EP1에서 EP2에 데이터 덩어리 수: </translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="60"/>
        <source>Number of Data Bytes from EP1 to EP2:</source>
        <translation>EP1에서 EP2에 데이터 바이트:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="74"/>
        <source>Number of Data Bytes from EP2 to EP1: </source>
        <translation>EP2에서 EP1에 데이터 바이트: </translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="67"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="88"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="95"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="102"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="116"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="202"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="209"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="216"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="251"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="265"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="272"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="369"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="376"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="411"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="418"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="425"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="439"/>
        <source>TextLabel</source>
        <translation>텍스트레이블</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="174"/>
        <source>Endpoint 1</source>
        <translation>종단점 1</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="321"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="494"/>
        <source>Graph TSN</source>
        <translation>TSN 그래프</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="311"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="484"/>
        <source>Graph Bytes</source>
        <translation>바이트 그래프</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="279"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="362"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="147"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="198"/>
        <source>Requested Number of Inbound Streams:</source>
        <translation>요청된 인바운드 스트림의 수:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="258"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="397"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="244"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="383"/>
        <source>Sent Verification Tag:</source>
        <translation>발신 검증 태그:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="237"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="404"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="149"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="200"/>
        <source>Minimum Number of Inbound Streams:</source>
        <translation>최소 인바운드 스트림 수:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="186"/>
        <source>Complete List of IP addresses from INIT Chunk:</source>
        <translation>INIT 덩어리로부터 완성 IP 주소 목록:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="223"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="390"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="154"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="205"/>
        <source>Minimum Number of Outbound Streams:</source>
        <translation>최소 아웃바운드 스트림 수:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="328"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="501"/>
        <source>Graph Arwnd</source>
        <translation>Arwnd 그래프</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="347"/>
        <source>Endpoint 2</source>
        <translation>종단점 2</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="455"/>
        <source>Complete List of IP addresses from INIT_ACK Chunk:</source>
        <translation>INIT_ACK 덩어리로부터 IP 주소 목록 완성:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.ui" line="230"/>
        <location filename="sctp_assoc_analyse_dialog.ui" line="432"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="152"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="203"/>
        <source>Provided Number of Outbound Streams:</source>
        <translation>제공된 아웃바운드 스트림 수:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="39"/>
        <source>SCTP Analyse Association: %1 Port1 %2 Port2 %3</source>
        <translation>SCTP 분석 연결: %1 포트 1 %2 포트 2 %3</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="86"/>
        <source>No Association found for this packet.</source>
        <translation>이 패킷에 대한 연결을 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="97"/>
        <source>Warning</source>
        <translation>경고</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="97"/>
        <source>Could not find SCTP Association with id: %1</source>
        <translation>ID %1 와 연결된 SCTP 연결을 찾을 수 없음</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="117"/>
        <source>Complete list of IP addresses from INIT Chunk:</source>
        <translation>INIT 덩어리로부터 IP 주소의 목록을 완성:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="119"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="171"/>
        <source>Complete list of IP addresses from INIT_ACK Chunk:</source>
        <translation>INIT_ACK 덩어리로부터 IP 주소의 목록 완성:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="121"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="173"/>
        <source>List of Used IP Addresses</source>
        <translation>사용된 IP 주소의 목록</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="158"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="209"/>
        <source>Used Number of Inbound Streams:</source>
        <translation>사용된 인바운드 스트림의 수:</translation>
    </message>
    <message>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="160"/>
        <location filename="sctp_assoc_analyse_dialog.cpp" line="211"/>
        <source>Used Number of Outbound Streams:</source>
        <translation>사용된 아웃바운드 스트림의 수:</translation>
    </message>
</context>
<context>
    <name>SCTPChunkStatisticsDialog</name>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="70"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="315"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="325"/>
        <source>Association</source>
        <translation>연결</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="75"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="316"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="326"/>
        <source>Endpoint 1</source>
        <translation>종료 1</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="80"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="317"/>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="327"/>
        <source>Endpoint 2</source>
        <translation>종단점 2</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="94"/>
        <source>Save Chunk Type Order</source>
        <translation>덩어리 유형 순서를 저장</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="99"/>
        <source>Hide Chunk Type</source>
        <translation>덩어리 유형을 숨기기</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="102"/>
        <source>Remove the chunk type from the table</source>
        <translation>표에서 덩어리 유형을 삭제</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="107"/>
        <source>Chunk Type Preferences</source>
        <translation>덩어리 유형 설정</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="110"/>
        <source>Go to the chunk type preferences dialog to show or hide other chunk types</source>
        <translation>다른 덩어리 유형을 표시하거나 숨기기 위해 덩어리 유형 설정 대화상자로 이동</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="115"/>
        <source>Show All Registered Chunk Types</source>
        <translation>등록된 모든 덩어리 유형을 표시</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.ui" line="118"/>
        <source>Show all chunk types with defined names</source>
        <translation>정의된 이름을 갖는 모든 덩어리 유형을 표시</translation>
    </message>
    <message>
        <location filename="sctp_chunk_statistics_dialog.cpp" line="45"/>
        <source>SCTP Chunk Statistics: %1 Port1 %2 Port2 %3</source>
        <translation>SCTP 덩어리 통계: %1 포트1 %2 포트2 %3</translation>
    </message>
</context>
<context>
    <name>SCTPGraphArwndDialog</name>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="14"/>
        <source>SCTP Graph</source>
        <translation>SCTP 그래프</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="47"/>
        <source>Reset to full size</source>
        <translation>최대 크기로 다시 설정</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="32"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="54"/>
        <source>Save Graph</source>
        <translation>그래프 저장</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="91"/>
        <source>goToPacket</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.ui" line="94"/>
        <source>Go to Packet</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="43"/>
        <source>SCTP Data and Adv. Rec. Window over Time: %1 Port1 %2 Port2 %3</source>
        <translation>SCTP 데이터와 알려진 수신 윈도우 over 시간: %1 포트1 %2 포트2 %3</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="47"/>
        <source>No Data Chunks sent</source>
        <translation>발신된 데이터 덩어리 없음</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="110"/>
        <source>Arwnd</source>
        <translation>Arwnd</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="118"/>
        <source>time [secs]</source>
        <translation>시간 [초]</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="119"/>
        <source>Advertised Receiver Window [Bytes]</source>
        <translation>알려진 수신지 창 [바이트]</translation>
    </message>
    <message>
        <location filename="sctp_graph_arwnd_dialog.cpp" line="167"/>
        <source>&lt;small&gt;&lt;i&gt;Graph %1: a_rwnd=%2 Time=%3 secs &lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;그래프 %1: a_rwnd=%2 시간=%3 초 &lt;/i&gt;&lt;/small&gt;</translation>
    </message>
</context>
<context>
    <name>SCTPGraphByteDialog</name>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="14"/>
        <source>SCTP Graph</source>
        <translation>SCTP 그래프</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="47"/>
        <source>Reset to full size</source>
        <translation>최대 크기로 다시 설정</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="32"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="54"/>
        <source>Save Graph</source>
        <translation>그래프 저장</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="91"/>
        <source>goToPacket</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.ui" line="94"/>
        <source>Go to Packet</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="42"/>
        <source>SCTP Data and Adv. Rec. Window over Time: %1 Port1 %2 Port2 %3</source>
        <translation>시간당 SCTP 데이터와 보급된 수신지 창: %1 포트1 %2 포트2 %3</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="46"/>
        <source>No Data Chunks sent</source>
        <translation>송신된 데이터 덩어리 없음</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="110"/>
        <location filename="sctp_graph_byte_dialog.cpp" line="157"/>
        <source>Bytes</source>
        <translation>바이트</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="117"/>
        <source>time [secs]</source>
        <translation>시간 [초]</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="118"/>
        <source>Received Bytes</source>
        <translation>수신 바이트</translation>
    </message>
    <message>
        <location filename="sctp_graph_byte_dialog.cpp" line="170"/>
        <source>&lt;small&gt;&lt;i&gt;Graph %1: Received bytes=%2 Time=%3 secs &lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;그래프 %1: 수신 바이트=%2 시간=%3 초 &lt;/i&gt;&lt;/small&gt;</translation>
    </message>
</context>
<context>
    <name>SCTPGraphDialog</name>
    <message>
        <location filename="sctp_graph_dialog.ui" line="14"/>
        <source>SCTP Graph</source>
        <translation>SCTP 그래프</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="44"/>
        <source>Relative TSNs</source>
        <translation>상대적 TSN</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="58"/>
        <source>Only SACKs</source>
        <translation>SACK만</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="68"/>
        <source>Only TSNs</source>
        <translation>TSN만</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="78"/>
        <source>Show both</source>
        <translation>모두 표시</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="88"/>
        <source>Reset to full size</source>
        <translation>최대 크기로 다시 설정</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="95"/>
        <source>Save Graph</source>
        <translation>그래프 저장</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="132"/>
        <source>goToPacket</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.ui" line="135"/>
        <source>Go to Packet</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="49"/>
        <source>SCTP TSNs and SACKs over Time: %1 Port1 %2 Port2 %3</source>
        <translation>SCTP TSN 수와 SACK 수 over 시간: %1 포트1 %2 포트2 %3</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="53"/>
        <source>No Data Chunks sent</source>
        <translation>발신된 데이터 덩어리 없음</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="218"/>
        <source>CumTSNAck</source>
        <translation>CumTSNAck</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="231"/>
        <source>Gap Ack</source>
        <translation>Gap Ack</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="244"/>
        <source>NR Gap Ack</source>
        <translation>NR Gap Ack</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="257"/>
        <source>Duplicate Ack</source>
        <translation>중복 ACK</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="313"/>
        <source>TSN</source>
        <translation>TSN</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="371"/>
        <source>time [secs]</source>
        <translation>시간 [초]</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="372"/>
        <source>TSNs</source>
        <translation>TSNs</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="472"/>
        <source>&lt;small&gt;&lt;i&gt;%1: %2 Time: %3 secs &lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;%1: %2 시간: %3 초수 &lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="482"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="483"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="484"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="486"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="sctp_graph_dialog.cpp" line="493"/>
        <source>Save Graph As…</source>
        <translation>다른이름으로 그래프 저장하기…</translation>
    </message>
</context>
<context>
    <name>ScsiServiceResponseTimeDialog</name>
    <message>
        <location filename="scsi_service_response_time_dialog.cpp" line="29"/>
        <source>&lt;small&gt;&lt;i&gt;Select a command and enter a filter if desired, then press Apply.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;명령을 선택하고 원하는 경우 필터를 입력한 다음 적용을 누르십시오.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="scsi_service_response_time_dialog.cpp" line="36"/>
        <source>Command:</source>
        <translation>주석:</translation>
    </message>
    <message>
        <location filename="scsi_service_response_time_dialog.cpp" line="38"/>
        <source>SCSI Service Response Times</source>
        <translation>SCSI 서비스 응답 시간</translation>
    </message>
</context>
<context>
    <name>SearchFrame</name>
    <message>
        <location filename="search_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="48"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search the Info column of the packet list (summary pane), decoded packet display labels (tree view pane) or the ASCII-converted packet data (hex view pane).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;패킷 목록 (요약 부분)과 디코딩된 패킷 표시 딱지 (트리 표시 부분)와 아스키 변환된 패킷 데이터 (16진수 표시 부분)을 검색합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="52"/>
        <source>Packet list</source>
        <translation>패킷 목록</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="57"/>
        <source>Packet details</source>
        <translation>패킷 상세 정보</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="62"/>
        <source>Packet bytes</source>
        <translation>패킷 바이트 열</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="83"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for strings containing narrow (UTF-8 and ASCII) or wide (UTF-16) characters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;좁은 문자열 (UTF-8과 ASCII) 또는 넓은 (UTF-16) 문자를 검색합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="87"/>
        <source>Narrow &amp; Wide</source>
        <translation>좁음 및 넓음</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="92"/>
        <source>Narrow (UTF-8 / ASCII)</source>
        <translation>좁음 (UTF-8/ASCII)</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="97"/>
        <source>Wide (UTF-16)</source>
        <translation>넓음 (UTF-16)</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="105"/>
        <source>Case sensitive</source>
        <translation>대소문자 구분</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="125"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for data using display filter syntax (e.g. ip.addr==10.1.1.1), a hexadecimal string (e.g. fffffda5), a plain string (e.g. My String) or a regular expression (e.g. colou?r).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for data using display filter syntax (e.g. ip.addr==10.1.1.1), a hexadecimal string (e.g. fffffda5) or a plain string (e.g. My String).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;표시 필터 문법 (예: ip.addr==10.1.1.1) 16진수 문자열 (예: ffffda5) 평문 문자열 (예: My String) 또는 정규 표현식 (예: colou? r)을 사용하여 데이터를 검색합니다..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="129"/>
        <source>Display filter</source>
        <translation>표시 필터</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="134"/>
        <source>Hex value</source>
        <translation>16진수 값</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="139"/>
        <source>String</source>
        <translation>문자열</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="144"/>
        <source>Regular Expression</source>
        <translation>정규표현식</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="168"/>
        <source>Find</source>
        <translation>찾기</translation>
    </message>
    <message>
        <location filename="search_frame.ui" line="184"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="431"/>
        <source>No valid search type selected. Please report this to the development team.</source>
        <translation>올바른 검색 유형이 선택되지 않았습니다. 이것을 개발팀에 보고하십시오.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="388"/>
        <source>Invalid filter.</source>
        <translation>유효하지 않은 필터입니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="393"/>
        <source>That filter doesn&apos;t test anything.</source>
        <translation>그 필터는 아무것도 시험하지 않습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="400"/>
        <source>That&apos;s not a valid hex string.</source>
        <translation>유효하지 않은 16진수 문자열이 아닙니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="408"/>
        <source>You didn&apos;t specify any text for which to search.</source>
        <translation>검색할 텍스트를 지정하지 않았습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="425"/>
        <source>No valid character set selected. Please report this to the development team.</source>
        <translation>올바른 문자 집합이 선택되지 않았습니다. 이것을 개발팀에 보고하십시오.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="446"/>
        <source>No valid search area selected. Please report this to the development team.</source>
        <translation>올바른 검색 영역이 선택되지 않았습니다. 이것을 개발팀에 보고하십시오.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="453"/>
        <source>Searching for %1…</source>
        <translation>%1 검색중…</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="461"/>
        <source>No packet contained those bytes.</source>
        <translation>그런 바이트 열을 포함한 패킷이 없습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="474"/>
        <source>No packet contained that string in its Info column.</source>
        <translation>정보 열의 문자열을 포함한 패킷이 없습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="482"/>
        <source>No packet contained that string in its dissected display.</source>
        <translation>분석된 표시에 문자열을 포함한 패킷이 없습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="490"/>
        <source>No packet contained that string in its converted data.</source>
        <translation>변환된 데이터에 문자열을 포함한 패킷이 없습니다.</translation>
    </message>
    <message>
        <location filename="search_frame.cpp" line="499"/>
        <source>No packet matched that filter.</source>
        <translation>필터에 일치하는 패킷이 없습니다.</translation>
    </message>
</context>
<context>
    <name>SequenceDialog</name>
    <message>
        <location filename="sequence_dialog.cpp" line="84"/>
        <source>Call Flow</source>
        <translation>전화 흐름</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="117"/>
        <source>Time</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="124"/>
        <source>Comment</source>
        <translation>주석</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="399"/>
        <source>No data</source>
        <translation>데이터 없음</translation>
    </message>
    <message numerus="yes">
        <location filename="sequence_dialog.cpp" line="401"/>
        <source>%Ln node(s)</source>
        <translation><numerusform>%Ln 노드</numerusform></translation>
    </message>
    <message numerus="yes">
        <location filename="sequence_dialog.cpp" line="402"/>
        <source>%Ln item(s)</source>
        <translation><numerusform>%Ln 항목</numerusform></translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="439"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="440"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="441"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="443"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="444"/>
        <source>ASCII (*.txt)</source>
        <translation>ASCII 형식 (*.txt)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="455"/>
        <source>Save Graph As…</source>
        <translation>다른이름으로 그래프를 저장하기…</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="84"/>
        <source>Flow</source>
        <translation>흐름</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="48"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;n&lt;/th&gt;&lt;td&gt;Go to the next packet&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;p&lt;/th&gt;&lt;td&gt;Go to the previous packet&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;n&lt;/th&gt;&lt;td&gt;Go to the next packet&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;p&lt;/th&gt;&lt;td&gt;Go to the previous packet&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;원자 값의 있고 굉장한 시간을 절약하는 키보드 단축키&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;초기 상태 그래프를 다시 설정&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;커서 아래쪽의 패킷으로 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;n&lt;/th&gt;&lt;td&gt;다음 패킷으로 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;p&lt;/th&gt;&lt;td&gt;이전 패킷으로 이동&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="74"/>
        <source>&lt;small&gt;&lt;i&gt;A hint&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="107"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only show flows matching the current display filter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;현재 디스플레이 필터와 일치하는 흐름만 표시&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="110"/>
        <source>Limit to display filter</source>
        <translation>표시 필터에 제한</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="130"/>
        <source>Flow type:</source>
        <translation>흐름 유형:</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="153"/>
        <source>Addresses:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="161"/>
        <source>Any</source>
        <translation>모든</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="166"/>
        <source>Network</source>
        <translation>네트워크</translation>
    </message>
    <message>
        <location filename="sequence_dialog.cpp" line="139"/>
        <source>Reset Diagram</source>
        <translation>다이어그램을 다시 설정</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="189"/>
        <source>Reset &amp;Diagram</source>
        <translation>다이어그램 초기화(&amp;D)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="192"/>
        <location filename="sequence_dialog.cpp" line="140"/>
        <source>Reset the diagram to its initial state.</source>
        <translation>다이어그램을 초기 상태로 다시 설정합니다.</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="195"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="200"/>
        <source>&amp;Reset Diagram</source>
        <translation>다이어그램 리셋(&amp;R)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="203"/>
        <source>Reset the diagram to its initial state</source>
        <translation>초기 상태로 다이어그램 초기화</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="208"/>
        <source>&amp;Export</source>
        <translation>내보내기(&amp;E)</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="211"/>
        <source>Export diagram</source>
        <translation>다이어그램 내보내기</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="216"/>
        <location filename="sequence_dialog.ui" line="219"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="222"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="227"/>
        <location filename="sequence_dialog.ui" line="230"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="233"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="238"/>
        <location filename="sequence_dialog.ui" line="241"/>
        <source>Move Up 10 Pixels</source>
        <translation>위쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="244"/>
        <source>Up</source>
        <translation>위</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="249"/>
        <location filename="sequence_dialog.ui" line="252"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="255"/>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="260"/>
        <location filename="sequence_dialog.ui" line="263"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="266"/>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="271"/>
        <location filename="sequence_dialog.ui" line="274"/>
        <source>Move Down 10 Pixels</source>
        <translation>아래쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="277"/>
        <source>Down</source>
        <translation>아래쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="282"/>
        <location filename="sequence_dialog.ui" line="285"/>
        <source>Move Up 1 Pixel</source>
        <translation>위쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="288"/>
        <source>Shift+Up</source>
        <translation>Shift+위쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="293"/>
        <location filename="sequence_dialog.ui" line="296"/>
        <source>Move Left 1 Pixel</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="299"/>
        <source>Shift+Left</source>
        <translation>Shift+왼쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="304"/>
        <location filename="sequence_dialog.ui" line="307"/>
        <source>Move Right 1 Pixel</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="310"/>
        <source>Shift+Right</source>
        <translation>Shift+오른쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="315"/>
        <location filename="sequence_dialog.ui" line="318"/>
        <source>Move Down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="321"/>
        <source>Shift+Down</source>
        <translation>Shift+아래쪽</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="326"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="329"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치에 있는 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="332"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="337"/>
        <source>All Flows</source>
        <translation>모든 흐름</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="340"/>
        <source>Show flows for all packets</source>
        <translation>모든 패킷의 흐름을 표시</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="343"/>
        <location filename="sequence_dialog.ui" line="354"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="348"/>
        <source>TCP Flows</source>
        <translation>TCP 흐름</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="351"/>
        <source>Show only TCP flow information</source>
        <translation>TCP 흐름 정보만 표시</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="359"/>
        <source>Go To Next Packet</source>
        <translation>다음 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="362"/>
        <source>Go to the next packet</source>
        <translation>다음 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="365"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="370"/>
        <source>Go To Previous Packet</source>
        <translation>이전 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="373"/>
        <source>Go to the previous packet</source>
        <translation>이전 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="376"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="381"/>
        <source>Select RTP Stream</source>
        <translation>RTP 스트림 선택</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="384"/>
        <source>Select RTP stream in RTP Streams dialog</source>
        <translation>RTP 스트림 대화상자에서 RTP 스트림 선택</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="387"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="392"/>
        <source>Deselect RTP Stream</source>
        <translation>RTP 스트림 선택해제</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="395"/>
        <source>Deselect RTP stream in RTP Streams dialog</source>
        <translation>RTP 스트림 대화상자에서 RTP 스트림 선택해제</translation>
    </message>
    <message>
        <location filename="sequence_dialog.ui" line="398"/>
        <source>D</source>
        <translation>D</translation>
    </message>
</context>
<context>
    <name>ShortcutListModel</name>
    <message>
        <location filename="about_dialog.cpp" line="175"/>
        <source>Shortcut</source>
        <translation>단축키</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="175"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="about_dialog.cpp" line="175"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
</context>
<context>
    <name>ShowPacketBytesDialog</name>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="20"/>
        <source>Show Packet Bytes</source>
        <translation>패킷 바이트 표기</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="36"/>
        <source>Hint.</source>
        <translation>필터힌트.</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="51"/>
        <source>Decode as</source>
        <translation>다른 인코딩으로 디코드</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="61"/>
        <source>Show as</source>
        <translation>다른 형식으로 표시</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="88"/>
        <source>Start</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="98"/>
        <source>End</source>
        <translation>종료</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="112"/>
        <location filename="show_packet_bytes_dialog.cpp" line="239"/>
        <source>Find:</source>
        <translation>찾기:</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.ui" line="122"/>
        <source>Find &amp;Next</source>
        <translation>다음 찾기(&amp;N)</translation>
    </message>
    <message numerus="yes">
        <location filename="show_packet_bytes_dialog.cpp" line="51"/>
        <source>Frame %1, %2, %Ln byte(s).</source>
        <translation><numerusform>프레임 %1, %2, %Ln 바이트.</numerusform></translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="61"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="62"/>
        <source>Base64</source>
        <translation>Base64</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="63"/>
        <source>Compressed</source>
        <translation>압축됨</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="64"/>
        <source>Hex Digits</source>
        <translation>16진</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="65"/>
        <source>Percent-Encoding</source>
        <translation>퍼센트인코딩</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="66"/>
        <source>Quoted-Printable</source>
        <translation>인용 인쇄 가능</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="67"/>
        <source>ROT13</source>
        <translation>ROT13 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="71"/>
        <source>ASCII</source>
        <translation>ASCII 문자 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="72"/>
        <source>ASCII &amp; Control</source>
        <translation>ASCII 문자와 제어 문자 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="73"/>
        <source>C Array</source>
        <translation>C 언어 배열 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="74"/>
        <source>EBCDIC</source>
        <translation>EBCDIC 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="75"/>
        <source>Hex Dump</source>
        <translation>16진수 덤프 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="76"/>
        <source>HTML</source>
        <translation>HTML 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="77"/>
        <source>Image</source>
        <translation>그림 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="79"/>
        <source>Raw</source>
        <translation>Raw(무가공) 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="80"/>
        <source>Rust Array</source>
        <translation>러스트 배열</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="82"/>
        <source>UTF-8</source>
        <translation>UTF-8 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="83"/>
        <source>YAML</source>
        <translation>YAML 형식</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="90"/>
        <source>Print</source>
        <translation>출력</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="93"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="96"/>
        <source>Save as…</source>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="315"/>
        <source>Save Selected Packet Bytes As…</source>
        <translation>선택한 패킷 바이트를 다른 이름으로 저장…</translation>
    </message>
    <message numerus="yes">
        <location filename="show_packet_bytes_dialog.cpp" line="182"/>
        <source>Displaying %Ln byte(s).</source>
        <translation><numerusform>%Ln 바이트를 표시하고 있습니다.</numerusform></translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="78"/>
        <source>JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="237"/>
        <source>Regex Find:</source>
        <translation>정규식 찾기:</translation>
    </message>
</context>
<context>
    <name>ShowPacketBytesTextEdit</name>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="905"/>
        <source>Show Selected</source>
        <translation>선택된 것을 표시</translation>
    </message>
    <message>
        <location filename="show_packet_bytes_dialog.cpp" line="909"/>
        <source>Show All</source>
        <translation>모두 표시</translation>
    </message>
</context>
<context>
    <name>SplashOverlay</name>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="126"/>
        <source>Initializing dissectors</source>
        <translation>분해기를 초기화 하는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="129"/>
        <source>Initializing tap listeners</source>
        <translation>tap 리스너를 초기화 하는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="132"/>
        <source>Initializing external capture plugins</source>
        <translation>외부 캡쳐 플러그인을 초기화하는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="135"/>
        <source>Registering dissectors</source>
        <translation>분해기를 등록 하는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="138"/>
        <source>Registering plugins</source>
        <oldsource>Registering dissector</oldsource>
        <translation>플러그인 등록 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="141"/>
        <source>Handing off dissectors</source>
        <translation>분해기를 제거하는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="144"/>
        <source>Handing off plugins</source>
        <translation>플러그인을 제거하고 있습 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="147"/>
        <source>Loading Lua plugins</source>
        <translation>Lua 플러그인 로딩 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="150"/>
        <source>Removing Lua plugins</source>
        <translation>Lua 플러그인을 삭제 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="153"/>
        <source>Loading module preferences</source>
        <translation>모듈 설정을 로딩 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="156"/>
        <source>Finding local interfaces</source>
        <translation>로컬 인터페이스를 찾는 중</translation>
    </message>
    <message>
        <location filename="widgets/splash_overlay.cpp" line="159"/>
        <source>(Unknown action)</source>
        <translation>(알 수 없는 동작)</translation>
    </message>
</context>
<context>
    <name>StatsTreeDialog</name>
    <message>
        <location filename="stats_tree_dialog.cpp" line="65"/>
        <source>Configuration not found</source>
        <translation>설정을 찾을 수 없습니다</translation>
    </message>
    <message>
        <location filename="stats_tree_dialog.cpp" line="66"/>
        <source>Unable to find configuration for %1.</source>
        <translation>%1에 대한 설정을 찾을 수 없습니다.</translation>
    </message>
</context>
<context>
    <name>StripHeadersDialog</name>
    <message>
        <location filename="strip_headers_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화상자</translation>
    </message>
    <message>
        <location filename="strip_headers_dialog.ui" line="45"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
</context>
<context>
    <name>SupportedProtocolsDialog</name>
    <message>
        <location filename="supported_protocols_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화상자</translation>
    </message>
    <message>
        <location filename="supported_protocols_dialog.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search the list of field names.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;필드 이름 목록을 검색합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="supported_protocols_dialog.ui" line="29"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
    <message>
        <location filename="supported_protocols_dialog.ui" line="54"/>
        <source>&lt;small&gt;&lt;i&gt;Gathering protocol information…&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;프로토콜 정보 수집중…&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="supported_protocols_dialog.cpp" line="38"/>
        <source>Supported Protocols</source>
        <translation>지원되는 프로토콜</translation>
    </message>
    <message>
        <location filename="supported_protocols_dialog.cpp" line="67"/>
        <source>%1 protocols, %2 fields.</source>
        <translation>%1 프로토콜, %2 필드.</translation>
    </message>
</context>
<context>
    <name>SupportedProtocolsModel</name>
    <message>
        <location filename="models/supported_protocols_model.cpp" line="74"/>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="models/supported_protocols_model.cpp" line="76"/>
        <source>Filter</source>
        <translation>필터</translation>
    </message>
    <message>
        <location filename="models/supported_protocols_model.cpp" line="78"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="models/supported_protocols_model.cpp" line="80"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
</context>
<context>
    <name>SyntaxLineEdit</name>
    <message>
        <location filename="widgets/syntax_line_edit.cpp" line="152"/>
        <source>Invalid filter: %1</source>
        <translation>유효하지 않은 필터: %1</translation>
    </message>
    <message>
        <location filename="widgets/syntax_line_edit.cpp" line="228"/>
        <source>&quot;%1&quot; is deprecated in favour of &quot;%2&quot;. See Help section 6.4.8 for details.</source>
        <translation>&quot;%1&quot; 는 &quot;%2&quot; 향에서 제거되었습니다. 세부사항을 위해 도움말 6.4.8 을 보십시오.</translation>
    </message>
    <message>
        <location filename="widgets/syntax_line_edit.cpp" line="232"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>TCPStreamDialog</name>
    <message>
        <location filename="tcp_stream_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화 상자</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="33"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;x&lt;/th&gt;&lt;td&gt;Zoom in X axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;X&lt;/th&gt;&lt;td&gt;Zoom out X axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;y&lt;/th&gt;&lt;td&gt;Zoom in Y axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Y&lt;/th&gt;&lt;td&gt;Zoom out Y axis&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Up&lt;/i&gt;&lt;/th&gt;&lt;td&gt;Next stream&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Dn&lt;/i&gt;&lt;/th&gt;&lt;td&gt;Previous stream&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;d&lt;/th&gt;&lt;td&gt;Switch direction (swap TCP endpoints)&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;Toggle mouse drag / zoom&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;s&lt;/th&gt;&lt;td&gt;Toggle relative / absolute sequence numbers&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;Toggle capture / session time origin&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;Toggle crosshairs&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;1&lt;/th&gt;&lt;td&gt;Round Trip Time graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;2&lt;/th&gt;&lt;td&gt;Throughput graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;3&lt;/th&gt;&lt;td&gt;Stevens-style Time / Sequence graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;4&lt;/th&gt;&lt;td&gt;tcptrace-style Time / Sequence graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;5&lt;/th&gt;&lt;td&gt;Window Scaling graph&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;Valuable and amazing time-saving keyboard shortcuts&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;Zoom in&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;Zoom out&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;Reset graph to its initial state&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;Move right 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;Move left 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;Move up 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;Move down 10 pixels&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;Move right 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;Move left 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;Move up 1 pixel&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;Move down 1 pixel&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Up&lt;/i&gt;&lt;/th&gt;&lt;td&gt;Next stream&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Dn&lt;/i&gt;&lt;/th&gt;&lt;td&gt;Previous stream&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;d&lt;/th&gt;&lt;td&gt;Switch direction (swap TCP endpoints)&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;Go to packet under cursor&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;Toggle mouse drag / zoom&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;s&lt;/th&gt;&lt;td&gt;Toggle relative / absolute sequence numbers&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;Toggle capture / session time origin&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;Toggle crosshairs&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;1&lt;/th&gt;&lt;td&gt;Round Trip Time graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;2&lt;/th&gt;&lt;td&gt;Throughput graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;3&lt;/th&gt;&lt;td&gt;Stevens-style Time / Sequence graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;4&lt;/th&gt;&lt;td&gt;tcptrace-style Time / Sequence graph&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;5&lt;/th&gt;&lt;td&gt;Window Scaling graph&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;

&lt;h3&gt;가치있고 놀라운 시간 절약형 키보드 단축키&lt;/h3&gt;
&lt;table&gt;&lt;tbody&gt;

&lt;tr&gt;&lt;th&gt;+&lt;/th&gt;&lt;td&gt;확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;-&lt;/th&gt;&lt;td&gt;축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;x&lt;/th&gt;&lt;td&gt;X 축에 확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;X&lt;/th&gt;&lt;td&gt;X 축에 축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;y&lt;/th&gt;&lt;td&gt;Y 축으로 확대&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Y&lt;/th&gt;&lt;td&gt;Y 축에 축소&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;0&lt;/th&gt;&lt;td&gt;초기 상태 그래프를 다시 설정&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 10 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;→&lt;/th&gt;&lt;td&gt;오른쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;←&lt;/th&gt;&lt;td&gt;왼쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↑&lt;/th&gt;&lt;td&gt;위쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Shift+&lt;/i&gt;↓&lt;/th&gt;&lt;td&gt;아래쪽으로 1 픽셀 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Up&lt;/i&gt;&lt;/th&gt;&lt;td&gt;다음 스트림&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;&lt;i&gt;Pg Dn&lt;/i&gt;&lt;/th&gt;&lt;td&gt;이전 스트림&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;d&lt;/th&gt;&lt;td&gt;방향 바꾸기 (TCP 종단점 바꾸기)&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;g&lt;/th&gt;&lt;td&gt;커서 아래의 패킷으로 이동&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;z&lt;/th&gt;&lt;td&gt;마우스 드래그 및 줌을 전환&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;s&lt;/th&gt;&lt;td&gt;상대와 절대 순서 번호를 전환&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;t&lt;/th&gt;&lt;td&gt;캡쳐 / 세션 시간 기준을 전환&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;Space&lt;/th&gt;&lt;td&gt;십자선 전환&lt;/td&gt;&lt;/th&gt;

&lt;tr&gt;&lt;th&gt;1&lt;/th&gt;&lt;td&gt;왕복 지연 시간 그래프&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;2&lt;/th&gt;&lt;td&gt;처리량 그래프&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;3&lt;/th&gt;&lt;td&gt;스티븐스 형식 시간/순서 그래프&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;4&lt;/th&gt;&lt;td&gt;tcptrace-형식 시간/순서 그래프&lt;/td&gt;&lt;/th&gt;
&lt;tr&gt;&lt;th&gt;5&lt;/th&gt;&lt;td&gt;창 규모화 그래프&lt;/td&gt;&lt;/th&gt;

&lt;/tbody&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="75"/>
        <source>&lt;small&gt;&lt;i&gt;Mouse over for shortcuts&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;단축키에 대한 마우스 연결&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="87"/>
        <source>Type</source>
        <translation>유형</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="117"/>
        <source>MA Window (s)</source>
        <translation>MA 윈도우 (초)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="130"/>
        <source>Allow SACK segments as well as data packets to be selected by clicking on the graph</source>
        <translation>그래프를 클릭하여 데이터 패킷인 SACK 세그먼트 허용함</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="133"/>
        <source>Select SACKs</source>
        <oldsource>select SACKs</oldsource>
        <translation>SACKs 선택</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="153"/>
        <source>Stream</source>
        <translation>스트림</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Switch the direction of the connection (view the opposite flow).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;연결 방향 전환 (역방향 흐름 표시).&lt;/p&gt;&lt;/body&gt;lt ;/html&gt;</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="166"/>
        <location filename="tcp_stream_dialog.ui" line="491"/>
        <source>Switch Direction</source>
        <translation>방향 전환</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="177"/>
        <source>Mouse</source>
        <translation>마우스</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="187"/>
        <source>Drag using the mouse button.</source>
        <translation>마우스 버튼를 사용하여 드래그합니다.</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="190"/>
        <source>drags</source>
        <translation>드래그</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="206"/>
        <source>Select using the mouse button.</source>
        <translation>마우스 버튼를 사용을 선택합니다.</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="209"/>
        <source>zooms</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="238"/>
        <source>Display Round Trip Time vs Sequence Number</source>
        <translation>왕복 시간 대 순서 번호 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="241"/>
        <source>RTT By Sequence Number</source>
        <translation>시퀀스 번호별 RTT</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="251"/>
        <source>Display graph of Segment Length vs Time</source>
        <translation>세그먼트 길이 대 시간의 그래프 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="254"/>
        <source>Segment Length</source>
        <translation>세그먼트 길이</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="264"/>
        <source>Display graph of Mean Transmitted Bytes vs Time</source>
        <translation>평균 전송된 바이트 대 시간의 그래프 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="277"/>
        <source>Display graph of Mean ACKed Bytes vs Time</source>
        <translation>평균 ACK 된 바이트 대 시간의 그래프 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="280"/>
        <source>Goodput</source>
        <translation>굿풋 (응용 프로그램 수준의 처리량)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="290"/>
        <source>Display graph of Receive Window Size vs Time</source>
        <translation>수신 윈도우 크기 대 시간의 그래프 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="293"/>
        <source>Rcv Win</source>
        <translation>수신 윈도우</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="303"/>
        <source>Display graph of Outstanding Bytes vs Time</source>
        <translation>눈에 띄는 바이트 대 시간의 그래프 표시</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="306"/>
        <source>Bytes Out</source>
        <translation>바이트 아웃</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="326"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset the graph to its initial state.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;그래프를 초기 상태로 다시 설정합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="329"/>
        <source>Reset</source>
        <translation>다시 설정</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="348"/>
        <source>Reset Graph</source>
        <translation>그래프를 다시 설정</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="351"/>
        <source>Reset the graph to its initial state.</source>
        <translation>그래프를 초기 상태로 다시 설정합니다.</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="354"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="359"/>
        <location filename="tcp_stream_dialog.ui" line="362"/>
        <source>Zoom In</source>
        <translation>확대</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="365"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="370"/>
        <location filename="tcp_stream_dialog.ui" line="373"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="376"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="381"/>
        <location filename="tcp_stream_dialog.ui" line="384"/>
        <source>Move Up 10 Pixels</source>
        <translation>위쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="387"/>
        <source>Up</source>
        <translation>위쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="392"/>
        <location filename="tcp_stream_dialog.ui" line="395"/>
        <source>Move Left 10 Pixels</source>
        <translation>왼쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="398"/>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="403"/>
        <location filename="tcp_stream_dialog.ui" line="406"/>
        <source>Move Right 10 Pixels</source>
        <translation>오른쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="409"/>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="414"/>
        <location filename="tcp_stream_dialog.ui" line="417"/>
        <source>Move Down 10 Pixels</source>
        <translation>아래쪽으로 10 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="420"/>
        <source>Down</source>
        <translation>아래쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="425"/>
        <location filename="tcp_stream_dialog.ui" line="428"/>
        <source>Move Up 1 Pixel</source>
        <translation>위쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="431"/>
        <source>Shift+Up</source>
        <translation>Shift+Up</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="436"/>
        <location filename="tcp_stream_dialog.ui" line="439"/>
        <source>Move Left 1 Pixel</source>
        <translation>왼쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="442"/>
        <source>Shift+Left</source>
        <translation>Shift+왼쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="447"/>
        <location filename="tcp_stream_dialog.ui" line="450"/>
        <source>Move Right 1 Pixel</source>
        <translation>오른쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="453"/>
        <source>Shift+Right</source>
        <translation>Shift+오른쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="458"/>
        <location filename="tcp_stream_dialog.ui" line="461"/>
        <source>Move Down 1 Pixel</source>
        <translation>아래쪽으로 1 픽셀 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="464"/>
        <source>Shift+Down</source>
        <translation>Shift+아래쪽</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="469"/>
        <source>Next Stream</source>
        <translation>다음 스트림</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="472"/>
        <source>Go to the next stream in the capture</source>
        <translation>캡쳐에서 다음 스트림으로 이동합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="475"/>
        <source>PgUp</source>
        <translation>페이지 업</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="480"/>
        <source>Previous Stream</source>
        <translation>이전 스트림</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="483"/>
        <source>Go to the previous stream in the capture</source>
        <translation>캡쳐의 이전 스트림으로 이동합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="486"/>
        <source>PgDown</source>
        <translation>페이지 다운</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="494"/>
        <source>Switch direction (swap TCP endpoints)</source>
        <translation>방향 전환 (TCP 종단점 바꾸기)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="497"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="502"/>
        <source>Go To Packet Under Cursor</source>
        <translation>커서 위치의 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="505"/>
        <source>Go to packet currently under the cursor</source>
        <translation>현재 커서 위치에 있는 패킷으로 이동합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="508"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="513"/>
        <source>Drag / Zoom</source>
        <translation>드래그 / 줌</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="516"/>
        <source>Toggle mouse drag / zoom behavior</source>
        <translation>마우스 드래그/줌 동작을 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="519"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="524"/>
        <source>Relative / Absolute Sequence Numbers</source>
        <translation>상대적/절대적 순서 번호</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="527"/>
        <source>Toggle relative / absolute sequence numbers</source>
        <translation>상대적/절대적 순서 번호를 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="530"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="535"/>
        <source>Capture / Session Time Origin</source>
        <translation>캡쳐 / 세션 시간 기점</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="538"/>
        <source>Toggle capture / session time origin</source>
        <translation>캡쳐 / 세션 시간 기점을 바꾸기</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="541"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="546"/>
        <source>Crosshairs</source>
        <translation>십자선</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="549"/>
        <source>Toggle crosshairs</source>
        <translation>십자선의 표시를 바꿉니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="552"/>
        <source>Space</source>
        <translation>스페이스</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="557"/>
        <location filename="tcp_stream_dialog.cpp" line="1440"/>
        <source>Round Trip Time</source>
        <translation>왕복 지연 시간</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="560"/>
        <source>Switch to the Round Trip Time graph</source>
        <translation>왕복 지연 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="563"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="267"/>
        <location filename="tcp_stream_dialog.ui" line="568"/>
        <location filename="tcp_stream_dialog.cpp" line="1168"/>
        <source>Throughput</source>
        <translation>처리량</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="571"/>
        <source>Switch to the Throughput graph</source>
        <translation>처리량 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="574"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="579"/>
        <source>Time / Sequence (Stevens)</source>
        <translation>시간 / 순서 (Stevens)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="582"/>
        <source>Switch to the Stevens-style Time / Sequence graph</source>
        <translation>Stevens 형식의 시간/순서 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="585"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="590"/>
        <location filename="tcp_stream_dialog.cpp" line="1535"/>
        <source>Window Scaling</source>
        <translation>윈도우 규모</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="593"/>
        <source>Switch to the Window Scaling graph</source>
        <translation>윈도우 규모 그래프로 전환</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="596"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="601"/>
        <source>Time / Sequence (tcptrace)</source>
        <translation>시간/순서 (tcptrace)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="604"/>
        <source>Switch to the tcptrace-style Time / Sequence graph</source>
        <translation>tcptrace 형식의 시간/순서 그래프로 전환합니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="607"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="612"/>
        <location filename="tcp_stream_dialog.ui" line="615"/>
        <source>Zoom In X Axis</source>
        <translation>X 축을 확대</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="618"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="623"/>
        <location filename="tcp_stream_dialog.ui" line="626"/>
        <source>Zoom Out X Axis</source>
        <translation>X 축을 축소</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="629"/>
        <source>Shift+X</source>
        <translation>Shift+X</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="634"/>
        <location filename="tcp_stream_dialog.ui" line="637"/>
        <source>Zoom In Y Axis</source>
        <translation>Y 축을 확대</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="640"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="645"/>
        <location filename="tcp_stream_dialog.ui" line="648"/>
        <source>Zoom Out Y Axis</source>
        <translation>Y 축을 축소</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.ui" line="651"/>
        <source>Shift+Y</source>
        <translation>Shift+Y</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="341"/>
        <source>Save As…</source>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="556"/>
        <source>No Capture Data</source>
        <translation>캡쳐 데이터가 없습니다</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="625"/>
        <source>%1 %2 pkts, %3 %4 %5 pkts, %6 </source>
        <translation>%1 %2 pkts, %3 %4 %5 pkts, %6 </translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="779"/>
        <source>Sequence Numbers (Stevens)</source>
        <translation>순서 번호 (Stevens)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="804"/>
        <source>Sequence Numbers (tcptrace)</source>
        <translation>순서 번호 (tcptrace)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1170"/>
        <source> (MA)</source>
        <translation> (MA)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1172"/>
        <source> (%1 Segment MA)</source>
        <translation> (%1 세그먼트 MA)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1193"/>
        <source> [not enough data]</source>
        <translation> [충분한 데이터가 없습니다]</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1590"/>
        <source> for %1:%2 %3 %4:%5</source>
        <translation>대상 %1:%2 %3 %4:%5</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1781"/>
        <source>%1 %2 (%3s len %4 seq %5 ack %6 win %7)</source>
        <translation>%1 %2 (%3s len %4 seq %5 ack %6 win %7)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1782"/>
        <source>Click to select packet</source>
        <translation>클릭하여 패킷을 선택</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1782"/>
        <source>Packet</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1796"/>
        <source>Release to zoom, x = %1 to %2, y = %3 to %4</source>
        <translation>확대를 위해 릴리즈, x = %1 ~ %2, y = %3 ~ %4</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1802"/>
        <source>Unable to select range.</source>
        <translation>범위를 선택할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1805"/>
        <source>Click to select a portion of the graph.</source>
        <translation>클릭하여 그래프의 비율을 선택합니다.</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1849"/>
        <source>Portable Document Format (*.pdf)</source>
        <translation>PDF 형식 (*.pdf)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1850"/>
        <source>Portable Network Graphics (*.png)</source>
        <translation>PNG 형식 (*.png)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1851"/>
        <source>Windows Bitmap (*.bmp)</source>
        <translation>윈도우즈 비트맵 형식 (*.bmp)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1853"/>
        <source>JPEG File Interchange Format (*.jpeg *.jpg)</source>
        <translation>JPEG 형식 (*.jpeg *.jpg)</translation>
    </message>
    <message>
        <location filename="tcp_stream_dialog.cpp" line="1860"/>
        <source>Save Graph As…</source>
        <translation>다른 이름으로 그래프 저장…</translation>
    </message>
</context>
<context>
    <name>TapParameterDialog</name>
    <message>
        <location filename="tap_parameter_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>대화상자</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="27"/>
        <source>Item</source>
        <translation>항목</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="35"/>
        <source>&lt;small&gt;&lt;i&gt;A hint.&lt;/i&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;i&gt;필터힌트.&lt;/i&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="47"/>
        <source>Display filter:</source>
        <translation>표시 필터:</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="57"/>
        <source>Regenerate statistics using this display filter</source>
        <translation>표시 필터를 사용하여 통계를 다시 생성</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="60"/>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="79"/>
        <location filename="tap_parameter_dialog.cpp" line="81"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="82"/>
        <source>Copy a text representation of the tree to the clipboard</source>
        <translation>트리의 텍스트 표현을 클립 보드에 복사</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="90"/>
        <location filename="tap_parameter_dialog.cpp" line="84"/>
        <source>Save as…</source>
        <oldsource>Save as...</oldsource>
        <translation>다른 이름으로 저장…</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.ui" line="93"/>
        <source>Save the displayed data in various formats</source>
        <translation>다양한 형식으로 표시 데이터를 저장합니다</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="485"/>
        <source>Collapse All</source>
        <translation>모두 접기</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="489"/>
        <source>Expand All</source>
        <translation>모두 확장</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="551"/>
        <source>Save Statistics As…</source>
        <translation>다른 이름으로 통계치를 저장…</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="553"/>
        <source>Plain text file (*.txt);;Comma separated values (*.csv);;XML document (*.xml);;YAML document (*.yaml)</source>
        <translation>일반 텍스트 파일 (*.txt);;쉼표로 구분된 값 (*.csv);;XML 문서 (*.xml);;YAML 문서 (*.yaml)</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="557"/>
        <source>Plain text file (*.txt)</source>
        <translation>일반 텍스트 파일 (*.txt)</translation>
    </message>
    <message>
        <location filename="tap_parameter_dialog.cpp" line="603"/>
        <source>Error saving file %1</source>
        <translation>파일 저장 오류 %1</translation>
    </message>
</context>
<context>
    <name>TimeShiftDialog</name>
    <message>
        <location filename="time_shift_dialog.ui" line="25"/>
        <source>Shift all packets by</source>
        <translation>모든 패킷을 조정합니다</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="38"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;[-][[hh:]mm:]ss[.ddd] &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;[-][[hh:]mm:]ss[.ddd] &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="62"/>
        <source>Set the time for packet</source>
        <translation>패킷에 대한 시간 설정</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="72"/>
        <location filename="time_shift_dialog.ui" line="106"/>
        <source>to</source>
        <translation>시간 설정</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="96"/>
        <source>…then set packet</source>
        <oldsource>...then set packet</oldsource>
        <translation>… 그리고 패킷 번호</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="140"/>
        <source>and extrapolate the time for all other packets</source>
        <translation>그리고 모든 다른 패킷의 시간을 추정합니다</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="160"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;[YYYY-MM-DD] hh:mm:ss[.ddd] &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;&lt;span style=&quot; font-size:small; font-style:italic;&quot;&gt;[YYYY-MM-DD] hh:mm:ss[.ddd] &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.ui" line="169"/>
        <source>Undo all shifts</source>
        <translation>모든 조정을 되돌리기</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.cpp" line="27"/>
        <source>Time Shift</source>
        <translation>시간 조정</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.cpp" line="127"/>
        <source>Frame numbers must be between 1 and %1.</source>
        <translation>프레임 번호는 1에서 %1 사이여야 합니다.</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.cpp" line="129"/>
        <source>Invalid frame number.</source>
        <translation>프레임 번호가 잘못되었습니다.</translation>
    </message>
    <message>
        <location filename="time_shift_dialog.cpp" line="232"/>
        <source>Time shifting is not available capturing packets.</source>
        <translation>시간 조정은 패킷 캡쳐에 사용할 수 없습니다.</translation>
    </message>
</context>
<context>
    <name>TrafficTab</name>
    <message>
        <location filename="widgets/traffic_tab.cpp" line="471"/>
        <location filename="widgets/traffic_tab.cpp" line="585"/>
        <location filename="widgets/traffic_tab.cpp" line="592"/>
        <source>Map file error</source>
        <translation>맵 파일 오류</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tab.cpp" line="471"/>
        <source>Could not open base file %1 for reading: %2</source>
        <translation>%2 읽기에 대한 기반 파일 %1 을 열 수 없습니다.</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tab.cpp" line="585"/>
        <source>No endpoints available to map</source>
        <translation>맵에 사용 가능한 종단점이 없음</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tab.cpp" line="592"/>
        <source>Unable to create temporary file</source>
        <translation>임시 파일을 생성할 수 없음</translation>
    </message>
</context>
<context>
    <name>TrafficTableDialog</name>
    <message>
        <location filename="traffic_table_dialog.ui" line="34"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show resolved addresses and port names rather than plain values. The corresponding name resolution preference must be enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;플레인 값이 아닌 해석된 주소와 포트 이름을 표시합니다. 관련 이름 해석 설정이 활성되어 있어야합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="37"/>
        <source>Name resolution</source>
        <translation>이름 해석</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="54"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only show conversations matching the current display filter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;현재 표시 필터에 맞는 대화만 표시합니다&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="57"/>
        <source>Limit to display filter</source>
        <translation>표시 필터에 제한</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="99"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only show types matching the filter value&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;필터값과 일치하는 값만 표시&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="105"/>
        <source>Filter list for specific type</source>
        <translation>특정 유형에 대한 필터 목록</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show absolute times in the start time column.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;시작 시간 열에 절대 시간을 표시합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="28"/>
        <source>GroupBox</source>
        <translation>그룹상자</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.ui" line="47"/>
        <source>Absolute start time</source>
        <translation>절대 시작 시간</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.cpp" line="49"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="traffic_table_dialog.h" line="51"/>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
</context>
<context>
    <name>TrafficTree</name>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="639"/>
        <source>Resize all columns to content</source>
        <translation>컨텐츠에 맞춰 모든 열 재조정</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="683"/>
        <source>Filter on stream id</source>
        <translation>스트림 ID 상의 필터</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="709"/>
        <source>Copy %1 table</source>
        <translation>%1 테이블 복사</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="711"/>
        <source>as CSV</source>
        <translation>CSV</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="712"/>
        <source>Copy all values of this page to the clipboard in CSV (Comma Separated Values) format.</source>
        <translation>이 페이지의 모든 값을 CSV 형식으로 클립보드에 복사합니다.</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="715"/>
        <source>as YAML</source>
        <translation>YAML</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="716"/>
        <source>Copy all values of this page to the clipboard in the YAML data serialization format.</source>
        <translation>이 페이지의 모든 값을 YAML 데이터 순열 형식으로 클립보드에 복사합니다.</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="719"/>
        <source>as JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="720"/>
        <source>Copy all values of this page to the clipboard in the JSON data serialization format.</source>
        <translation>이 페이지의 모든 값을 JSON 데이터 순열 형식으로 클립보드에 복사합니다.</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="725"/>
        <source>Save data as raw</source>
        <translation>raw 형식으로 데이터 저장</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="726"/>
        <source>Disable data formatting for export/clipboard and save as raw data</source>
        <translation>내보내기/클립보드를 위해 데이터 형식 지정을 하지 않고 raw 데이터를 저장</translation>
    </message>
</context>
<context>
    <name>TrafficTreeHeaderView</name>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="86"/>
        <source>Less than</source>
        <translation>작음</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="90"/>
        <source>Greater than</source>
        <translation>높음</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="93"/>
        <source>Equal</source>
        <translation>같음</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="116"/>
        <source>Columns to display</source>
        <translation>표시할 열</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="146"/>
        <source>Filter %1 by</source>
        <translation>다음 내용으로 %1 필터링</translation>
    </message>
    <message>
        <location filename="widgets/traffic_tree.cpp" line="150"/>
        <source>Enter filter value</source>
        <translation>필터값 입력</translation>
    </message>
</context>
<context>
    <name>TrafficTypesModel</name>
    <message>
        <location filename="widgets/traffic_types_list.cpp" line="134"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
</context>
<context>
    <name>UatDialog</name>
    <message>
        <location filename="uat_dialog.ui" line="38"/>
        <source>Create a new entry.</source>
        <translation>새로운 항목을 만듭니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.ui" line="51"/>
        <source>Remove this entry.</source>
        <oldsource>Remove this profile.</oldsource>
        <translation>이 항목을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.ui" line="61"/>
        <source>Copy this entry.</source>
        <oldsource>Copy this profile.</oldsource>
        <translation>이 항목을 복사합니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.ui" line="74"/>
        <source>Move entry up.</source>
        <translation>항목을 올립니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.ui" line="87"/>
        <source>Move entry down.</source>
        <translation>항목을 내립니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.ui" line="100"/>
        <source>Clear all entries.</source>
        <translation>모든 항목을 지웁니다.</translation>
    </message>
    <message>
        <location filename="uat_dialog.cpp" line="90"/>
        <source>Unknown User Accessible Table</source>
        <translation>알 수 없는 사용자가 접근 가능한 표</translation>
    </message>
    <message>
        <location filename="uat_dialog.cpp" line="113"/>
        <source>Open </source>
        <translation>열기 </translation>
    </message>
</context>
<context>
    <name>UatFrame</name>
    <message>
        <location filename="uat_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="44"/>
        <source>Create a new entry.</source>
        <translation>새로운 항목을 만듭니다.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="57"/>
        <source>Remove this entry.</source>
        <translation>이 항목을 삭제합니다.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="67"/>
        <source>Copy this entry.</source>
        <translation>이 항목을 복사합니다.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="77"/>
        <source>Move entry up.</source>
        <translation>항목을 위로 이동하십시오.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="87"/>
        <source>Move entry down.</source>
        <translation>항목을 아래로 이동하십시오.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="97"/>
        <source>Clear all entries.</source>
        <translation>모든 항목을 삭제하십시오.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="107"/>
        <source>Copy entries from another profile.</source>
        <translation>다른 프로파일에서 항목 복사합니다.</translation>
    </message>
    <message>
        <location filename="uat_frame.ui" line="110"/>
        <source>Copy from</source>
        <translation>복사 위치, 에서복사</translation>
    </message>
    <message>
        <location filename="uat_frame.cpp" line="81"/>
        <source>Unknown User Accessible Table</source>
        <translation>알 수 없는 사용자가 접근할 수 있는 표</translation>
    </message>
    <message>
        <location filename="uat_frame.cpp" line="102"/>
        <source>Open </source>
        <translation>열기 </translation>
    </message>
</context>
<context>
    <name>VoipCallsDialog</name>
    <message>
        <location filename="voip_calls_dialog.ui" line="39"/>
        <source>&lt;small&gt;&lt;/small&gt;</source>
        <translation>&lt;small&gt;&lt;/small&gt;</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="48"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only show conversations matching the current display filter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;현재 표시 필터와 일치하는 대화만 보이기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>

    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="51"/>
        <source>Limit to display filter</source>
        <translation>표시 필터 제한</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="58"/>
        <source>Time of Day</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="90"/>
        <source>Flow &amp;Sequence</source>
        <translation>시퀀스 따르기(&amp;S)</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="93"/>
        <source>Show flow sequence for selected call(s).</source>
        <translation>선택된 호출에 대한 시퀀스 흐름을 보여줍니다.</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="98"/>
        <source>Prepare &amp;Filter</source>
        <translation>필터 준비(&amp;F)</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="101"/>
        <source>Prepare a filter matching the selected calls(s).</source>
        <translation>선택된 호출과 일치하는 필터를 준비합니다.</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="106"/>
        <source>Cop&amp;y</source>
        <translation>복사(&amp;y)</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="109"/>
        <source>Open copy menu</source>
        <translation>복사 메뉴 열기</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="125"/>
        <source>All</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="128"/>
        <source>Select all</source>
        <translation>모두 선택</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="136"/>
        <source>None</source>
        <translation>없음</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="147"/>
        <source>Invert</source>
        <translation>반전</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="150"/>
        <source>Invert selection</source>
        <translation>선택 반전</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="158"/>
        <source>Select related RTP streams</source>
        <translation>연관된 RTP 스트림 선택</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="161"/>
        <location filename="voip_calls_dialog.ui" line="172"/>
        <source>Select RTP streams related to selected calls in RTP Streams dialog</source>
        <translation>RTP 스트림 대화상자에서 선택한 호출과 연관된 RTP 스트림을 선택</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="164"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="169"/>
        <source>Deselect related RTP Streams</source>
        <translation>연관된 RTP 스트림 선택 해제</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="175"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="139"/>
        <source>Clear selection</source>
        <translation>선택 해제</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="268"/>
        <source>Display time as time of day</source>
        <translation>하루 기준으로 시간 표시</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="273"/>
        <source>Copy as CSV</source>
        <translation>CSV로 복사</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="274"/>
        <source>Copy stream list as CSV.</source>
        <translation>CSV로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="275"/>
        <source>Copy as YAML</source>
        <translation>YAML로 복사</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="276"/>
        <source>Copy stream list as YAML.</source>
        <translation>YAML로 스트림 목록을 복사합니다.</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="96"/>
        <source>SIP Flows</source>
        <translation>SIP 흐름</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="14"/>
        <location filename="voip_calls_dialog.cpp" line="96"/>
        <source>VoIP Calls</source>
        <translation>VoIP 호출</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="110"/>
        <source>as CSV</source>
        <translation>CSV로</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.cpp" line="112"/>
        <source>as YAML</source>
        <translation>YAML로</translation>
    </message>
    <message>
        <location filename="voip_calls_dialog.ui" line="114"/>
        <source>Select</source>
        <translation>선택</translation>
    </message>
</context>
<context>
    <name>VoipCallsInfoModel</name>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="86"/>
        <source>On</source>
        <translation>켜기</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="87"/>
        <source>Off</source>
        <translation>끄기</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="95"/>
        <source>Tunneling: %1  Fast Start: %2</source>
        <translation>터널링: %1 빠른 시작: %2</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="115"/>
        <source>Start Time</source>
        <translation>시작 시간</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="117"/>
        <source>Stop Time</source>
        <translation>종료 시간</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="119"/>
        <source>Initial Speaker</source>
        <translation>초기 스피커</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="121"/>
        <source>From</source>
        <translation>부터</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="123"/>
        <source>To</source>
        <translation>까지</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="125"/>
        <source>Protocol</source>
        <translation>프로토콜</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="127"/>
        <source>Duration</source>
        <translation>지속시간</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="129"/>
        <source>Packets</source>
        <translation>패킷</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="131"/>
        <source>State</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="models/voip_calls_info_model.cpp" line="133"/>
        <source>Comments</source>
        <translation>주석</translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="welcome_page.ui" line="14"/>
        <source>Form</source>
        <translation>양식</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="63"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:large;&quot;&gt;Welcome to Wireshark&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:large;&quot;&gt;Wireshark에 오신 것을 환영합니다&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="129"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open a file on your file system&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;파일 시스템 상의 파일 열기&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="132"/>
        <source>&lt;h2&gt;Open&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;열기&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="145"/>
        <source>Recent capture files</source>
        <translation>최근 캡쳐 파일</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="148"/>
        <source>Capture files that have been opened previously</source>
        <translation>이전에 열었던 캡쳐 파일</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="167"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Capture live packets from your network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;여러분의 네트워크에서 패킷을 바로 캡쳐하십시오.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="170"/>
        <source>&lt;h2&gt;Capture&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;캡쳐&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="189"/>
        <source>…using this filter:</source>
        <translation>…이 필터 사용:</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="225"/>
        <source>Interface list</source>
        <translation>인터페이스 목록</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="228"/>
        <source>List of available capture interfaces</source>
        <translation>사용 가능한 캡쳐 인터페이스 목록</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="247"/>
        <source>&lt;h2&gt;Learn&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;배우기&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="welcome_page.ui" line="254"/>
        <source>&lt;html&gt;&lt;head&gt;
&lt;style&gt;
a:link {
  color: palette(text);
  text-decoration: none;
}
a:hover {
  color: palette(text);
  text-decoration: underline;
}
&lt;/style&gt;
&lt;/head&gt;
&lt;body&gt;

&lt;table&gt;&lt;tr&gt;
&lt;th&gt;&lt;a href=&quot;https://www.wireshark.org/docs/wsug_html_chunked/&quot;&gt;User's Guide&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://gitlab.com/wireshark/wireshark/-/wikis/&quot;&gt;Wiki&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://ask.wireshark.org/&quot;&gt;Questions and Answers&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://www.wireshark.org/lists/&quot;&gt;Mailing Lists&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://sharkfest.wireshark.org/&quot;&gt;SharkFest&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://discord.com/invite/ts9GZCjGj5&quot;&gt;Wireshark Discord&lt;/a&gt;&lt;/th&gt;

&lt;td style=&quot;padding-left: 8px; padding-right: 8px;&quot;&gt;·&lt;/td&gt;

&lt;th&gt;&lt;a href=&quot;https://wiresharkfoundation.org/donate/&quot;&gt;Donate&lt;/a&gt;&lt;/th&gt;

&lt;/tr&gt;&lt;/table&gt;
&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="49"/>
        <source>Show in Finder</source>
        <translation>찾기 도구로 표시</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="51"/>
        <source>Show in Folder</source>
        <translation>폴더 표시</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="62"/>
        <source>Welcome to %1</source>
        <translation>%1 에 오신 것을 환영</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="123"/>
        <source>All interfaces shown</source>
        <translation>모든 인터페이스가 표시됨</translation>
    </message>
    <message numerus="yes">
        <location filename="welcome_page.cpp" line="125"/>
        <source>%n interface(s) shown, %1 hidden</source>
        <translation><numerusform>%n 인터페이스 표시됨, %1 숨겨짐</numerusform></translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="139"/>
        <source>You are sniffing the glue that holds the Internet together using Wireshark </source>
        <translation>Wireshark를 사용하여 인터넷을 고정하는 접착제를 냄새 맡고(스니핑) 있습니다 </translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="141"/>
        <source>You are running Wireshark </source>
        <translation>Wireshark를 실행하고 있습니다 </translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="147"/>
        <source> You receive automatic updates.</source>
        <translation> 자동 갱신을 받으십시오.</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="149"/>
        <source> You have disabled automatic updates.</source>
        <translation> 자동 갱신을 사용하지 않도록 설정하였습니다.</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="302"/>
        <source>not found</source>
        <translation>찾지 못했습니다</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="383"/>
        <source>Copy file path</source>
        <translation>파일 경로 복사</translation>
    </message>
    <message>
        <location filename="welcome_page.cpp" line="389"/>
        <source>Remove from list</source>
        <translation>목록에서 제거</translation>
    </message>
</context>
<context>
    <name>WirelessFrame</name>
    <message>
        <location filename="wireless_frame.ui" line="14"/>
        <source>Frame</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="42"/>
        <source>Interface</source>
        <translation>인터페이스</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="69"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the 802.11 channel.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;802.11 채널을 설정합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="72"/>
        <source>Channel</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When capturing, show all frames, ones that have a valid frame check sequence (FCS), or ones with an invalid FCS.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;캡쳐할 때 모든 프레임, 올바른 프레임 확인 순서(FCS, Frame Check Sequence) 또는 FCS가 잘못된 프레임을 표시합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="127"/>
        <source>FCS Filter</source>
        <translation>FCS 필터</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="135"/>
        <source>All Frames</source>
        <translation>모든 프레임</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="140"/>
        <source>Valid Frames</source>
        <translation>유효한 프레임</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="145"/>
        <source>Invalid Frames</source>
        <translation>유효하지 않은 프레임</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="166"/>
        <source>Wireless controls are not supported in this version of Wireshark.</source>
        <translation>무선 제어는 이 버전의 Wireshark에서 지원되지 않습니다.</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="190"/>
        <source>External Helper</source>
        <translation>외부 도우미</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show the IEEE 802.11 preferences, including decryption keys.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;복호화 키를 포함 IEEE 802.11 설정을 표시합니다.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="wireless_frame.ui" line="200"/>
        <source>802.11 Preferences</source>
        <translation>802.11 설정</translation>
    </message>
    <message>
        <location filename="wireless_frame.cpp" line="60"/>
        <source>AirPcap Control Panel</source>
        <translation>AirPcap 제어판</translation>
    </message>
    <message>
        <location filename="wireless_frame.cpp" line="61"/>
        <source>Open the AirPcap Control Panel</source>
        <translation>AirPcap 제어판을 열기</translation>
    </message>
    <message>
        <location filename="wireless_frame.cpp" line="330"/>
        <location filename="wireless_frame.cpp" line="338"/>
        <source>Unable to set channel or offset.</source>
        <translation>채널 또는 오프셋을 설정할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireless_frame.cpp" line="344"/>
        <source>Unable to set FCS validation behavior.</source>
        <translation>FCS의 확인 작업을 설정할 수 없습니다.</translation>
    </message>
</context>
<context>
    <name>WirelessTimeline</name>
    <message>
        <location filename="widgets/wireless_timeline.cpp" line="273"/>
        <source>Packet number %1 does not include TSF timestamp, not showing timeline.</source>
        <translation>%1 패킷 번호에 TSF 타임스탬프가 포함되어 있지 않고 타임라인이 표시되지 않습니다.</translation>
    </message>
    <message>
        <location filename="widgets/wireless_timeline.cpp" line="278"/>
        <source>Packet number %u has large negative jump in TSF, not showing timeline. Perhaps TSF reference point is set wrong?</source>
        <translation>%u 패킷 번호는 TSF에서 크게 역방향으로 점프하지만 타임라인을 보이지 않습니다. TSF 기준점이 잘못 설정되었습니까?</translation>
    </message>
</context>
<context>
    <name>WiresharkDialog</name>
    <message>
        <location filename="wireshark_dialog.cpp" line="103"/>
        <source>Failed to attach to tap &quot;%1&quot;</source>
        <translation>탭 &quot;%1&quot; 부착에 실패했습니다</translation>
    </message>
</context>
<context>
    <name>WiresharkMainWindow</name>
    <message>
        <location filename="wireshark_main_window.ui" line="17"/>
        <location filename="wireshark_main_window.ui" line="910"/>
        <source>Wireshark</source>
        <translation>Wireshark</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="87"/>
        <source>Go to packet</source>
        <translation>패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="103"/>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="159"/>
        <source>File Set</source>
        <translation>파일 설정</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="167"/>
        <source>Export Packet Dissections</source>
        <translation>패킷 분해자 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="179"/>
        <source>Export Objects</source>
        <translation>객체 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="285"/>
        <source>&amp;Zoom</source>
        <translation>확대(&amp;Z)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="293"/>
        <source>&amp;Time Display Format</source>
        <translation>시간 표시 형식(&amp;T)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="654"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="226"/>
        <source>Manual pages</source>
        <translation>메뉴얼 페이지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="400"/>
        <source>Apply as Filter</source>
        <translation>필터로서 적용하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="405"/>
        <source>Prepare as Filter</source>
        <translation>필터로서 준비하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="410"/>
        <source>SCTP</source>
        <translation>SCTP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="463"/>
        <source>TCP Stream Graphs</source>
        <translation>TCP 스트림 그래프</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="473"/>
        <source>BACnet</source>
        <translation>BACnet</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="482"/>
        <source>HTTP</source>
        <translation>HTTP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="149"/>
        <source>&amp;File</source>
        <translation>파일(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="207"/>
        <source>&amp;Capture</source>
        <translation>캡쳐(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="219"/>
        <source>&amp;Help</source>
        <translation>도움말(&amp;)</translation)
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="258"/>
        <source>&amp;Go</source>
        <translation>이동(&amp;G)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="276"/>
        <source>&amp;View</source>
        <translation>보기(&amp;)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="396"/>
        <source>&amp;Analyze</source>
        <translation>분석(&amp;)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="418"/>
        <source>Follow</source>
        <translation>따라가기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="459"/>
        <source>&amp;Statistics</source>
        <translation>통계(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="491"/>
        <location filename="wireshark_main_window.ui" line="2063"/>
        <source>29West</source>
        <translation>29West</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="495"/>
        <source>Topics</source>
        <translation>주제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="507"/>
        <source>Queues</source>
        <translation>큐</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="516"/>
        <source>UIM</source>
        <translation>UIM</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="579"/>
        <source>Telephon&amp;y</source>
        <translation>텔레포니(&amp;y)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="583"/>
        <source>RTSP</source>
        <translation>RTSP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="650"/>
        <source>&amp;Edit</source>
        <translation>편집(&amp;E)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="692"/>
        <source>Packet Comments</source>
        <translation>패킷 주석</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="738"/>
        <source>Main Toolbar</source>
        <translation>주요 도구막대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="786"/>
        <source>Display Filter Toolbar</source>
        <translation>표시 필터 도구막대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="824"/>
        <source>Open a capture file</source>
        <translation>캡쳐 파일 열기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="838"/>
        <source>Quit Wireshark</source>
        <translation>Wireshark 종료</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="852"/>
        <source>&amp;Start</source>
        <translation>시작(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="855"/>
        <source>Start capturing packets</source>
        <translation>패킷 캡쳐링 시작</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="863"/>
        <source>S&amp;top</source>
        <translation>정지(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="866"/>
        <source>Stop capturing packets</source>
        <translation>패킷 캡쳐링 정지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="891"/>
        <source>No files found</source>
        <translation>찾은 파일 없음</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="896"/>
        <source>&amp;Contents</source>
        <translation>컨텐츠(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="915"/>
        <source>Wireshark Filter</source>
        <translation>Wireshark 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="920"/>
        <source>TShark</source>
        <translation>TShark</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="925"/>
        <source>Rawshark</source>
        <translation>Rawshark</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="930"/>
        <source>Dumpcap</source>
        <translation>Dumpcap</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="935"/>
        <source>Mergecap</source>
        <translation>Mergecap</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="940"/>
        <source>Editcap</source>
        <translation>Editcap</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="945"/>
        <source>Text2pcap</source>
        <translation>Text2pacp</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="954"/>
        <source>Website</source>
        <translation>웹사이트</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="964"/>
        <source>Downloads</source>
        <translation>다운로드</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="973"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="981"/>
        <source>Sample Captures</source>
        <translation>샘플 캡쳐</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="986"/>
        <source>&amp;About Wireshark</source>
        <translation>Wireshark에 관하여(&amp;A)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="998"/>
        <source>Ask (Q&amp;&amp;A)</source>
        <translation>물어보기(Q&amp;&amp;A)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1006"/>
        <source>Next Packet</source>
        <translation>다음 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1009"/>
        <source>Go to the next packet</source>
        <translation>다음 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1017"/>
        <source>Previous Packet</source>
        <translation>이전 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1020"/>
        <source>Go to the previous packet</source>
        <translation>이전 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1072"/>
        <source>First Packet</source>
        <translation>처음 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1075"/>
        <source>Go to the first packet</source>
        <translation>첫 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1083"/>
        <source>Last Packet</source>
        <translation>마지막 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1086"/>
        <source>Go to the last packet</source>
        <translation>마지막 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1097"/>
        <source>E&amp;xpand Subtrees</source>
        <translation>하위 트리 확장하기(&amp;x)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1100"/>
        <source>Expand the current packet detail</source>
        <translation>현재 패킷 세부사항을 확장하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1122"/>
        <source>&amp;Expand All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1125"/>
        <source>Expand packet details</source>
        <translation>패킷 세부정보 확장하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1133"/>
        <source>Collapse &amp;All</source>
        <translation>모두 접기(&amp;A)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1136"/>
        <source>Collapse all packet details</source>
        <translation>모든 패킷의 세부사항 접기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1150"/>
        <source>Go to specified packet</source>
        <translation>지정된 패킷으로 이동하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1161"/>
        <source>Merge one or more files</source>
        <translation>하나 이상의 파일을 병합하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1169"/>
        <source>Import a file</source>
        <translation>파일 가져오기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1174"/>
        <source>&amp;Save</source>
        <translation>저장(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1191"/>
        <source>Save as a different file</source>
        <translation>다른 파일에 저장하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1202"/>
        <source>Export specified packets</source>
        <translation>지정한 패킷 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1215"/>
        <source>Export TLS Session Keys…</source>
        <translation>TLS 세션 키 내보내기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1228"/>
        <source>List Files</source>
        <translation>파일 나열하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1233"/>
        <source>Next File</source>
        <translation>다음 파일</translation> 
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1238"/>
        <source>Previous File</source>
        <translation>이전 파일</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1243"/>
        <source>&amp;Reload</source>
        <translation>재로딩(&amp;R)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1268"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1271"/>
        <source>Capture options</source>
        <translation>캡쳐 옵션</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1285"/>
        <source>Capture filters</source>
        <translation>캡쳐 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1290"/>
        <source>Refresh Interfaces</source>
        <translation>인터페이스 갱신</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1293"/>
        <source>Refresh interfaces</source>
        <translation>인터페이스 갱신</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1301"/>
        <source>&amp;Restart</source>
        <translation>재시작(&amp;R)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1304"/>
        <source>Restart current capture</source>
        <translation>현재 캡쳐 재시작</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1317"/>
        <source>As &amp;CSV…</source>
        <translation>CSV로…(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1322"/>
        <source>As &quot;C&quot; &amp;Arrays…</source>
        <translation>&quot;C&quot; 배열로…(&amp;A)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1327"/>
        <source>As P&amp;SML XML…</source>
        <translation>PSML XML로…(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1332"/>
        <source>As P&amp;DML XML…</source>
        <translation>PDML XML로…(&amp;D)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1337"/>
        <source>As &amp;JSON…</source>
        <translation>JSON으로…(&amp;J)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1342"/>
        <source>Description</source>
        <translation>기술</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1381"/>
        <source>Field Name</source>
        <translation>필드 이름</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1392"/>
        <source>Value</source>
        <translation>값</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1403"/>
        <source>As Filter</source>
        <translation>필터로</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="877"/>
        <source>Close this capture file</source>
        <translation>이 캡쳐 파일 닫기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="70"/>
        <source>Packet:</source>
        <translation>패킷:</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="280"/>
        <source>Interface Toolbars</source>
        <translation>인터페이스 도구막대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="328"/>
        <source>Colorize Conversation</source>
        <translation>대화 색상화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="346"/>
        <source>Internals</source>
        <translation>내부</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="354"/>
        <source>Additional Toolbars</source>
        <translation>추가 도구막대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="432"/>
        <source>Conversation Filter</source>
        <translation>대화 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="557"/>
        <source>Reliable Server Pooling (RSerPool)</source>
        <translation>신뢰할 만한 서버 풀링 (RSerPool)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="564"/>
        <source>SOME/IP</source>
        <translation>SOME/IP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="572"/>
        <source>&amp;DTN</source>
        <translation>DTN(&amp;D)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="628"/>
        <source>Osmux</source>
        <translation>Dsmux</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="713"/>
        <source>&amp;Tools</source>
        <oldsource>Tools</oldsource>
        <translation>도구</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="807"/>
        <source>Wireless Toolbar</source>
        <translation>무선 도구막대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="899"/>
        <source>Help contents</source>
        <translation>도움말 컨텐츠</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="959"/>
        <source>FAQs</source>
        <translation>자주 묻는 질문</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1028"/>
        <source>Next Packet in Conversation</source>
        <translation>대화 내의 다음 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1031"/>
        <source>Go to the next packet in this conversation</source>
        <translation>이 대화에서 다음 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1039"/>
        <source>Previous Packet in Conversation</source>
        <translation>대화내의 이전 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1042"/>
        <source>Go to the previous packet in this conversation</source>
        <translation>이 대화 내의 이전 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1050"/>
        <source>Next Packet In History</source>
        <translation>히스토리 내의 다음 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1053"/>
        <source>Go to the next packet in your selection history</source>
        <translation>당신이 선택한 히스토리 내의 다음 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1061"/>
        <source>Previous Packet In History</source>
        <translation>히스토리 내의 이전 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1064"/>
        <source>Go to the previous packet in your selection history</source>
        <translation>당신이 선택한 히스토리내의 이전 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1111"/>
        <source>Collapse Subtrees</source>
        <translation>하위 트리 접기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1114"/>
        <source>Collapse the current packet detail</source>
        <translation>현재 패킷 세부사항을 접기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1147"/>
        <source>Go to Packet…</source>
        <translation>패킷으로 이동…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1158"/>
        <source>&amp;Merge…</source>
        <translation>병합…(&amp;M)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1166"/>
        <source>&amp;Import from Hex Dump…</source>
        <translation>16진수 덤프로부터 가져오기(&amp;I)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1177"/>
        <source>Save this capture file</source>
        <translation>이 캡쳐파일 저장하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1188"/>
        <source>Save &amp;As…</source>
        <translation>다른 이름으로 저장하기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1199"/>
        <source>Export Specified Packets…</source>
        <translation>지정한 패킷 내보내기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1207"/>
        <source>Export Packet &amp;Bytes…</source>
        <translation>패킷 바이트 내보내기…(&amp;B)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1220"/>
        <source>&amp;Print…</source>
        <translation>출력…(&amp;P)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1246"/>
        <source>Reload this file</source>
        <translation>이 파일 다시 로딩</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1257"/>
        <source>Reload as File Format/Capture</source>
        <translation>파일 형식/캡쳐로서 다시 불러오기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1345"/>
        <source>Copy this item&apos;s description</source>
        <translation>이 항목의 기술을 복사</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1384"/>
        <source>Copy this item&apos;s field name</source>
        <translation>이 항목의 필드 이름 복사</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1395"/>
        <source>Copy this item&apos;s value</source>
        <translation>이 항목의 값 복사</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1406"/>
        <source>Copy this item as a display filter</source>
        <translation>표시 필터로서 이 항목 복사</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1424"/>
        <source>Apply as Column</source>
        <translation>열로서 적용</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1427"/>
        <source>Create a packet list column from the selected field.</source>
        <translation>선택된 필드로부터 패킷 목록 열을 생성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1438"/>
        <source>Find a packet</source>
        <translation>패킷 찾기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1449"/>
        <source>Find the next packet</source>
        <translation>다음 패킷 찾기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1460"/>
        <source>Find the previous packet</source>
        <translation>이전 패킷 찾기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1468"/>
        <location filename="wireshark_main_window_slots.cpp" line="1257"/>
        <source>&amp;Mark/Unmark Packet(s)</source>
        <oldsource>&amp;Mark/Unmark Packet</oldsource>
        <translation>패킷 마크/마크해제(&amp;M)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1479"/>
        <source>Mark All Displayed</source>
        <translation>표시된 모든 패킷 마크</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1482"/>
        <source>Mark all displayed packets</source>
        <translation>모든 표시된 패킷들을 마크</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1493"/>
        <source>Unmark all displayed packets</source>
        <translation>모든 표시된 패킷들을 마크 해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1501"/>
        <source>Next Mark</source>
        <translation>다음 마크</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1504"/>
        <source>Go to the next marked packet</source>
        <translation>다음 마크 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1512"/>
        <source>Previous Mark</source>
        <translation>이전 마크</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1515"/>
        <source>Go to the previous marked packet</source>
        <translation>이전 마크한 패킷으로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1523"/>
        <location filename="wireshark_main_window_slots.cpp" line="1258"/>
        <source>&amp;Ignore/Unignore Packet(s)</source>
        <oldsource>&amp;Ignore/Unignore Packet</oldsource>
        <translation>패킷 무시/무시해제(&amp;I)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1534"/>
        <source>Ignore All Displayed</source>
        <translation>표시된 모든 패킷 무시</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1537"/>
        <source>Ignore all displayed packets</source>
        <translation>모든 표시된 패킷들을 무시</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1556"/>
        <source>Set/Unset Time Reference</source>
        <translation>시간 참조 설정/설정해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1559"/>
        <source>Set or unset a time reference for this packet</source>
        <translation>이 패킷에 대한 시간 참조 설정/설정해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1567"/>
        <source>Unset All Time References</source>
        <translation>모든 시간 참조 설정해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1570"/>
        <source>Remove all time references</source>
        <translation>모든 시간 참조 제거</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1578"/>
        <source>Next Time Reference</source>
        <translation>다음 시간 참조</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1581"/>
        <source>Go to the next time reference</source>
        <translation>다음 시간 참조로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1589"/>
        <source>Previous Time Reference</source>
        <translation>이전 시간 참조</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1592"/>
        <source>Go to the previous time reference</source>
        <translation>이전 시간 참조로 이동</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1603"/>
        <source>Shift or change packet timestamps</source>
        <translation>패킷 타임스탬프를 이동하거나 변경</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1611"/>
        <source>Delete All Packet Comments</source>
        <translation>모든 패킷 주석 삭제하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1614"/>
        <source>Remove all packet comments in the capture file</source>
        <translation>캡쳐 파일 내의 모든 패킷 주석을 삭제하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1622"/>
        <source>&amp;Configuration Profiles…</source>
        <translation>설정 프로파일…(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1625"/>
        <source>Configuration profiles</source>
        <translation>설정 프로파일</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1628"/>
        <source>Manage your configuration profiles</source>
        <translation>당신의 설정 프로파일을 관리</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1642"/>
        <source>Manage Wireshark&apos;s preferences</source>
        <translation>Wireshark의 설정을 관리</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1656"/>
        <source>Capture File Properties</source>
        <translation>캡쳐 파일 속성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1659"/>
        <source>Capture file properties</source>
        <translation>캡쳐 파일 속성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1670"/>
        <source>&amp;Protocol Hierarchy</source>
        <translation>프로토콜 수직계층(&amp;P)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1673"/>
        <source>Show a summary of protocols present in the capture file.</source>
        <translation>캡쳐 파일 내에 존재하는 프로토콜 요약을 보여줌</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1678"/>
        <source>Capinfos</source>
        <translation>Capinfos</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1683"/>
        <source>Reordercap</source>
        <translation>Reordercap</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1688"/>
        <source>Time Sequence (Stevens)</source>
        <translation>시간 순서 (Stevens)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1691"/>
        <source>TCP time sequence graph (Stevens)</source>
        <translation>TCP 시간 순서 그래프 (Stevens)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1696"/>
        <source>Throughput</source>
        <translation>처리량</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1704"/>
        <source>Round Trip Time</source>
        <translation>왕복시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1707"/>
        <source>TCP round trip time</source>
        <translation>TCP 왕복시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1712"/>
        <source>Window Scaling</source>
        <translation>윈도우 크기고정</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1715"/>
        <source>TCP window scaling</source>
        <translation>TCP 윈도우 크기조정</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1745"/>
        <source>DCCP Stream</source>
        <translation>DCCP 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1756"/>
        <source>TLS Stream</source>
        <translation>TLS 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1767"/>
        <source>HTTP Stream</source>
        <translation>HTTP 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1778"/>
        <source>HTTP/2 Stream</source>
        <translation>HTTP/2 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1786"/>
        <source>QUIC Stream</source>
        <translation>QUIC 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1794"/>
        <source>Websocket Stream</source>
        <translation>웹소켓 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1802"/>
        <source>SIP Call</source>
        <translation>SIP 호출</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1807"/>
        <source>Time Sequence (tcptrace)</source>
        <translation>시간 순서 (tcptrace)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1810"/>
        <source>TCP time sequence graph (tcptrace)</source>
        <translation>TCP 시간 순서 그래프 (tcptrace)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1815"/>
        <source>Analyse this Association</source>
        <translation>이 연결을 분석</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1820"/>
        <source>Show All Associations</source>
        <translation>모든 연결 보기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1825"/>
        <source>Flow Graph</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1828"/>
        <source>Flow sequence diagram</source>
        <translation>흐름 순서 다이어그램</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1833"/>
        <source>ANCP</source>
        <translation>ANCP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1836"/>
        <source>ANCP statistics</source>
        <translation>ANCP 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1841"/>
        <source>Packets sorted by Instance ID</source>
        <translation>인스턴스 ID로 정렬한 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1844"/>
        <source>BACapp statistics sorted by instance ID</source>
        <translation>인스턴스 ID로 정렬한 BACapp 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1849"/>
        <source>Packets sorted by IP</source>
        <translation>IP로 정렬한 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1852"/>
        <source>BACapp statistics sorted by IP</source>
        <translation>IP로 정렬한 BACapp 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1857"/>
        <source>Packets sorted by object type</source>
        <translation>객체 유형으로 정렬한 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1860"/>
        <source>BACapp statistics sorted by object type</source>
        <translation>객체 유형으로 정렬한 BACapp 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1865"/>
        <source>Packets sorted by service</source>
        <translation>서비스로 정렬한 패킷</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1868"/>
        <source>BACapp statistics sorted by service</source>
        <translation>서비스로 정렬한 BACapp 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1873"/>
        <source>Collectd</source>
        <translation>수집됨</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1876"/>
        <source>Collectd statistics</source>
        <translation>수집된 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1881"/>
        <source>DNS</source>
        <translation>DNS</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1884"/>
        <source>DNS statistics</source>
        <translation>DNS 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1889"/>
        <source>HART-IP</source>
        <translation>HART-IP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1892"/>
        <source>HART-IP statistics</source>
        <translation>HART-IP 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1897"/>
        <source>HPFEEDS</source>
        <translation>HPFEEDS</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1900"/>
        <source>hpfeeds statistics</source>
        <translation>hpfeeds 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1905"/>
        <source>HTTP2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1908"/>
        <source>HTTP2 statistics</source>
        <translation>HTTP2 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1913"/>
        <location filename="wireshark_main_window.ui" line="1993"/>
        <location filename="wireshark_main_window.ui" line="2001"/>
        <source>Packet Counter</source>
        <translation>패킷 카운터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1916"/>
        <source>HTTP packet counter</source>
        <translation>HTTP 패킷 카운터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1921"/>
        <source>Requests</source>
        <translation>요청</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1924"/>
        <source>HTTP requests</source>
        <translation>HTTP 리퀘스트</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1929"/>
        <source>Load Distribution</source>
        <translation>로드 분산</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1932"/>
        <source>HTTP load distribution</source>
        <translation>HTTP 로드 분산</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1945"/>
        <source>Packet Lengths</source>
        <translation>패킷 길이</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1948"/>
        <source>Packet length statistics</source>
        <translation>패킷 길이 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1953"/>
        <source>Sametime</source>
        <translation>동일시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1956"/>
        <source>Sametime statistics</source>
        <translation>동일시간 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1961"/>
        <source>SOME/IP Messages</source>
        <translation>SOME/IP 메시지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1964"/>
        <source>SOME/IP Message statistics</source>
        <translation>SOME/IP 메시지 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1969"/>
        <source>SOME/IP-SD Entries</source>
        <translation>SOME/IP-SD 엔트리</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1972"/>
        <source>SOME/IP-SD Entries statistics</source>
        <translation>SOME/IP-SD 엔트리 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1977"/>
        <source>&amp;LTP</source>
        <translation>LTP(&amp;L)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1980"/>
        <source>LTP segment and block statistics</source>
        <translation>LTP 세그먼트와 블럭 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1985"/>
        <source>&amp;ISUP Messages</source>
        <translation>ISUP 메시지(&amp;I)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1988"/>
        <source>ISUP message statistics</source>
        <translation>ISUP 메시지 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1996"/>
        <source>Osmux packet counts</source>
        <translation>Osmux 패킷 카운트</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2004"/>
        <source>RTSP packet counts</source>
        <translation> RTSP 패킷 카운트</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2009"/>
        <source>SM&amp;PP Operations</source>
        <translation>SMPP 동작(&amp;P)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2012"/>
        <source>SMPP operation statistics</source>
        <translation>SMPP 동작 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2017"/>
        <source>&amp;UCP Messages</source>
        <translation>UCP 메시지(&amp;U)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2020"/>
        <source>UCP message statistics</source>
        <translation>UCP 메시지 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2025"/>
        <source>F1AP</source>
        <translation>F1AP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2028"/>
        <source>F1AP Messages</source>
        <translation>F1AP 메시지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2033"/>
        <source>NGAP</source>
        <translation>NGAP</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2036"/>
        <source>NGAP Messages</source>
        <translation>NGAP 메시지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2044"/>
        <source>Change the way packets are dissected</source>
        <translation>패킷 분해 방식 변경</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2052"/>
        <source>Reload Lua Plugins</source>
        <translation>Lua 플러그인 재적재</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2055"/>
        <source>Reload Lua plugins</source>
        <translation>Lua 플러그인 재적재</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2068"/>
        <source>Advertisements by Topic</source>
        <translation>토픽으로 광고</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2073"/>
        <location filename="wireshark_main_window.ui" line="2108"/>
        <source>Advertisements by Source</source>
        <translation>소스로 광고</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2078"/>
        <source>Advertisements by Transport</source>
        <translation>전송으로 광고</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2083"/>
        <source>Queries by Topic</source>
        <translation>토픽으로 질의</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2088"/>
        <location filename="wireshark_main_window.ui" line="2118"/>
        <source>Queries by Receiver</source>
        <translation>리시버로 질의</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2093"/>
        <source>Wildcard Queries by Pattern</source>
        <translation>패턴으로 와일드카드 질의</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2098"/>
        <source>Wildcard Queries by Receiver</source>
        <translation>리시버에 의한 와일드카드 질의</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2103"/>
        <source>Advertisements by Queue</source>
        <translation>큐에 의한 광고</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2113"/>
        <source>Queries by Queue</source>
        <translation>큐에 의한 질의</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2123"/>
        <source>Streams</source>
        <translation>스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2128"/>
        <source>LBT-RM</source>
        <translation>LBT-RM</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2133"/>
        <source>LBT-RU</source>
        <translation>LBT-RU</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2138"/>
        <location filename="wireshark_main_window.ui" line="2141"/>
        <source>Filter this Association</source>
        <translation>이 연결을 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2151"/>
        <source>Strip Headers…</source>
        <translation>헤더 깎기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2154"/>
        <source>Strip headers and export higher level encapsulations to file</source>
        <translation>헤더를 깎고 더 높은 레벨의 캡슐화로 파일 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2159"/>
        <source>&amp;I/O Graphs</source>
        <translation>I/O 그래프(&amp;I)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2195"/>
        <source>&amp;Conversations</source>
        <translation>대화(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2203"/>
        <source>&amp;Endpoints</source>
        <translation>종단점(&amp;E)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2236"/>
        <source>Shrink the main window text</source>
        <translation>주 윈도우 텍스트 줄이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2247"/>
        <source>Return the main window text to its normal size</source>
        <translation>주 윈도우 텍스트를 보통 크기로 되돌림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2255"/>
        <source>Reset Layout</source>
        <translation>레이아웃 초기화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2258"/>
        <source>Reset appearance layout to default size</source>
        <translation>기본 크기로 모양 레이아웃을 초기화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2333"/>
        <source>Seconds Since First Captured Packet</source>
        <translation>1차 캡쳐된 패킷으로부터 2차</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2587"/>
        <source>Packet &amp;Diagram</source>
        <translation>패킷 다이어그램(&amp;D)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2590"/>
        <source>Show or hide the packet diagram</source>
        <translation>패킷 다이어그램 보이기 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2598"/>
        <source>Show each conversation hash table</source>
        <translation>각 대화 해시 테이블 보이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2606"/>
        <source>Show each dissector table and its entries</source>
        <translation>각 분해자 테이블과 항목들을 보이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2614"/>
        <source>Show the currently supported protocols and display filter fields</source>
        <translation>현재 지원하는 프로토콜과 표시 필터 필드를 보이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2627"/>
        <source>MAC Statistics</source>
        <translation>MAC 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2630"/>
        <source>LTE MAC statistics</source>
        <translation>LTE MAC 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2635"/>
        <source>RLC Statistics</source>
        <translation>RLC 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2638"/>
        <source>LTE RLC statistics</source>
        <translation>LTE RLC 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2646"/>
        <source>LTE RLC graph</source>
        <translation>LTE RLC 그래프</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2651"/>
        <source>MTP3 Summary</source>
        <translation>MTP3 요약</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2654"/>
        <source>MTP3 summary statistics</source>
        <translation>MTP3 요약 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2693"/>
        <source>Bluetooth Devices</source>
        <translation>블루투스 장치</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2698"/>
        <source>Bluetooth HCI Summary</source>
        <translation>블루투스 HCI 요약</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2738"/>
        <source>Display Filter &amp;Expression…</source>
        <translation>표시 필터 표현식(&amp;E)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2741"/>
        <source>Display Filter Expression…</source>
        <translation>표시 필터 표현식…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2763"/>
        <source>REGISTER_STAT_GROUP_RSERPOOL</source>
        <translation>REGISTER_STAT_GROUP_RSERPOOL</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2766"/>
        <source>Start of &quot;REGISTER_STAT_GROUP_RSERPOOL&quot;</source>
        <translation>&quot;REGISTER_STAT_GROUP_RSERPOOL&quit;의 시작</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2788"/>
        <source>No GSM statistics registered</source>
        <translation>등록된 GSM 통계가 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2796"/>
        <source>No LTE statistics registered</source>
        <translation>등록된 LTE 통계 없음</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2804"/>
        <source>No MTP3 statistics registered</source>
        <translation>등록된 MTP3 통계가 없음</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2962"/>
        <source>IAX2 Stream Analysis</source>
        <translation>IAX2 스트림 분석</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2986"/>
        <source>Show Packet Bytes…</source>
        <translation>패킷 바이트 보기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3010"/>
        <source>Go to &amp;Linked Packet</source>
        <translation>연결된 패킷으로 이동(&amp;L)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3018"/>
        <source>UDP Multicast Streams</source>
        <translation>UDP 멀티캐스트 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3021"/>
        <source>Show UTP multicast stream statistics.</source>
        <translation>UTP 멀티캐스트 스트림 통계를 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3026"/>
        <source>WLAN Traffic</source>
        <translation>WLAN 트래픽</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3029"/>
        <source>Show IEEE 802.11 wireless LAN statistics.</source>
        <translation>IEEE 802.11 무선 LAN 통계를 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3034"/>
        <source>Add a display filter button.</source>
        <translation>표시 필터 버튼을 추가합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3039"/>
        <source>Firewall ACL Rules</source>
        <translation>방화벽 ACL 규칙</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3042"/>
        <source>Create firewall ACL rules</source>
        <translation>방화벽 ACL 규칙 생성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3050"/>
        <source>&amp;Full Screen</source>
        <translation>전체 스크린(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3055"/>
        <source>Credentials</source>
        <translation>인증서</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1265"/>
        <source>&amp;Options…</source>
        <translation>옵션…(&amp;O)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="703"/>
        <source>&amp;Wireless</source>
        <translation>무선(&amp;W)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1282"/>
        <source>Capture &amp;Filters…</source>
        <translation>캡쳐 필터…(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1312"/>
        <source>As Plain &amp;Text…</source>
        <translation>순수 텍스트…(&amp;T)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1353"/>
        <source>As Plain &amp;Text</source>
        <translation>순수 텍스트(&amp;T)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1358"/>
        <source>As &amp;CSV</source>
        <translation>CSV(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1363"/>
        <source>As &amp;YAML</source>
        <translation>YAML(&amp;Y)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1368"/>
        <source>All Visible Items</source>
        <translation>보이는 모든 항목</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1376"/>
        <source>All Visible Selected Tree Items</source>
        <translation>보이는 모든 선택된 트리 항목</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1419"/>
        <source>Display Filter &amp;Macros…</source>
        <translation>필터 매크로 표시…(&amp;M)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1435"/>
        <source>&amp;Find Packet…</source>
        <translation>패킷 찾기…(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1446"/>
        <source>Find Ne&amp;xt</source>
        <translation>다음 찾기(&amp;x)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1457"/>
        <source>Find Pre&amp;vious</source>
        <translation>이전 찾기(&amp;v)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1471"/>
        <source>Mark or unmark each selected packet</source>
        <translation>각 선택된 패킷을 마크 혹은 마크해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1526"/>
        <source>Ignore or unignore each selected packet</source>
        <translation>각 선택된 패킷을 무시 혹은 무시해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1545"/>
        <source>U&amp;nignore All Displayed</source>
        <translation>표시된 모든 항목을 무시해제(&amp;n)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1548"/>
        <source>Unignore all displayed packets</source>
        <translation>표시된 모든 패킷을 무시 해제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1600"/>
        <source>Time Shift…</source>
        <translation>시간 이동…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1639"/>
        <source>&amp;Preferences…</source>
        <translation>설정(&amp;P)…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1699"/>
        <source>TCP throughput</source>
        <translation>TCP 처리량</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1723"/>
        <source>TCP Stream</source>
        <translation>TCP 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1734"/>
        <source>UDP Stream</source>
        <translation>UDP 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1937"/>
        <source>Request Sequences</source>
        <translation>요청 순서</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1940"/>
        <source>HTTP Request Sequences</source>
        <translation>HTTP 요청 순서</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2041"/>
        <source>Decode &amp;As…</source>
        <translation>다음 인코딩으로 디코딩…(&amp;A)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2146"/>
        <source>Export PDUs to File…</source>
        <translation>PDU를 파일로 내보내기…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2162"/>
        <source>Create graphs based on display filter fields</source>
        <translation>표시 필터 필드를 기반으로 그래프 생성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2173"/>
        <source>&amp;Main Toolbar</source>
        <translation>주 도구막대(&amp;M)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2176"/>
        <source>Show or hide the main toolbar</source>
        <translation>주 도구막대 표시 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2187"/>
        <source>&amp;Filter Toolbar</source>
        <translation>필터 도구막대(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2190"/>
        <source>Show or hide the display filter toolbar</source>
        <translation>표시 필터 도구막대 표시 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2198"/>
        <source>Conversations at different protocol levels</source>
        <translation>다른 프로토콜 레벨의 대화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2206"/>
        <source>Endpoints at different protocol levels</source>
        <translation>다른 프로토콜 레벨의 종단점</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2214"/>
        <source>Colorize Packet List</source>
        <translation>패킷 목록 색상화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2217"/>
        <source>Draw packets using your coloring rules</source>
        <translation>당신의 컬러링 규칙을 사용하여 패킷을 그리기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2222"/>
        <source>&amp;Zoom In</source>
        <translation>확대(&amp;Z)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2225"/>
        <source>Enlarge the main window text</source>
        <translation>주 윈도우 텍스트 확대</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2233"/>
        <source>Zoom Out</source>
        <translation>축소</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2244"/>
        <source>Normal Size</source>
        <translation>보통 크기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2266"/>
        <source>Resize Columns</source>
        <translation>열 크기조정</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2269"/>
        <source>Resize packet list columns to fit contents</source>
        <translation>내용에 맞춰 패킷 목록 열을 크기조정</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2280"/>
        <source>Date and Time of Day (1970-01-01 01:02:03.123456)</source>
        <translation>날짜와 시간 (1970-01-01 01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2283"/>
        <location filename="wireshark_main_window.ui" line="2308"/>
        <location filename="wireshark_main_window.ui" line="2336"/>
        <source>Show packet times as the date and time of day.</source>
        <translation>패킷 시간을 날짜와 시간으로 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2294"/>
        <source>Year, Day of Year, and Time of Day (1970/001 01:02:03.123456)</source>
        <translation>년, 연중일, 시각 (1970/001 01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2297"/>
        <source>Show packet times as the year, day of the year and time of day.</source>
        <translation>연도, 년도 단위 일수, 시각으로 패킷 시간을 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2305"/>
        <source>Time of Day (01:02:03.123456)</source>
        <translation>시각 (01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2319"/>
        <source>Seconds Since 1970-01-01</source>
        <translation> 1970-01-01로부터 초단위 시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2322"/>
        <source>Show packet times as the seconds since the UNIX / POSIX epoch (1970-01-01).</source>
        <translation>UNIX / POSIX 시점 (1970-01-01)으로부터의 초단위 시간으로 패킷 시간을 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2347"/>
        <source>Seconds Since Previous Captured Packet</source>
        <translation>이전 패킷이 캡쳐된 순간으로부터 초단위 시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2350"/>
        <source>Show packet times as the seconds since the previous captured packet.</source>
        <translation>이전 패킷 캡쳐된 시간으로부터 초단위 시간으로 패킷 시간을 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2361"/>
        <source>Seconds Since Previous Displayed Packet</source>
        <translation>이전 표시된 패킷으로부터 초단위 시간</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2364"/>
        <source>Show packet times as the seconds since the previous displayed packet.</source>
        <translation>이전 표시된 패킷으로부터의 초단위 시간으로 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2375"/>
        <source>UTC Date and Time of Day (1970-01-01 01:02:03.123456)</source>
        <translation>UTC 날짜와 시간 (1970-01-01 01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2378"/>
        <source>Show packet times as the UTC date and time of day.</source>
        <translation>UTC 날짜와 시각으로 패킷 시간을 표현합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2389"/>
        <source>UTC Year, Day of Year, and Time of Day (1970/001 01:02:03.123456)</source>
        <translation>UTC 년, 연중일, 시각 (1970/001 01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2392"/>
        <source>Show packet times as the UTC year, day of the year and time of day.</source>
        <translation>UTC 년, 연중일, 시각으로 패킷 시간을 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2400"/>
        <source>UTC Time of Day (01:02:03.123456)</source>
        <translation>UTC 시각 (01:02:03.123456)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2403"/>
        <source>Show packet times as the UTC time of day.</source>
        <translation>UTC 시각으로 패킷 시간을 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2414"/>
        <source>Automatic (from capture file)</source>
        <translation>자동 (캡쳐 파일로부터)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2417"/>
        <source>Use the time precision indicated in the capture file.</source>
        <translation>캡쳐 파일에 표기된 시간 표기를 사용합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2425"/>
        <source>Seconds</source>
        <translation>초단위</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2433"/>
        <source>Tenths of a second</source>
        <translation>십분의 1초</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2441"/>
        <source>Hundredths of a second</source>
        <translation>백분의 1초</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2449"/>
        <source>Milliseconds</source>
        <translation>밀리초</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2457"/>
        <source>Microseconds</source>
        <translation>마이크로초</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2465"/>
        <source>Nanoseconds</source>
        <translation>나노초</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2473"/>
        <source>Display Seconds With Hours and Minutes</source>
        <translation>시간과 분으로 초 표시하기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2476"/>
        <source>Display seconds with hours and minutes</source>
        <translation>시간과 분으로 초들을 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2484"/>
        <source>Resolve &amp;Physical Addresses</source>
        <translation>물리 주소 해석(&amp;P)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2487"/>
        <source>Show names for known MAC addresses. Lookups use a local database.</source>
        <translation>잘 알려진 MAC 주소에 대한 이름을 표시합니다. 검색에 로컬 데이터에비스를 사용합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2495"/>
        <source>Resolve &amp;Network Addresses</source>
        <translation>네트워크 주소 해석</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2498"/>
        <source>Show names for known IPv4, IPv6, and IPX addresses. Lookups can generate network traffic.</source>
        <translation>알려진 IPv4, IPv6, IPX 주소를 이름으로 표시합니다. 검색에 네트워크 트래픽을 발생할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2506"/>
        <source>Resolve &amp;Transport Addresses</source>
        <translation>전송 주소 해석(&amp;T)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2509"/>
        <source>Show names for known TCP, UDP, and SCTP services. Lookups can generate traffic on some systems.</source>
        <translation>알려진 TCP, UDP, SCTP 서비스에 대한 이름을 표시합니다. 검색 시 몇몇 시스템 상에 트래픽을 발생할 수 있습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2517"/>
        <source>Wire&amp;less Toolbar</source>
        <translation>무선 도구막대(&amp;l)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2520"/>
        <source>Show or hide the wireless toolbar</source>
        <translation>무선 도구막대 표시 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2531"/>
        <source>&amp;Status Bar</source>
        <translation>상태 막대(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2534"/>
        <source>Show or hide the status bar</source>
        <translation>상태 막대 표시하기 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2545"/>
        <source>Packet &amp;List</source>
        <translation>패킷 목록(&amp;L)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2548"/>
        <source>Show or hide the packet list</source>
        <translation>패킷 목록 보이기 혹은 감추기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2559"/>
        <source>Packet &amp;Details</source>
        <translation>패킷 세부사항(&amp;D)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2562"/>
        <source>Show or hide the packet details</source>
        <translation>패킷 세부사항 보이기 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2573"/>
        <source>Packet &amp;Bytes</source>
        <translation>패킷 바이트(&amp;B)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2576"/>
        <source>Show or hide the packet bytes</source>
        <translation>패킷 바이트를 표시 혹은 숨김</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2595"/>
        <source>&amp;Conversation Hash Tables</source>
        <translation>대화 해시 테이블(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2603"/>
        <source>&amp;Dissector Tables</source>
        <translation>분해 테이블(&amp;D)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2611"/>
        <source>&amp;Supported Protocols</source>
        <translation>지원되는 프로토콜(&amp;S)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2619"/>
        <source>MAP Summary</source>
        <translation>MAP 요약</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2622"/>
        <source>GSM MAP summary statistics</source>
        <translation>GSM MAP 요약 통계</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2643"/>
        <source>RLC &amp;Graph</source>
        <translation>RLC 그래프(&amp;G)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2680"/>
        <source>&amp;Coloring Rules…</source>
        <translation>색상 규칙…(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2711"/>
        <source>Show Linked Packet in New Window</source>
        <translation>새 윈도우에 연결된 패킷 보이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2924"/>
        <source>New Coloring Rule…</source>
        <oldsource>New Conversation Rule…</oldsource>
        <translation>새 색상 규칙…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2946"/>
        <source>RTP Stream Analysis for selected stream. Press CTRL key for adding reverse stream too.</source>
        <translation>선택한 스트림에 대한 RTP 스트림 분석. 역방향 스트림을 추가하기 위해 CTRL 키를 누르십시오.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2951"/>
        <source>RTP Player</source>
        <translation>RTP 재생기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2954"/>
        <source>Play selected stream. Press CTRL key for playing reverse stream too.</source>
        <translation>선택한 스트림을 재생합니다. 역방향 스트림을 재생하기 위해서는 CTRL 키를 누르십시오.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2959"/>
        <source>IA&amp;X2 Stream Analysis</source>
        <translation>IAX2 스트림 분석(&amp;X)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2975"/>
        <source>Enabled Protocols…</source>
        <oldsource>Enable Protocols…</oldsource>
        <translation>프로토콜 사용…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2994"/>
        <source>Wiki Protocol Page</source>
        <translation>위키 프로토콜 페이지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2997"/>
        <source>Open the Wireshark wiki page for this protocol.</source>
        <translation>이 프로토콜에 대한 Wireshark 위키 페이지 열기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3002"/>
        <source>Filter Field Reference</source>
        <translation>필터 필드 참조</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3005"/>
        <source>Open the display filter reference page for this filter field.</source>
        <translation>이 필터 필드에 대한 표시 필터 참조 페이지를 열기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="3013"/>
        <source>Go to the packet referenced by the selected field.</source>
        <translation>선택한 필드를 참조하는 패킷으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2659"/>
        <source>&amp;VoIP Calls</source>
        <translation>VoIP 호출들(&amp;V)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="153"/>
        <source>Open &amp;Recent</source>
        <translation>최신 파일 열기(&amp;R)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="318"/>
        <source>Name Resol&amp;ution</source>
        <translation>이름 해석(&amp;u)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="528"/>
        <source>Service &amp;Response Time</source>
        <translation>서비스 응답시간(&amp;R)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="589"/>
        <source>&amp;RTP</source>
        <translation>RTP(&amp;R)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="597"/>
        <source>S&amp;CTP</source>
        <translation>SCTP(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="604"/>
        <source>&amp;ANSI</source>
        <translation>ANSI(&amp;)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="610"/>
        <source>&amp;GSM</source>
        <translation>GSM(&amp;G)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="616"/>
        <source>&amp;LTE</source>
        <translation>LTE(&amp;L)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="622"/>
        <source>&amp;MTP3</source>
        <translation>MTP3(&amp;M)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="821"/>
        <source>&amp;Open</source>
        <translation>Open(&amp;O)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="835"/>
        <source>&amp;Quit</source>
        <translation>종료(&amp;Q)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="874"/>
        <source>&amp;Close</source>
        <translation>닫기(&amp;C)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1414"/>
        <source>Display &amp;Filters…</source>
        <translation>필터 표시…(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="1490"/>
        <source>&amp;Unmark All Displayed</source>
        <translation>표시된 모든 내용 마크 해제(&amp;U)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2662"/>
        <source>All VoIP Calls</source>
        <translation>모든 VoIP 호출</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2667"/>
        <source>SIP &amp;Flows</source>
        <translation>SIP 흐름(&amp;F)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2670"/>
        <source>SIP Flows</source>
        <translation>SIP 흐름</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2675"/>
        <source>RTP Streams</source>
        <translation>RTP 스트림</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2683"/>
        <source>Edit the packet list coloring rules.</source>
        <translation>패킷 목록 색상 규칙을 편집합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2688"/>
        <source>Bluetooth ATT Server Attributes</source>
        <oldsource>ATT Server Attributes</oldsource>
        <translation>ATT 서버 속성</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2703"/>
        <source>Show Packet in New &amp;Window</source>
        <translation>새 윈도우에서 패킷 보이기(&amp;W)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2706"/>
        <source>Show this packet in a separate window.</source>
        <translation>분리된 윈도우에 이 패킷을 표시합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2714"/>
        <source>Show the linked packet in a separate window.</source>
        <translation>분리된 윈도우에 연결된 패킷을 보여줍니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2722"/>
        <source>Auto Scroll in Li&amp;ve Capture</source>
        <translation>자동 스크롤 라이브 캡쳐(&amp;v)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2725"/>
        <source>Automatically scroll to the last packet during a live capture.</source>
        <translation>라이브 캡쳐를 진행하는 동안 마지막 패킷으로 자동 스크롤 합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2730"/>
        <source>Expert Information</source>
        <translation>정보 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2733"/>
        <source>Show expert notifications</source>
        <translation>전문적인 알림 보이기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2744"/>
        <source>Add an expression to the display filter.</source>
        <translation>표시 필터에 대한 표현식을 추가합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2749"/>
        <source>REGISTER_STAT_GROUP_UNSORTED</source>
        <translation>REGISTER_STAT_GROUP_UNSORTED</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2752"/>
        <source>Start of &quot;REGISTER_STAT_GROUP_UNSORTED&quot;</source>
        <translation>&quot;REGISTER_STAT_GROUP_UNSORTED&quot;의 시작</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2780"/>
        <source>No ANSI statistics registered</source>
        <oldsource>No tools registered</oldsource>
        <translation>등록된 ANSI 통계가 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2809"/>
        <source>Resolved Addresses</source>
        <translation>해석된 주소들</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2812"/>
        <source>Show each table of resolved addresses as copyable text.</source>
        <translation>복사 가능한 텍스트로서 해석된 주소의 각 테이블을 보여줍니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2817"/>
        <source>Color &amp;1</source>
        <translation>색상 &amp;1</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2820"/>
        <location filename="wireshark_main_window.ui" line="2831"/>
        <location filename="wireshark_main_window.ui" line="2842"/>
        <location filename="wireshark_main_window.ui" line="2853"/>
        <location filename="wireshark_main_window.ui" line="2864"/>
        <location filename="wireshark_main_window.ui" line="2875"/>
        <location filename="wireshark_main_window.ui" line="2886"/>
        <location filename="wireshark_main_window.ui" line="2897"/>
        <location filename="wireshark_main_window.ui" line="2908"/>
        <location filename="wireshark_main_window.ui" line="2919"/>
        <source>Mark the current conversation with its own color.</source>
        <oldsource>Mark the current coversation with its own color.</oldsource>
        <translation>현재 대화가 가지고 있는 색상으로 마크합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2828"/>
        <source>Color &amp;2</source>
        <translation>색상 &amp;2</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2839"/>
        <source>Color &amp;3</source>
        <translation>색상 &amp;3</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2850"/>
        <source>Color &amp;4</source>
        <translation>색상 &amp;4</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2861"/>
        <source>Color &amp;5</source>
        <translation>색상 &amp;5</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2872"/>
        <source>Color &amp;6</source>
        <translation>색상 &amp;6</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2883"/>
        <source>Color &amp;7</source>
        <translation>색상 &amp;7</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2894"/>
        <source>Color &amp;8</source>
        <translation>색상 &amp;8</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2905"/>
        <source>Color &amp;9</source>
        <translation>색상 &amp;9</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2916"/>
        <source>Color 1&amp;0</source>
        <translation>색상 1&amp;0</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2927"/>
        <source>Create a new coloring rule based on this field.</source>
        <oldsource>Create a new coloring rule based on this conversation.</oldsource>
        <translation>이 대화에 기반한 새로운 색상 규칙을 생성하십시오.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2932"/>
        <source>Reset Colorization</source>
        <translation>색상화 초기화</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2935"/>
        <source>Reset colorized conversations.</source>
        <translation>색상처리된 대화를 초기화합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2943"/>
        <source>RTP Stream Analysis</source>
        <translation>RTP 스트림 분석</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2967"/>
        <source>Edit Resolved Name</source>
        <translation>해석된 이름 편집</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2970"/>
        <source>Manually edit a name resolution entry.</source>
        <translation>수동으로 이름 해석 항목을 편집합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.ui" line="2978"/>
        <source>Enable and disable specific protocols</source>
        <translation>특정 프로토콜을 사용하거나 사용하지 않음</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="959"/>
        <source> before quitting</source>
        <translation> 종료 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1198"/>
        <source>Save packets before merging?</source>
        <translation>병합 이전에 패킷을 저장하시겠습니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1199"/>
        <source>A temporary capture file can&apos;t be merged.</source>
        <translation>임시 캡쳐 파일은 병합할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1205"/>
        <source>Save changes in &quot;%1&quot; before merging?</source>
        <translation>병합하기 전에 &quot;%1&quot;에서 수정된 내용을 저장하시겠습니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1207"/>
        <source>Changes must be saved before the files can be merged.</source>
        <translation>파일들을 병합하기 전에 변경된 내용을 저정해야만 합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="230"/>
        <source>Invalid Display Filter</source>
        <translation>유효하지 않은 표시 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1244"/>
        <source>Invalid Read Filter</source>
        <translation>유효하지 않은 읽기 필터</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1245"/>
        <source>The filter expression %1 isn&apos;t a valid read filter. (%2).</source>
        <translation>필터 표현식 %1 유효하지 않은 읽기 필터입니다. (%2).</translation> 
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1330"/>
        <source> before importing a capture</source>
        <oldsource> before importing a new capture</oldsource>
        <translation> 새로운 캡쳐를 가져오기 전에</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1626"/>
        <source>Unable to export to &quot;%1&quot;.</source>
        <translation>&quot;%1&quot;에 내보낼 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1627"/>
        <source>You cannot export packets to the current capture file.</source>
        <translation>현재 캡쳐 파일로 패킷을 내보낼 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1862"/>
        <source>Do you want to save the changes you&apos;ve made%1?</source>
        <oldsource>Do you want to save the captured packets</oldsource>
        <translation>여러분이 생성한 %1 의 변경 내용을 저장하시길 원하십니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1858"/>
        <location filename="wireshark_main_window.cpp" line="1866"/>
        <source>Your captured packets will be lost if you don&apos;t save them.</source>
        <translation>저장하지 않으신다면, 캡쳐한 패킷들은 소실될 것입니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1871"/>
        <source>Do you want to save the changes you&apos;ve made to the capture file &quot;%1&quot;%2?</source>
        <translation>당신이 캡쳐파일 &quot;%1&quot;%2 에 생성한 변경내용을 저장하시길 원하십니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1863"/>
        <location filename="wireshark_main_window.cpp" line="1872"/>
        <source>Your changes will be lost if you don&apos;t save them.</source>
        <translation>저장하시지 않으시면, 변경내용이 소실될 것입니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="353"/>
        <source>Check for Updates…</source>
        <translation>업데이트 확인…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1002"/>
        <source>Unable to drop files during capture.</source>
        <translation>캡쳐를 하는 동안에는 파일을 버릴 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1497"/>
        <source>Unknown file type returned by merge dialog.</source>
        <translation>병합 대화상자로부터 알 수 없는 파일 형식을 반환받았습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1498"/>
        <location filename="wireshark_main_window.cpp" line="1642"/>
        <source>Please report this as a Wireshark issue at https://gitlab.com/wireshark/wireshark/-/issues.</source>
        <translation>Wireshark 이슈로서 이 문제를 https://gitlab.com/wireshark/wireshark/-/issues 에 보고하여 주십시오.
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1641"/>
        <source>Unknown file type returned by export dialog.</source>
        <translation>내보내기 대화상자로부터 알 수 없는 파일 형식을 반환받았습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1857"/>
        <source>Do you want to stop the capture and save the captured packets%1?</source>
        <translation>캡쳐를 정지하고 캡텨된 패킷 %1 저장하시길 원하십니까?</translation> 
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1865"/>
        <source>Do you want to save the captured packets%1?</source>
        <translation>캡쳐한 패킷 %1 저장하시길 원하십니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1889"/>
        <source>Save before Continue</source>
        <translation>계속하기 전에 저장</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1891"/>
        <source>Stop and Save</source>
        <translation>정지 그리고 저장</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1903"/>
        <source>Stop and Quit &amp;without Saving</source>
        <oldsource>Stop and Quit without Saving</oldsource>
        <translation>정지 그리고 저장없이 종료</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1915"/>
        <source>Quit &amp;without Saving</source>
        <oldsource>Quit without Saving</oldsource>
        <translation>저장없이 종료</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="3030"/>
        <source>There is no &quot;rtp.ssrc&quot; field in this version of Wireshark.</source>
        <translation>이 버전의 Wireshark에서는 &quot;rtp.ssrc&quot; 필드가 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="3065"/>
        <source>Please select an RTPv2 packet with an SSRC value</source>
        <translation>SSRC 값으로 RTPv2 패킷을 선택하여 주십시오</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="3075"/>
        <source>SSRC value not found.</source>
        <translation>SSRC 값을 찾을 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="819"/>
        <location filename="wireshark_main_window.cpp" line="2942"/>
        <source>Show or hide the toolbar</source>
        <translation>도구막대 보이기 혹은 숨기기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1906"/>
        <location filename="wireshark_main_window.cpp" line="1919"/>
        <source>Continue &amp;without Saving</source>
        <oldsource>Continue without Saving</oldsource>
        <translation>저장없이 계속하기(&amp;w)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="1909"/>
        <source>Stop and Continue &amp;without Saving</source>
        <oldsource>Stop and Continue without Saving</oldsource>
        <translation>정지 그리고 저장없이 계속하기(&amp;w)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="2403"/>
        <source>The Wireshark Network Analyzer</source>
        <translation>Wireshark 네트워크 분석기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window.cpp" line="2432"/>
        <source>Capturing from %1</source>
        <translation>%1 으로부터 캡쳐링</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="218"/>
        <source> before opening another file</source>
        <translation> 다른 파일을 열기 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="676"/>
        <source>Merging files.</source>
        <translation>파일을 병합중입니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="758"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1092"/>
        <source>Clear Menu</source>
        <translation>메뉴 비우기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1783"/>
        <source> before closing the file</source>
        <translation> 파일을 닫기 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1860"/>
        <source>Export Selected Packet Bytes</source>
        <translation>선택된 패킷 바이트를 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1924"/>
        <source>No Keys</source>
        <translation>키가 없음</translation>
    </message>
    <message numerus="yes">
        <source>Export SSL Session Keys (%Ln key(s))</source>
        <oldsource>Export SSL Session Keys (%1 key%2</oldsource>
        <translation>SSL 세션 키 (%Ln 키) 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1862"/>
        <source>Raw data (*.bin *.dat *.raw);;All Files (</source>
        <translation>Raw 데이터 (*.bin *.dat *.raw);;모든 파일 (</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2188"/>
        <source>Couldn&apos;t copy text. Try another item.</source>
        <translation>텍스트를 복사할 수 없습니다. 다른 항목으로 시도해보십시오.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2299"/>
        <source>Are you sure you want to remove all packet comments?</source>
        <translation>모든 패킷 주석을 제거하시길 원하십니까?</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2642"/>
        <location filename="wireshark_main_window_slots.cpp" line="2848"/>
        <source>Unable to build conversation filter.</source>
        <translation>대화 필터를 구축할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2746"/>
        <location filename="wireshark_main_window_slots.cpp" line="2764"/>
        <source> before reloading the file</source>
        <translation> 파일을 다시 불러오기 전에</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2855"/>
        <source>Error compiling filter for this conversation.</source>
        <translation>이 대화에 대한 필터를 컴파일 하는 중 오류가 발생하였습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2864"/>
        <source>No previous/next packet in conversation.</source>
        <translation>이 대화에서 이전/다음 패킷이 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="862"/>
        <source>No interface selected.</source>
        <translation>선택한 인터페이스가 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="692"/>
        <source>Saving %1…</source>
        <translation>%1 저장중…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="887"/>
        <source>Configure all extcaps before start of capture.</source>
        <translation>캡쳐를 시작하기 전에 모든 extcaps 설정합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="897"/>
        <source>Invalid capture filter.</source>
        <translation>유효하지 않은 캡쳐 필터입니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1121"/>
        <source>(empty comment)</source>
        <comment>placeholder for empty comment</comment>
        <translation>(빈 주석)</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1129"/>
        <location filename="wireshark_main_window.cpp" line="523"/>
        <source>Add New Comment…</source>
        <translation>새 주석 추가…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1142"/>
        <source>Edit &quot;%1&quot;</source>
        <comment>edit packet comment</comment>
        <translation>&quot;%1&quot; 편집</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1151"/>
        <source>Delete &quot;%1&quot;</source>
        <comment>delete packet comment</comment>
        <translation>&quot;%1&quot; 삭제</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1156"/>
        <source>Delete packet comments</source>
        <translation>패킷 주석 삭제</translation>
    </message>
    <message numerus="yes">
        <location filename="wireshark_main_window_slots.cpp" line="1163"/>
        <source>Delete comments from %n packet(s)</source>
        <translation><numerusform>&n 패킷으로부터 주석을 삭제</numerusform></translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1462"/>
        <location filename="wireshark_main_window_slots.cpp" line="2989"/>
        <location filename="wireshark_main_window_slots.cpp" line="4013"/>
        <source> before starting a new capture</source>
        <translation> 새로운 캡쳐를 시작하기 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1539"/>
        <source> before reloading Lua plugins</source>
        <translation> Lua 플러그인을 다시 불러오기 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1700"/>
        <source>Please wait while Wireshark is initializing…</source>
        <translation>Wireshark를 초기화 하는 중에 기다려 주십시오…</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1925"/>
        <source>There are no TLS Session Keys to save.</source>
        <translation>저장하기 위한 TLS 세션 키가 없습니다.</translation>
    </message>
    <message numerus="yes">
        <location filename="wireshark_main_window_slots.cpp" line="1931"/>
        <source>Export TLS Session Keys (%Ln key(s))</source>
        <translation>TLS 세션 키 (%Ln 키) 내보내기</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="1935"/>
        <source>TLS Session Keys (*.keys *.txt);;All Files (</source>
        <translation>TLS 세션 키 (*.keys *.txt);;모든 파일 (</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3102"/>
        <source>No filter available. Try another %1.</source>
        <translation>사용 가능한 필터가 없습니다. 다른 %1 시도합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3102"/>
        <source>column</source>
        <translation>열</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3102"/>
        <source>item</source>
        <translation>항목</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3120"/>
        <source>The &quot;%1&quot; column already exists.</source>
        <translation>&quot;%1&quot; 열은 이미 존재합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3122"/>
        <source>The &quot;%1&quot; column already exists as &quot;%2&quot;.</source>
        <translation>&quot;%1&quot;열은 &quot;%2&quot;으로 이미 존재합니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="3720"/>
        <location filename="wireshark_main_window_slots.cpp" line="3742"/>
        <source>RTP packet search failed</source>
        <translation>RTP 패킷 검색 실패</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2982"/>
        <source>No Interface Selected.</source>
        <translation>선택한 인터페이스가 없습니다.</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="2888"/>
        <source> before restarting the capture</source>
        <translation> 캡쳐를 다시 시작하기 전</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="4054"/>
        <source>Wiki Page for %1</source>
        <translation>%1 에 대한 위키 페이지</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="4055"/>
        <source>&lt;p&gt;The Wireshark Wiki is maintained by the community.&lt;/p&gt;&lt;p&gt;The page you are about to load might be wonderful, incomplete, wrong, or nonexistent.&lt;/p&gt;&lt;p&gt;Proceed to the wiki?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Wireshark 위키는 커뮤니티에 의해 운영되고 있습니다&lt;/p&gt;&lt;p&gt; 여러분이 방문하려는 페이지는 뛰어날 수도, 불완전 할 수도, 잘못될 수도, 혹은 존재하지 않을 수도 있습니다.&lt;/p&gt;&lt;p&gt;위키 페이지로 진행하시겠습니까?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="619"/>
        <source>Loading</source>
        <translation>불러오는중</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="632"/>
        <source>Reloading</source>
        <translation>다시 불러오는 중</translation>
    </message>
    <message>
        <location filename="wireshark_main_window_slots.cpp" line="646"/>
        <source>Rescanning</source>
        <translation>재탐색중</translation>
    </message>
</context>
<context>
    <name>WlanStatisticsDialog</name>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="498"/>
        <source>Wireless LAN Statistics</source>
        <translation>무선 랜 통계</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="502"/>
        <source>Channel</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="502"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="502"/>
        <source>Percent Packets</source>
        <translation>패킷 비율</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="502"/>
        <source>Percent Retry</source>
        <translation>재시도 비율</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="503"/>
        <source>Probe Reqs</source>
        <translation>Probe Reqs</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="503"/>
        <source>Probe Resp</source>
        <translation>Probe Resp</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="503"/>
        <source>Auths</source>
        <translation>인증</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="503"/>
        <source>Retry</source>
        <translation>재시도</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="504"/>
        <source>Deauths</source>
        <translation>비인증</translation>
    </message>
    <message>
        <location filename="wlan_statistics_dialog.cpp" line="504"/>
        <source>Other</source>
        <translation>기타</translation>
    </message>
</context>
</TS>
